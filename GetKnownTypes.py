from imports import *

class TypeFinder():
# get all object expressions for which the type is known.
# type is known when virtual table is assigned to object or when object is sent to constructor
	typesCache = {}
	depthLimit = 1

	def getKnownTypes(self,ea, d=0):
		from analysisUtils import getDataAnalyzer
		da = getDataAnalyzer(ea)
		#i = 0
		#indent = ""
		#while i < d:
		#	indent += "\t"
		#	i += 1
		#print indent+hex(ea)[:-1]
		f = da.function.ea
		#print hex(f)[:-1]
		if f in TypeFinder.typesCache.keys():
			if d in TypeFinder.typesCache[f].keys():
				return TypeFinder.typesCache[f][d]
		else:
			TypeFinder.typesCache[f] = {}
		types = {}
		"""
		# collect objects sent to constructors
		constructors = {}
		for vt in vts:
			constructors[vt.ea] = set(map(lambda x: hex(getFunctionStartAddress(x))[:-1], filter(lambda y:getFunctionStartAddress(y),getFilteredConstructorsOfVT(vt))))
		allConstructors = set()
		for c in constructors.keys():
			allConstructors = allConstructors.union(constructors[c])
		#print allConstructors
		"""
		#if hex(f)[:-1] not in allConstructors:
		if d < TypeFinder.depthLimit:
			for l in da.getAllCalls():
				opnd = GetOpnd(long(l), 0)
				address = LocByName(opnd)
				if address:
					while GetMnem(address) == 'jmp':
						opnd = GetOpnd(address, 0)
						address = LocByName(opnd)	
					#print address
					if address and address != 0xffffffffffffffff:
						if get_func(address):
							#target = hex(address)[:-1]
							#print target
							targetTypes = self.getKnownTypes(address,d+1)
							#if target in allConstructors:
							def setTypeForInitialRegister(line,regName,regVal,types,da,targetTypes):
								if regVal in targetTypes.keys():
									currStates = da.getStatesPerLine()[l]
									prevStates = []
									for s in currStates:
											prevStates.extend(s.previous)
									objects = set(map(lambda x: x.registers[regName],prevStates)) # object is in 'this' pointer which resides in ecx
									#for c in constructors.keys():
									#	if target in constructors[c]:
									for o in objects:
										if o not in types.keys():
											types[o] = set()
										types[o] = types[o].union(targetTypes[regVal])	
							setTypeForInitialRegister(l,'eax','EAX0',types,da,targetTypes)
							setTypeForInitialRegister(l,'ebx','EBX',types,da,targetTypes)
							setTypeForInitialRegister(l,'ecx','ECX0',types,da,targetTypes)
							setTypeForInitialRegister(l,'edx','EDX0',types,da,targetTypes)
							setTypeForInitialRegister(l,'edi','EDI',types,da,targetTypes)
							setTypeForInitialRegister(l,'esi','ESI',types,da,targetTypes)			
							def setTypeForFinalRegister(line,regName,types,da,targetTypes, targetDa):
								#states = set()
								states = []
								for s in targetDa.getExitPointStates():
									#states = states.union(s)
									states.extend(s)
								#regVals = map(lambda s: s.registers[regName], states)
								regVals = set(map(lambda s: s.registers[regName], states))
								if len(filter(lambda v: v not in targetTypes.keys(), regVals)) == 0:
									regTypes = map(lambda v: targetTypes[v], regVals)
									typeVal = None
									for t in regTypes:
										if not typeVal:
											typeVal = t
										else:
											typeVal = typeVal.intersection(t)
									if typeVal:
										if len(typeVal) > 0:
											states = da.getStatesPerLine()[line]
											objects = set(map(lambda x: x.registers[regName],states)) # object is in 'this' pointer which resides in ecx
											for o in objects:
												if o not in types.keys():
													types[o] = set()
												types[o] = types[o].union(typeVal)
							targetDa = getDataAnalyzer(address)
							setTypeForFinalRegister(l,'eax',types,da,targetTypes,targetDa)
							setTypeForFinalRegister(l,'ebx',types,da,targetTypes,targetDa)
							setTypeForFinalRegister(l,'ecx',types,da,targetTypes,targetDa)
							setTypeForFinalRegister(l,'edx',types,da,targetTypes,targetDa)
							setTypeForFinalRegister(l,'edi',types,da,targetTypes,targetDa)
							setTypeForFinalRegister(l,'esi',types,da,targetTypes,targetDa)													
		from vtables import VirtualTableFinder
		from analysisUtils import getAllAllocatedVirtualTables
		allocated = getAllAllocatedVirtualTables()
		#print map(lambda x:hex(x),allocated)
		vts = VirtualTableFinder().find_virtual_tables()
		vts = map(lambda v:vts[v],allocated)
		vteas = map(lambda x: x.ea, vts)
		# collect objects to which virtual table is assigned
		for l in da.getStatesPerLine().keys():
			states = da.getStatesPerLine()[l]
			for s in states:
				for m in s.memory.keys():
					try:
						if int(s.memory[m][:-1],16) in vteas:
							if m not in types.keys():
								types[m] = set()
							types[m].add(s.memory[m][:-1])
					except ValueError:
						pass
				for m in s.stack.keys():
					try:
						if int(s.stack[m][:-1],16) in vteas:
							if m not in types.keys():
								types[m] = set()
							types[m].add(s.stack[m][:-1])
					except ValueError:
						pass
		vfuncs = set()
		for vt in vts:
			if f in map(lambda x:x.ea,vt.functions):
				vfuncs.add('0'+hex(vt.ea)[2:])
				#print hex(vt.ea)[:-1]
				#for func in vt.functions:
				#	print '\t'+str(func)
		if len(vfuncs) != 0:
			types['ECX0'] = vfuncs
		TypeFinder.typesCache[f][d] = types
		return types

if __name__ == '__main__':
	#from analysisUtils import getDataAnalyzer
	#da = getDataAnalyzer(ScreenEA())
	start = time()
	print TypeFinder().getKnownTypes(ScreenEA())
	end = time()
	print 'runtime: ' + str(end-start)
	print 'Done!'