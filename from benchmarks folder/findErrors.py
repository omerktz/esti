from re import match
from pymongo import MongoClient
db = MongoClient()['OmerK_Types_ShowTraf_exe'].untypedsignatures
def filterObjects(o):
	if o.startswith('[EBP'):
		return False
	def is_object(o):
		if '[' not in o and ']' not in o:
			return True
		matched = match('^\[(.+)\]([\+\-]0[0-9a-fA-F]+h)?$',o)
		if matched:
			return is_object(matched.groups()[0])
		else:
			return False
	return is_object(o)
for f in db.distinct('function'):
	for o in db.find({'function':f}).distinct('object'):
		if filterObjects(o):
			if max(db.find({'function':f,'object':o}).distinct('k'))<3:
				print f+'\t'+o
