from imports import *

"""
# gpref.exe
db = getDB("gperf.xml")
db.hierarchy.insert({"binary":"gperf.xml","type":"KeywordExt_Factory","parent":"Keyword_Factory"})
db.hierarchy.insert({"binary":"gperf.xml","type":"Output_Expr1","parent":"Output_Expr"})
for t in ["Output_Compare_Memcmp","Output_Compare_Strcmp","Output_Compare_Strncmp"]:
	db.hierarchy.insert({"binary":"gperf.xml","type":t,"parent":"Output_Compare"})
for t in [ "Output_Defines","Output_Enum"]:
	db.hierarchy.insert({"binary":"gperf.xml","type":t,"parent":"Output_Constants"})
"""
"""
# tinyxml.obj
db = getDB("tinyxml.obj")
for t in ["TiXmlNode","TiXmlAttribute"]:
	db.hierarchy.insert({"binary":"tinyxml.obj","type":t,"parent":"TiXmlBase"})
for t in [ "TiXmlElement" , "TiXmlText" , "TiXmlDocument" , "TiXmlUnknown" , "TiXmlDeclaration" , "TiXmlComment"]:
	db.hierarchy.insert({"binary":"tinyxml.obj","type":t,"parent":"TiXmlNode"})
"""

for b in getDB().database_names():
	if b.startswith('OmerK_Types_'):
		b = b[12:]
		db = getDB(b)
		objs = {}
		for r in db.typed.find():
			if db.hierarchy.find({"binary":r["binary"],"type":r["typename"]}).count() == 0:
				b = r['binary']
				b = b[:b.rfind('_')]
				if b not in objs.keys():
					objs[b] = set()
				objs[b].add(r['typename'])
		for b in objs.keys():
			for t in objs[b]:
				p = raw_input("("+b+") Parent of "+t+": ")
				if len(p.strip()) != 0:
					db.hierarchy.insert({"binary":b+'_Debug',"type":t,"parent":p})
					db.hierarchy.insert({"binary":b+'_Release',"type":t,"parent":p})