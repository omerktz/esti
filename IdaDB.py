from imports import *

class FunctionsDB(object):

	def __init__(self):
		self._functions = {}
		self._function_analyzer = CodeStructureAnalyzer()

	def clear(self):
		self._functions.clear()

	def reload(self, ea):
		self._functions[ea] = self._load_function(ea)

	def _get_func_ea(self, ea):
		f = get_func(ea)
		if not f:
			return BADADDR
		else:
			return f.startEA

	def _load_function(self, ea):
		f_ea = self._get_func_ea(ea)


		# Check for bad ea
		if f_ea == BADADDR:
			raise NoSuchFunctionException("No such function 0x%x" % f_ea)

		# return the function.
		return IdaFunction(GetFunctionName(f_ea), self._function_analyzer.getBasicBlockGraph(f_ea))

		# Get the function's graph.
		# FuncGraph = GetFunctionGraph(f_ea)

		# pred_2str(FuncGraph)
		# CreateFuncGraphs(FuncGraph)
		# return CreateIGraph(FuncGraph)
		
	def __getitem__(self, ea):
		# get the start ea
		f_ea = self._get_func_ea(ea)

		# Check for bad ea
		if f_ea == BADADDR:
			class NoSuchFunctionException(Exception):
				def __init__(self, value):
					self.value = value
				def __str__(self):
					return repr(self.value)
			raise NoSuchFunctionException("No such function 0x%x" % f_ea)

		# check if we have this function
		func = self._functions.get(f_ea)

		if func:
			return func
		else:
			return self._load_function(f_ea)

	def _load_all_functions(self):
		for ea in Functions():
			if not ea in self._functions:
				self._functions[ea] = self._load_function()


	def keys(self):
		self._load_all_functions()
		return self._functions.keys()

	def addresses(self):
		return self.keys()

	def iteritems(self):
		self._load_all_functions()
		return self._functions.iteritems()

	def values(self):
		return self.__iter__()

	def functions(self):
		return self.values

	def __iter__(self):
		self._load_all_functions()
		return self._functions.values()

	def __delitem__(self, ea):
		del self._functions[self._get_func_ea(ea)]

	def __repr__(self):
		return self._functions.__repr__()


class IdaFunction:

	def __init__(self, name, graph):
		self.name = name
		self.graph = graph

	def getGraph(self):
		return self.graph

	def __repr__(self):
		return "Function %s" % self.name


class BasicBlock:

	def __init__(self, start_addr, end_addr, code ):
		self.start_addr = start_addr
		self.end_addr = end_addr
		self.code = code

	def id(self):
		return self.start_addr

	def __hash__(self):
		return self.id()

	def __eq__(self, other):
		return isinstance(other, BasicBlock) and self.id() == other.id()

	def getCode(self):
		return self.code
#   def __iter__(self):
#   		for op in self.ops:
#   			yield op

	def __str__(self):
		return self.code


class Edge:
	def __init__(self, start_vertex, end_vertex, is_backedge = False,data=None):
		self.start_vertex = start_vertex
		self.end_vertex = end_vertex
		self.data = data
		self.is_backedge = is_backedge
		self.traversed_count = 0

	def copy(self):
		return Edge(self.start_vertex, self.end_vertex, self.is_backedge ,self.data)

	def source(self):
		return self.start_vertex

	def target(self):
		return self.end_vertex

	def sourceID(self):
		return self.start_vertex.id()

	def targetID(self):
		return self.end_vertex.id()

	def traverse(self):
		self.traversed_count += 1
		return self.traversed_count
		
	def __hash__(self):
		return hash(str(self))

	def __eq__(self, other):
		return isinstance(other, Edge) and self.start_vertex == other.start_vertex and self.end_vertex == other.end_vertex

	def __repr__(self):
		return "(0x%x) -> (0x%x)" % (hash(self.start_vertex), hash(self.end_vertex))

class BasicBlockGraph:

	def __init__(self, startEA):
		self.startEA=startEA

		# Dictionary of addresses to basic blocks		
		self.basic_blocks = {}
		# The outgoing edges for a basic block identified by its
		# starting address. A dictionary of addresses to a list of
		# addresses of basic blocks that the address may branch to		
		self.bb_out_edges = {}
		# The incoming edges for a basic block identified by its
		# starting address. As above.		
		self.bb_in_edges = {}

		self.edges = {}

	def __iter__(self):
		#for addr, bb in self.basic_blocks.items():
		for _, bb in self.basic_blocks.items():
			yield bb
	
	def calculateBackedges(self):
		def _calculateBackedges(v, seen):
			seen.add(v)
			for edge in self.bb_out_edges.get(v.start_addr,[]):
				if not edge.is_backedge:
					if edge.target() in seen:
						edge.is_backedge = True
					else:
						edge.is_backedge = False
						_calculateBackedges(edge.target(), seen)
			seen.remove(v)
		_calculateBackedges(self.basic_blocks.get(self.startEA), set())
		# blocks = [] 
		# seen_blocks = set(blocks[:])
		# while blocks:	   
		#	 block = blocks.pop()
		#	 for edge in self.bb_out_edges.get(block.start_addr,[]):
		#		 if edge.target() in seen_blocks:
		#			 edge.is_backedge = True
		#			 print edge
		#		 else:
		#			 edge.is_backedge = False
		#			 seen_blocks.add(edge.target())
		#			 blocks.append(edge.target())


	def edges(self):
		for edge in self.edges.keys():
			yield edge

	def getEdge(self, source, target):
		return self.edges.get(Edge(source, target))

	def _getBBSet(self, bbs):
		new_bbs = set([])
		for bb in bbs:
			if isinstance(bb, BasicBlock):
				bb = bb.start_addr # self.basic_blocks.get(bb.start_addr)
			new_bbs.add(bb)
		return new_bbs	  

	def _find_graph(self, bbs, seen=set(), edges={}):
		parents = self._getBBSet(bbs)
		seen = parents | seen
		children = set()
		for bb in parents:			
			for edge in edges.get(bb,[]):
				if not edge in seen:
					children.add(edge.targetID)
		if not children:
			return seen

		return self._find_graph(children, seen, edges)		
	
	def findParents(self, bbs):
		return self._find_graph(bbs, edges=self.bb_in_edges)

	def findChildren(self, bbs):
		return self._find_graph(bbs, edges=self.bb_out_edges)											

	def buildReachablityMap(self, bbs):
		rmap = {}
		bbs = self._getBBSet(bbs)

		for bb in bbs:
			bb_set = rmap.setdefault(bb, set())
			bb_set.add(bb)
			parents = self.findParents([bb])
			for parent in parents:
				parent_set = rmap.setdefault(parent, set())
				parent_set.add(bb)
		return rmap
				


	def getSubGraph(self, bbs):
		bbs = self._getBBSet(bbs)
		new_basic_blocks = {}
		new_bb_in_edges  = {}
		new_bb_out_edges = {}
		for bb in bbs:
			new_basic_blocks[bb] = self.basic_blocks[bb]
			for in_edge in self.bb_in_edges.get(bb, []):
				if in_edge.source() in bbs:
					new_bb_in_edges.setdefault(bb, []).append(in_edge.copy())
					
			for out_edge in self.bb_out_edges.get(bb, []):
				if out_edge.target() in bbs:
					new_bb_out_edges.setdefault(bb, []).append(out_edge.copy())

		bb_graph = BasicBlockGraph()
		bb_graph.basic_blocks = new_basic_blocks
		bb_graph.bb_out_edges = new_bb_out_edges 
		bb_graph.bb_in_edges = new_bb_in_edges
		return bb_graph
		
	def getFullSubGraph(self, bbs):
		return self.getSubGraph(self.findParents(bbs) | self.findChildren(bbs))
		
				
class CodeStructureAnalyzer:

	def buildIncomingGraph(self, basic_blocks, out_graph):
		"""
		This function takes a dictionary of basic blocks and a
		dictionary describing outgoing edges from the basic blocks
		and constructs a dictionary of the incoming edges to each
		basic block. 
		"""

		bb_in_edges = {}
		
		for out_bb_addr in out_graph:
			for in_bb_addr in out_graph[out_bb_addr]:
				if in_bb_addr in bb_in_edges:
					bb_in_edges[in_bb_addr].append(out_bb_addr)
				else:
					bb_in_edges[in_bb_addr] = [out_bb_addr]

		return bb_in_edges
		
	def getBasicBlockCode(self, start, end):
		addr = start
		originalCode = ""
		# subroutine marked as ended one line after the real end.
		while (addr<end):
			originalCode += getOriginalCodeFromAddr(addr)
			addr=NextHead(addr)
		return originalCode

	def getBasicBlockGraph(self, start_addr):
##		work_list = []
		bb_out_edges = {}
		#bb_in_edges = {}
		basic_blocks = {}
		edges = {}
		
		for bb in FlowChart(get_func(start_addr)):
			new_bb = BasicBlock(bb.startEA, PrevHead(bb.endEA), self.getBasicBlockCode(bb.startEA, bb.endEA))			
			# new_bb.end_op = self.imm.disasm(new_bb.end_addr, DISASM_FILE)
			basic_blocks[bb.startEA] = new_bb
			outs = bb_out_edges.setdefault(bb.startEA, [])
			for suc_bb in bb.succs():				
				outs.append( suc_bb.startEA )
			if not len(outs):
				del bb_out_edges[bb.startEA]
			#print hex(bb.startEA), str(map(hex, outs))

		bb_in_edges = self.buildIncomingGraph(basic_blocks,
												bb_out_edges)
				
		bb_graph = BasicBlockGraph(start_addr)

		actual_bb_out_edges = {}
		for bb, outs in bb_out_edges.iteritems():
			targets = actual_bb_out_edges.setdefault(bb, [])
			for target in outs:
				edge = Edge(basic_blocks[bb], basic_blocks[target])
				edges[edge] = edge
				targets.append(edge)


		actual_bb_in_edges = {}
		for bb, ins in bb_in_edges.iteritems():
			targets = actual_bb_in_edges.setdefault(bb, [])
			for target in ins:
				edge = Edge(basic_blocks[target], basic_blocks[bb])
				targets.append(edges.setdefault(edge, edge))

		bb_graph.basic_blocks = basic_blocks
		bb_graph.bb_out_edges = actual_bb_out_edges
		bb_graph.bb_in_edges = actual_bb_in_edges
		bb_graph.edges = edges


		return bb_graph
"""
members={}
def getMembers(ea):
	global members
	name = GetFunctionName(ea)
	if name not in members.keys():
		innerMembers = {}
		stack = GetFrame(ea)
		size = GetStrucSize(stack)
		for i in xrange(size):
			n = GetMemberName(stack,i)
			if n != None:
				n = n
				if n not in innerMembers.keys():
					innerMembers[n] = i
		members[name] = innerMembers
	return members[name]
"""

def str2int(s):
	base = 10
	if s.endswith('h'):
		base = 16
		s=s[:-1]
	if s.startswith('0x'):
		base = 16
		s=s[2:]
	if s.endswith('b'):
		base = 2
		s=s[:-1]
	return int(s,base)

def getNextOperand(string):
	plus = len(string)
	if '+' in string:
		plus = string.index('+')
	minus = len(string)
	if '-' in string:
		minus = string.index('-')
	index = plus
	coeff = 1
	if plus > minus:
		index = minus
		coeff = -1
	op = string[:index]
	rest = string[index+1:]
	return (op,coeff,rest)

"""
def rewriteOperand(op,ea):
	op = op
	if "[" in op and "]" in op: 
		prefix = op[:op.index("[")]
		rest = op[op.index("[")+1:op.rindex("]")].replace(" ","")
		first = True
		val = 0
		coeff = 1
		membs = getMembers(ea)
		last = "0"
		while len(rest) != 0:
			(o,c,r) = getNextOperand(rest)
			rest = r
			if first:
				if o != "esp":
					return op
				first = False
			else:			 
				if o in membs.keys():
					val += membs[o]
					last = "0"
				else:
					try:
						val += coeff*str2int(last)
					except ValueError:
						return op
					last = o
			coeff = c
		return prefix + "[esp+" + hex(val)[2:] + "h]"
	return op
"""
"""
#ida adds comments that we dont want for the compare...
def getOriginalCodeFromAddr(ea):
	line = GetMnem(ea)
	#print ea
	OpHex(ea,0)
	op1 = GetOpnd(ea,0)
	OpOff(ea,0, 0)
	OpHex(ea,1)
	op2 = GetOpnd(ea,1)
	OpOff(ea,1, 0)

	if op1 != "":
		#op1 = rewriteOperand(op1,ea)
		line += " " + op1
		if op2 != "":
			#op2 = rewriteOperand(op2,ea)
			line += "," + op2
	line += ";"
	return line
"""
from distorm3 import *
def getOriginalCodeFromAddr(ea):
	line = GetMnem(ea)
	#print ea
	# op1 = GetOpnd(ea,0)
	# op2 = GetOpnd(ea,1)
	op1 = ""
	op2 = ""
	insts = [x for x in Decompose(ea, GetManyBytes(ea, 16), Decode32Bits)]
	if len(insts) > 0:
		inst = insts[0]
		# OMER: my code use upper case names of registers to denote initial values
		#	   and lower case to denote current values. the strings returned by the
		#	   code below are upper case. i added a call to the lower function for
		#	   each operand to make the new code conform to old code behaviour. it
		#	   should not cause errors since all operands should be composed of
		#	   registers and immediate values (obviously none are in upper case).
		if len(inst.operands) > 0:
			op1 = str(inst.operands[0]).lower()
		if len(inst.operands) > 1:
			op2 = str(inst.operands[1]).lower()
		# OpHex(ea,0)
		# op1 = GetOpnd(ea,0)
		# OpHex(ea,1)
		# op2 = GetOpnd(ea,1)

	else:
		OpHex(ea,0)
		op1 = GetOpnd(ea,0)
		OpOff(ea,0,0)
		OpHex(ea,1)
		op2 = GetOpnd(ea,1)
		OpOff(ea,1,0)
		if match('^[0-9a-fA-F]+h$',op1):
			op1 = '0x'+op1[:-1]
		if match('^[0-9a-fA-F]+h$',op2):
			op2 = '0x'+op2[:-1]

	if op1 != "":
		line += " " + op1
		if op2 != "":
			line += "," + op2

	line += ";"
	return line

