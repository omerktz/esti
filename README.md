This repository contains the implementation of Esti -- a tool for statistically estimating types in binaries and resolving indirect call targets.
  
 This tool is described in the POPL 2016 submission ["Estimating Types in Binaries using Predictive Modeling"](https://dl.acm.org/citation.cfm?doid=2837614.2837674).
  
 Disclaimer: this is academic code used as a quick-and-dirty proof-of-concept for the evaluation of the ideas in the paper. It is unoptimized, inefficient, ugly and probably unreadable. We do not offer any guarantees, warranty or support for it. Use it at your own risk.
 
 The code is published under the [CRAPL](http://matt.might.net/articles/crapl/) license