from imports import *

def getTypenames():
	#binary = "Animal.exe"
	cache = {}
	i = 0
	for binary in sorted(db.untypedsignatures.distinct('binary')):
		db = getDB(binary)
		for r in db.untypedsignatures.find({"binary": binary, "realtype": "Unknown"}).sort("function",1):
			typename = "Unknown"
			obj = r["binary"]+'.'+r["function"]+'.'+r["object"]
			if obj in cache.keys():
				typename = cache[obj]
			else:
				if db.untypedsignatures.find({"binary": r["binary"], "function": r["function"], "object": r["object"], "realtype": {"$ne": r["realtype"]}}).count() > 0:
					typename = db.untypedsignatures.find({"binary": r["binary"], "function": r["function"], "object": r["object"], "realtype": {"$ne": r["realtype"]}}).distinct("realtype")[0]
				else:
					typename = raw_input(str(i)+"\t("+r["binary"]+") Give type name of object "+r["object"]+" of function "+r["function"]+": ")
					i += 1
					if len(typename.strip()) == 0:
						typename = "Unknown"
				cache[obj] = typename
			db.untypedsignatures.update({"_id":r["_id"]},{"$set": {"realtype": typename}})
getTypenames()

