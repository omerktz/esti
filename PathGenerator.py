from imports import *

class PathGenerator():
	# create path of blocks
	def extendBlockPaths(self,b,p):
		def countEdge(p,e):
			# count appearance of edge e in path p
			p = ' '.join(map(str, p))
			e = ' '.join(map(str, e))
			return p.count(e)
		global paths
		# don't allow an edge to appear twice
		if (len(p) == 0) or (countEdge(p,[p[len(p)-1],b]) <= 1):
			successors = False
			p = p[:]+[b]
			for s in b.succs():
				successors = True
				for path in self.extendBlockPaths(s,p):
					yield path
			if not successors:
				if p not in paths:
					paths.append(p)
					yield p
	
	# replace blocks with sequence of inner commands				
	def inflateBlocks(self,p):
		from FunctionProvider import LineId
		for b in p:
			b.start_addr = b.startEA
			i = 0
			while i < (b.endEA-b.startEA):
				l = LineId(b,i)
				yield l
				i += ItemSize(long(l))
					
	def getAllPathsInFunc(self,ea):
		for p in self.extendBlockPaths(FlowChart(get_func(ea))[0],[]):
			yield self.inflateBlocks(p)
	
if __name__ == '__main__':
	for p in PathGenerator().getAllPathsInFunc(ScreenEA()):
		path = []
		for l in p:
			path.append(hex(l)[:-1])
		print path
	print 'Done!'