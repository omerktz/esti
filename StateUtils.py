from imports import *

# replace all instances of src in str with dst. replace only full words.
def replaceAll(src,dst,rep):
	def prepareStr(s):
		return ' '+s.replace('*',' * ').replace('+',' + ').replace('-',' - ').replace('[',' [ ').replace(']',' ] ').replace('(',' ( ').replace(')',' ) ').replace('&',' & ').replace(',',' , ')+' '
	return prepareStr(rep).replace(prepareStr(src),' '+dst+' ').replace(' ','')

#simplifyTime = 0
# return simplified expression of v
cachedExpressions = {}
def simplifyExpression(v):
	# convert all numbers in e to hex format
	def convertNumsToHex(e):
		parts = split('[\*\+\-\[\]]',e)
		for p in parts:
			p = p.strip()
			if match('^[0-9]+$',p):
				val = hex(int(p))
				if val.endswith('L'):
					val = val[:-1]
				e = replaceAll(p,'0'+val[2:]+'h',e)
		return e	
	def notHex(n):
		# convert all hex numbers to regular numbers
		if n.endswith('h'):
			try:
				return str(int(n[:-1],16))
			except ValueError:
				pass
		return n
	#print v
	#global simplifyTime
	#originalSimplify = simplifyTime
	#startSimplify = time.time()
	#if len(v.strip()) == 0:
	#	return ''
	if len(v.strip()) == 0:
		return ''
	if v in cachedExpressions.keys():
		#endSimplify = time.time()
		#simplifyTime = originalSimplify + endSimplify-startSimplify
		return cachedExpressions[v]
	expression = ""
	children = {}
	rchildren = {}
	#addresses = {}
	i = 0
	part = ""
	# go over expression and split to parts
	while i < len(v):
		c = v[i]
		if c != '[':
			#if c!= '&':
			if c in  ['-','+','*','(',',',')','^']:
				expression += notHex(part) + c
				part = ""
			else:
				part += c
			#else:
			#	address = ""
			#	i += 1
			#	c = v[i]
			#	if c == '[':
			#		count = 1
			#		address += '['
			#		i += 1
			#		c = v[i]
			#		while count > 0:
			#			if c == '[':
			#				count += 1
			#			if c == ']':
			#				count -= 1
			#			address += c
			#			i += 1
			#			if i < len(v):
			#				c = v[i]
			#	else:
			#		while c != '-' and c != '+':
			#			address += c
			#			i += 1
			#			if i < len(v):
			#				c = v[i]
			#	a = 'a'+str(len(addresses))
			#	addresses[a] = address
			#	expression += a
			#	i -= 1
		else:
			# replace dereference with variable and simplify inner expression
			count = 1
			child = ""
			while count > 0:
				i += 1
				c = v[i]
				if c == '[':
					count += 1
				if c == ']':
					count -= 1
				if count != 0:
					child += c
			if child in rchildren.keys():
				expression += rchildren[child]
			else:
				x = 'x'+str(len(children))
				children[x] = child
				rchildren[child] = x
				expression += x
		i += 1
	expression += notHex(part)
	#print expression
	# replace special characters in expression with special markers
	uniqueAtString = '__At__'
	uniqueQuestionString = '__QuestionMark__'
	uniquePeriodString = '__Period__'
	uniqueDollarString = '__Dollar__'
	if uniqueAtString in expression:
		raise Exception('Unique string used instead of @ is not unique!')
	if uniqueQuestionString in expression:
		raise Exception('Unique string used instead of @? is not unique!')
	if uniquePeriodString in expression:
		raise Exception('Unique string used instead of . is not unique!')
	if uniqueDollarString in expression:
		raise Exception('Unique string used instead of $ is not unique!')
	expression = str(simplify(expression.replace('@',uniqueAtString).replace('.',uniquePeriodString).replace('?',uniqueQuestionString).replace('$',uniqueDollarString))).replace(uniqueDollarString,'$').replace(uniqueAtString,'@').replace(uniqueQuestionString,'?').replace(uniquePeriodString,'.')
	for x in children.keys():
		expression = replaceAll(x,'['+simplifyExpression(children[x])+']',expression)
	#for a in addresses.keys():
	#	expression = replaceAll(a,'&'+simplifyExpression(addresses[a]),expression)
	expression = convertNumsToHex(expression)
	cachedExpressions[v] = expression
	#endSimplify = time.time()
	#simplifyTime = originalSimplify + endSimplify-startSimplify
	return expression
	
def isValidExpr(o):
	count = 0
	for c in o:
		if c == '[':
			count += 1
		if c == ']':
			count -= 1
		if count < 0:
			return False
	return (count == 0)
	
# replace all known parts off expression val with their values and simplify result
#inlineTime = 0
def inlineValues(val,state):
	"""
	#global inlineTime
	#startInline = time.time()
	# next 3 lines were to added to allow simplification of address expressions prior to replacing them
	for r in state.registers.keys():
		val = replaceAll(r,state.registers[r],val)
	for r in state.stack.keys():
		val = replaceAll('['+r+']',state.stack[r],val)
	for r in state.memory.keys():
		val = replaceAll('['+r+']',state.memory[r],val)
	#for r in state.registers.keys():
	#	val = replaceAll(r,state.registers[r],val)
	val = simplifyExpression(val)
	#endInline = time.time()
	#inlineTime += endInline-startInline
	return val
	"""
	if len(val.strip()) == 0:
		return ''
	deref = False
	if val.startswith('[') and val.endswith(']'):
		if isValidExpr(val[1:-1]):
			deref = True
	for r in state.registers.keys():
		val = replaceAll(r,state.registers[r],val)
	val = simplifyExpression(val)
	if deref:
		address = val[1:-1]
		if address in state.stack.keys():
			return state.stack[address]
		if address in state.memory.keys():
			return state.memory[address]
	return val

def getPreviousState(states,l):
	currs = states[l]
	prevs = []
	for s in currs:
		prevs.extend(s.previous)
	return prevs

"""
def getPreviousStateByNum(states,l):
	currs = states[filter(lambda x: long(x) == l, states.keys())[0]]
	prevs = []
	for s in currs:
		prevs.extend(s.previous)
	return prevs
"""

"""		
def printTimes():
	global inlineTime
	global simplifyTime
	print 'inline: ' + str(inlineTime)
	print 'simplify: ' + str(simplifyTime)
def resetTimes():
	global inlineTime
	global simplifyTime
	inlineTime = 0
	simplifyTime = 0
"""