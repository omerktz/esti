import sys
from re import match
from time import time
from multiprocessing import Pool

# open mongoDB instance for binary
def getDB(binary = None):
	from pymongo import MongoClient
	while True:
		try:
			db = MongoClient()
			break
		except:
			from time import sleep
			sleep(10)
	if binary:
		if '.' in binary:
			binary = binary.replace('.','_')
		if ' ' in binary:
			binary = binary.replace(' ','_')
		#print binary
		db = db['OmerK_Types_'+binary]
	return db

# calculate appropriate cost for an edit based on edited actions (default is 10)
costReadWriteReplace = 5
def getEditCost(t1,t2):
	global costReadWriteReplace
	if t1==t2:
		return 0
	if (t1.startswith('Read') or t1.startswith('Write')) and (t2.startswith('Read') or t2.startswith('Write')):
		if t1[t1.find('Offset'):] == t2[t2.find('Offset'):]:
			return costReadWriteReplace
	return 10

# based on internet version of damarau-levenshtein
# added support for different weights for different edits
# arguments:
#	seq1 & seq2: the tracelets in array form (split by ';')
#	last: last/minimum score, distance is minimum between calculated distance and this argument 
def damerau_levenshtein_distance((seq1, seq2)):
	# Conceptually, this is based on a len(seq1) + 1 * len(seq2) + 1 matrix.
	# However, only the current and two previous rows are needed at once,
	# so we only store those.
	oneago = None
	seq1firstIsOptional = int(not seq1[0].startswith('ReturnedFromFunction-'))
	seq2firstIsOptional = int(not seq2[0].startswith('ReturnedFromFunction-'))
	thisrow = map(lambda n:(n*10,0),range(seq2firstIsOptional, len(seq2) + seq2firstIsOptional)) + [(0,0)]
	for x in xrange(len(seq1)):
		# Python lists wrap around for negative indices, so put the
		# leftmost column at the *end* of the list. This matches with
		# the zero-indexed strings and saves extra calculation.
		twoago, oneago, thisrow = oneago, thisrow, [(0,0)] * len(seq2) + [((x + seq1firstIsOptional)*10,0)]
		for y in xrange(len(seq2)):
			delcost = (oneago[y][0] + 10,oneago[y][1])
			addcost = (thisrow[y - 1][0] + 10,thisrow[y - 1][1])
			subcost = (oneago[y - 1][0] + getEditCost(seq1[x],seq2[y]),oneago[y - 1][1]+2*(seq1[x]!=seq2[y] and seq1[x].startswith('CallFunction-') and seq2[y].startswith('CallFunction-')))
			thisrow[y] = min(delcost, addcost, subcost)
			# This block deals with transpositions
			if (x > 0 and y > 0 and seq1[x] == seq2[y - 1]
				and seq1[x-1] == seq2[y] and seq1[x] != seq2[y]):
				thisrow[y] = min(thisrow[y], (twoago[y - 2][0] + 10,twoago[y - 2][1]))
	return thisrow[len(seq2) - 1]

# add to each type the tracelets of his parent types (as declared in hierarchy db)
# currently doesn't do anything because hierarchy in db is empty
# returns (type, set of tracelets)
def concatParents((b,t,targets)):
	db = getDB(b).hierarchy
	types = set()
	nextTypes = set([t])
	result = set()
	while len(nextTypes) != 0:
		typeVals = nextTypes.pop()
		if typeVals not in types:
			types.add(typeVals)
			nextTypes = nextTypes.union(set(db.find({"binary":b,"type":typeVals}).distinct('parent')))
			result = result.union(targets[typeVals])
	return (t,result)

# add to each object the tracelets of it's (transitively) sibling object (as identified by analysis)
# returns mapping from k name to set of tracelets of length k
def concatSiblings(b,c,candidates):
	db = getDB(b).siblings
	result = {}
	function = c[:c.rfind('.')]
	siblings = set([c])
	checked = set()
	while len(siblings) != 0:
		x = siblings.pop()
		checked.add(x)
		if x in candidates.keys():
			for k in candidates[x].keys():
				if k not in result.keys():
					result[k] = set(candidates[x][k])
				else:
					result[k] = result[k].union(set(candidates[x][k]))
		for r in db.find({"binary":b,"function":function,"object":x[x.rfind('.')+1:]}).distinct('sibling'):
			r = function+'.'+r
			if r not in checked:
				siblings.add(r)
	return result

# for determination of valid actions
# assume no field is read only, make sure all fields have valid read and write actions to them
# return set of all valid single actions
#def extendK1s(k1s):
#	k1s = set(map(lambda c:c[:-1],k1s))
#	added = set()
#	for k in k1s:
#		if k.startswith('ReadField'):
#			added.add('Write'+k[4:])
#		if k.startswith('WriteField'):
#			added.add('Read'+k[5:])
#	return k1s.union(added)

# load signature of type from db
# return (type, set of tracelets, set of single actions)
def loadTypeSignature((b,t)):
	db = getDB(b).typesignatures
	return (t,db.find({'binary':b,'typename':t}).distinct('code'))

# load signature of object from db
# return (object, set of tracelets, real type (according to db if exist), mapping of possible types to base ranks/penalties, set of single actions)
def loadObjectSignature((b,f,o)):
	db = getDB(b).untypedsignatures
	def getCodes():
		ks = {}
		codes = {}
		for k in db.find({'binary':b,'function':f,'object':o}).distinct('k'):
			codes[k] = db.find({'binary':b,'function':f,'object':o,'k':k}).distinct('code')
			ks[k] = db.find({'binary':b,'function':f,'object':o,'k':k}).count()
		return (ks,codes)
	data = getCodes()
	def getMapping():
		mapping = {}
		for r in getDB(b).mapping.find({'binary':b,'function':f,'object':o}):
			mapping[r['type']] = r['base']
		return mapping
	return (f+'.'+o,data[1],db.find({'binary':b,'function':f,'object':o}).distinct('realtype')[0],getMapping(),data[0])

# load signatures for all objects in function
def loadFunctionSignatures(b,f,pool):
	db = getDB(b).untypedsignatures
	return pool.map_async(loadObjectSignature,map(lambda o:(b,f,o),db.find({'binary':b,'function':f}).distinct('object')))

# calculate match score between to tracelets
def calcTraceTraceMatch(partsI,j,score):
	partsJ = j.split(';')[:-1]
	#if len(set(partsI).intersection(set(partsJ))) == 0:
	#	returnm in(max(len(partsI),len(partsJ))*10,score)
	return damerau_levenshtein_distance(partsI,partsJ,score)
		
# calculate match score between a tracelet and a type
# puts result in coverage map argument
distancePenalty = 20
def calcTraceTypeMatch(i,t,coverage):
	global costReadWriteReplace
	global distancePenalty
	if i in t:
		coverage[i] = 0
		return
	partsI = i.split(';')[:-1]
	lenI = len(partsI)
	score = (len(partsI)*10,len(partsI))
	for j in sorted(map(lambda x:(x,abs(x.count(';')-lenI)),t),key=lambda y:y[1]):
		#if j[1]*costReadWriteReplace >= score:
		if (j[1]-1)*costReadWriteReplace >= score:
			break
		score = calcTraceTraceMatch(partsI,j[0],score)
	score = score[0]+(score[1]*distancePenalty)
	coverage[i] = score#/float(pow(len(partsI),2))

# calculate match score between object and type
# returns sum of scores for all object tracelets with that type		
def calcCoverageMatch(c,t):
	coverage = {}
	for i in c:
		calcTraceTypeMatch(i,t,coverage)
	return sum(coverage.values())

# calculate match score between object and type
# return (object, map between k values and score for each value)
basePenalty = 150
def calcObjectTypeMatch((binary,oTraces,t,targets,base)):
	global basePenalty
	tTraces = set()
	if t in targets.keys():
		tTraces = targets[t]
	results = {}
	for k in oTraces.keys():
		results[k] = calcCoverageMatch(oTraces[k],tTraces)
	results['base'] = base*basePenalty
	return (t,results)

# split a set of tracelets according to their lengths
# returns a map from length k to set of tracelets of that length		
def splitByK(data):
	result = {}
	for d in data:
		k = d.count(';')
		if k not in result:
			result[k] = set([d])
		else:
			result[k].add(d)
	return result	
	
# calculate match scores between object and all types
def calcObjectMatches(binary,o,candidates,targets,mapping,pool):
	return pool.map_async(calcObjectTypeMatch,map(lambda t:(binary,concatSiblings(binary,o,candidates),t,targets,mapping[t]),mapping.keys()))
	
def matches_main():
	start = time()
	
	candidates = {}
	targets = {}
	types = {}
	mapping = {}
	ks = {}

	# read arguments
	if len(sys.argv) < 4:
		print 'Usage: python matches.py <binaryName> <# of processes> <output file>'
		sys.exit()
	binary = sys.argv[1]
	end = ''
	i = 0
	if binary.startswith('\''):
		end = '\''
	if binary.startswith('\"'):
		end = '\"'
	if end != '':
		binary = binary [1:]
		while not binary.endswith(end):
			i += 1
			if len(sys.argv) < (4+i):
				print 'Usage: python matches.py <binaryName> <# of processes> <output file>'
				sys.exit()
			binary += ' '+sys.argv[1+i]
		binary = binary[:-1]
	procs = int(sys.argv[2+i])
	outputfilename = sys.argv[3+i]
	end = ''
	if outputfilename.startswith('\''):
		end = '\''
	if outputfilename.startswith('\"'):
		end = '\"'
	if end != '':
		outputfilename = outputfilename [1:]
		while not outputfilename.endswith(end):
			i += 1
			if len(sys.argv) < (4+i):
				print 'Usage: python matches.py <binaryName><# of processes> <output file>'
				sys.exit()
			outputfilename += ' '+sys.argv[3+i]
		outputfilename = outputfilename[:-1]
	if len(sys.argv) != 4+i:
		print 'Usage: python matches.py <binaryName> <# of processes> <output file>'
		sys.exit()
	outputfile = open(outputfilename,'w')

	pool = Pool(processes=procs)

	db = getDB(binary)

	# load tracelets from db	
	processesTypes = pool.map_async(loadTypeSignature,map(lambda t:(binary,t),db.typesignatures.find({"binary":binary}).distinct('typename')))
	processesObjects = map(lambda f:loadFunctionSignatures(binary,f,pool),db.untypedsignatures.find({"binary":binary}).distinct('function'))

	for r in processesTypes.get():
		targets[r[0]]=r[1]
	for rs in processesObjects:
		for r in rs.get():
			obj = r[0][r[0].rfind('.')+1:]
			if not match('^\[EBP[^\[\]]*\]$',obj): # filter irrelevant objects
				candidates[r[0]]=r[1]
				types[r[0]]=r[2]
				mapping[r[0]]=r[3]
				ks[r[0]] = r[4]
		
	withParents = {}
	for r in pool.map(concatParents,map(lambda t:(binary,t,targets),targets.keys())):
		withParents[r[0]]=r[1] 
	
	# all data is now in memory
	
	outputfile.write('Number of objects: '+str(len(candidates.keys()))+'\n\n')
	
	# calculate ranking for all objects
	results = {}
	for r in map(lambda o:(o,calcObjectMatches(binary,o,candidates,withParents,mapping[o],pool,True)),candidates.keys()):
		o = r[0]
		results[o]={}
		for x in r[1].get():
			results[o][x[0]]=x[1]
	def calcScore(data):
		# convert map of scores to final score
		score = data['base']
		for k in filter(lambda k:k!='base',data.keys()):
			score += data[k]/float(pow(k,2))
		return score
	for o in sorted(results.keys()):
		outputfile.write(o+' ('+types[o]+')\n')
		#for t in sorted(results[o].keys(), key=lambda k:prepareVector(results[o][k])):
		#	outputfile.write('\t'+t+' : '+printVector(results[o][t])+'\n')
		for t in sorted(results[o].keys(), key=lambda k:calcScore(results[o][k])):
			outputfile.write('\t'+t+' : '+str(calcScore(results[o][t]))+'\n')
		outputfile.write('\n')
	outputfile.close()
	
	end = time()
	print 'runtime: ' + str(end-start)
		
	pool.close()
	pool.join()
	
	print 'Done!'
	
if __name__ == '__main__':
	matches_main()