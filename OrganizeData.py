from imports import *

"""
def propogateTypename():
	cache = {}
	i=0
	for b in db.typed.distinct("binary"):
		db = getDB(b)
		for r in db.typed.find({"binary":b,"typename": "Unknown"}):
			typename = "Unknown"
			if (r["binary"]+'.'+r["type"]) in cache.keys():
				typename = cache[r["binary"]+'.'+r["type"]]
			else:
				if db.typed.find({"binary": r["binary"], "type": r["type"], "typename": {"$ne": r["typename"]}}).count() > 0:
					typename = db.typed.find({"binary": r["binary"], "type": r["type"], "typename": {"$ne": r["typename"]}}).distinct("typename")[0]
				else:
					typename = raw_input(str(i)+"\t("+r["binary"]+") Give name to class at address "+r["type"]+": ")
					i+=1
				cache[r["binary"]+'.'+r["type"]] = typename
			db.typed.update({"_id":r["_id"]},{"$set": {"typename": typename}})
propogateTypename()
"""

def createTypeSignatures(b):
	db = getDB(b)
	sigs = {}
	types = {}
	for r in db.typed.find({'binary':b}):
		typeval = r["type"]
		typename = r["typename"]
		code = r["code"]
		if typeval not in types.keys():
			types[typeval] = typename
		else:
			if types[typeval] != typename:
				#raise Exception('2 addresses for same type')
				pass
		if b not in sigs.keys():
			sigs[b] = {}
		if typeval not in sigs[b].keys():
			sigs[b][typeval] = {}
		if code not in sigs[b][typeval].keys():
			sigs[b][typeval][code] = 0
		sigs[b][typeval][code] += 1
	for b in sigs.keys():
		for t in sigs[b].keys():
			for c in sigs[b][t].keys():
				db.typesignatures.insert({"binary":b,"typename":types[t],"type":t,"code":c,"k":c.count(';'),"appearances":sigs[b][t][c]})

def createUntypedSignatures(b):
	db = getDB(b)
	sigs = {}
	for r in db.untyped.find({'binary':b}):
		code = r["code"]
		func = r["function"]
		obj = r["object"]
		if b not in sigs.keys():
			sigs[b] = {}
		if func not in sigs[b].keys():
			sigs[b][func] = {}
		if obj not in sigs[b][func].keys():
			sigs[b][func][obj] = {}
		if code not in sigs[b][func][obj].keys():
			sigs[b][func][obj][code] = 0
		sigs[b][func][obj][code] += 1
	for b in sigs.keys():
		for f in sigs[b].keys():
			for o in sigs[b][f].keys():
				for c in sigs[b][f][o].keys():
					db.untypedsignatures.insert({"binary":b,"function":f,"object":o,"code":c,"k":c.count(';'),"appearances":sigs[b][f][o][c],"typeguess":"None","realtype":"Unknown"})

if __name__ == '__main__':
	for b in getDB().database_names():
		if b.startswith('OmerK_Types_'):
			b = b[12:]
			db = getDB(b)
			print 'Organizing '+b
			createTypeSignatures(b)
			createUntypedSignatures(b)

"""
def copyTypes(b):
	db = getDB(b)
	objects = set()
	for r in db.typed.find({'binary':b}):
		if (r["binary"]+'.'+r["function"]+'.'+r["object"]) not in objects:
			for u in db.untypedsignatures.find({"binary":r["binary"],"function":r["function"],"object":r["object"]}):
				db.untypedsignatures.update({"_id":u["_id"]},{"$set": {"realtype":r["typename"]}})
			objects.add(r["binary"]+'.'+r["function"]+'.'+r["object"])
copyTypes()
"""
