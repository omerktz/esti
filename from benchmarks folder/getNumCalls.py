from re import match
from pymongo import MongoClient
db = MongoClient()
import sys
fOut = open(sys.argv[2],'w')

def filterObjects(o):
	if o.startswith('[EBP'):
		return False
	def is_object(o):
		if '[' not in o and ']' not in o:
			return True
		matched = match('^\[(.+)\]([\+\-]0[0-9a-fA-F]+h)?$',o)
		if matched:
			return is_object(matched.groups()[0])
		else:
			return False
	return is_object(o)

	d = 'OmerK_Types_'+sys.argv[1].replace(' ','_').replace('.','_')
	for f in db[d].untypedsignatures.distinct('function'):
		for o in db[d].untyped.find({'function':f}).distinct('object'):
			if filterObjects(o):
				calls = set()
				for c in db[d].untyped.find({'function':f,'object':o,'k':1}):
					if c['code'].startswith('Call'):
						calls.add(c['trace'])
				fOut.write(d[12:]+'\t'+f+'\t'+o+'\t'+str(len(calls))+'\t'+str(calls)+'\n')

fOut.close()

