
package vmm;

import com.mongodb.DBObject;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Comparator;
import java.util.TreeMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
//import java.util.concurrent.CountDownLatch;
//import java.util.concurrent.ExecutorService;
//import java.util.concurrent.Executors;
//import java.util.concurrent.TimeUnit;

import vmm.DBWrapper.applyToResults;
import vmm.algs.DCTWPredictor;
import vmm.algs.PPMCPredictor;
import vmm.pred.VMMPredictor;

public class Matcher {

	private static final String outputDir = "matchResults/";
	private class ObjectData {
		protected String realtype;
		protected Set<IntSequence> codes;
		protected Map<String,Integer> mapping;
	}

	private final ConcurrentMap<String,VMMPredictor> predictors;
	private final ConcurrentMap<String,ObjectData> objects;
	private final DBWrapper db;
	private final String binary;
	private final SequenceEncoder seqEnc;
	private final int numActions;
	private final int maxLength;
	
	//private final int numThreads;
	
	public Matcher(String binary, int numThreads) {
		this.binary = binary;
		this.predictors = new ConcurrentHashMap<String,VMMPredictor>();
		this.objects = new ConcurrentHashMap<String,ObjectData>();
		this.db = new DBWrapper(binary);
		this.seqEnc = new SequenceEncoder();
		this.numActions = countCodes();
		this.maxLength = getMaxK();
		//this.numThreads = numThreads;
		loadTypeSignatures();
		getObjectTracelets();
	}
	
	private VMMPredictor getNewPredictor() {
		PPMCPredictor p = new PPMCPredictor();
		//DCTWPredictor p = new DCTWPredictor();
		p.init(numActions, maxLength-1);
		return p;
	}
	
	
	private Map<String,Object> getNewQuery() {
		ConcurrentMap<String,Object> query = new ConcurrentHashMap<String,Object>();
		query.put("binary",this.binary);	
		return query;
	}
	
	private int countCodes() {
		Map<String,Object> query = getNewQuery();
		query.put("k", 1);
		Set<String> tracelets1 = db.distinctInCollection("typesignatures", "code", query);
		tracelets1.addAll(db.distinctInCollection("untypedsignatures", "code", query));
		return tracelets1.size();
	}
	
	private int getMaxK() {
		Map<String,Object> query = getNewQuery();
		Set<Integer> ks = db.distinctIntegersInCollection("typesignatures", "k", query);
		ks.addAll(db.distinctIntegersInCollection("untypedsignatures", "k", query));
		int maxK = 0;
		for (Integer k: ks) {
			if (k > maxK) {
				maxK = k;
			}
		}
		return maxK;
	}
	
	private void loadTypeSignatures() {
		Map<String,Object> query = getNewQuery();
		final ConcurrentMap<String,Set<String>> codes = new ConcurrentHashMap<String,Set<String>>();
		//ExecutorService executor = Executors.newFixedThreadPool(this.numThreads);
		for (final String type: db.distinctInCollection("typesignatures", "typename", query)) {
			//executor.execute(new Runnable() {
			//	@Override
			//	public void run() {
					Map<String,Object> innerQuery = getNewQuery();
					innerQuery.put("typename", type);
					final Set<String> data = new TreeSet<String>();
					final Set<String> paths = new TreeSet<String>();
					for (int k = 7; k > 0; k--) {
						final boolean lastK = (k == 1);
						innerQuery.put("k", k);
						db.findInCollectionAndApply("typed", innerQuery, new applyToResults() {
							@Override
							public void apply(DBObject obj) {
								String path = (String) obj.get("trace");
								if (!paths.contains(path)) {
									data.add((String) obj.get("code"));
								}
								paths.add(path);
								if (!lastK) {
									paths.add(path.substring(path.indexOf(',')+1));
									path = path.substring(0, path.length()-1);
									paths.add(path.substring(0,path.lastIndexOf(',')+1));
								}
							}
						});
						innerQuery.remove("k");
					}
					synchronized (codes) {
						codes.put(type, data);
					}
			//	}
			//});
		}
		//executor.shutdown();
		//try {
		//	executor.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
		//} catch (InterruptedException e) {
		//	e.printStackTrace();
		//}
		/*
		executor = Executors.newFixedThreadPool(this.numThreads);
		for (final String type: codes.keySet()) {
			executor.execute(new Runnable() {
				@Override
				public void run() {
					Map<String,Object> parentsQuery = getNewQuery();
					parentsQuery.put("type", type);
					Set<String> types = db.distinctInCollection("hierarchy", "parent", parentsQuery);
					parentsQuery.remove("type");
					Set<String> added = new TreeSet<String>();
					types.add(type);
					while (types.size() > 0) {
						String t = types.iterator().next();
						types.remove(t);
						if (!added.contains(t)) {
							added.add(t);
							parentsQuery.put("type", t);
							types.addAll(db.distinctInCollection("hierarchy", "parent", parentsQuery));
							parentsQuery.remove("type");
							codes.get(type).addAll(codes.get(t));
						}
					}
				}
			});
		}
		executor.shutdown();
		try {
			executor.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		*/
		//executor = Executors.newFixedThreadPool(this.numThreads);
		for (final String type: codes.keySet()) {
			//executor.execute(new Runnable() {
			//	@Override
			//	public void run() {
					final Set<IntSequence> codeSeqs = convertTracelets(codes.get(type));
					synchronized (predictors) {
						final VMMPredictor predictor = getNewPredictor();
						for (IntSequence seq: codeSeqs) {
							predictor.learn(seq);
						}
						predictors.put(type, predictor);			
					}
			//	}
			//});
		}
		//executor.shutdown();
		//try {
		//	executor.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
		//} catch (InterruptedException e) {
		//	e.printStackTrace();
		//}
	}
	
	private Set<IntSequence> convertTracelets(final Set<String> tracelets) {
		final Set<IntSequence> seqs = new TreeSet<IntSequence>();
		for (final String t: tracelets) {
			final String[] codes = t.split(";");
			int size = codes.length;
			if (codes[codes.length-1].trim().isEmpty()) {
				size -= 1;
			}
			final String[] trimmedCodes = new String[size];
			for (int i = 0; i< size; i++) {
				trimmedCodes[i] = codes[i].trim();
			}
			seqs.add(seqEnc.encode(trimmedCodes));
		}
		return seqs;
	}

	private boolean parenthsisCheck(String obj) {
		obj = obj.replaceAll("[^\\{\\}\\[\\]\\(\\)]", "");
		if (obj.length() %2 != 0) {
			return false;
		}
		String last;
		do {
			last = new String(obj);
			obj = obj.replaceAll("\\(\\)", "").replaceAll("\\[\\]", "").replaceAll("\\{\\}", "");
		} while (!obj.equals(last));
		return (obj.length() == 0);
	}
	
	private boolean filterObjects(String obj) {
		if (obj.matches("^\\[EBP[^\\[\\]]*\\]$")) {
			return false;
		}
		return parenthsisCheck(obj);
		//return true;
	}
	
	private void mergeObjectDatas(String obj1, String obj2) {
		if (obj1.equals(obj2)) {
			return;
		}
		if (!filterObjects(obj1.substring(obj1.indexOf('.')+1)) || !filterObjects(obj2.substring(obj2.indexOf('.')+1))) {
			return;
		}
		if (!this.objects.containsKey(obj1) || !this.objects.containsKey(obj2)) {
			return;
		}
		ObjectData data1 = this.objects.get(obj1);
		ObjectData data2 = this.objects.get(obj2);
		synchronized (data1) {
			synchronized (data2) {
				if (!(data1.codes.containsAll(data2.codes) && data2.codes.containsAll(data1.codes))) {
					data1.codes.addAll(data2.codes);
					data2.codes = data1.codes;
				}
				if (data1.mapping != data2.mapping) {
					Set<String> keys = data1.mapping.keySet();
					keys.retainAll(data2.mapping.keySet());
					Map<String,Integer> res = new TreeMap<String,Integer>();
					for (String key: keys) {
						Integer val = data1.mapping.get(key);
						if (data1.mapping.get(key) < data2.mapping.get(key)) {
							val = data2.mapping.get(key);
						}
						res.put(key, val);
					}
					data1.mapping = res;
					data2.mapping = res;
				}
			}
		}
	}
	
	private void getObjectTracelets() {
		final Map<String,Object> query = getNewQuery();
		//final ExecutorService executor = Executors.newFixedThreadPool(this.numThreads);
		//final CountDownLatch latchFuncs = new CountDownLatch(db.distinctInCollection("untypedsignatures", "function", query).size());
		for (final String f: db.distinctInCollection("untypedsignatures", "function", query)) {
			//executor.execute(new Runnable() {			
			//	@Override
			//	public void run() {
					Map<String,Object> innerQuery = getNewQuery();
					innerQuery.put("function",f);
					for (final String o: db.distinctInCollection("untypedsignatures", "object", innerQuery)) {
						//executor.execute(new Runnable() {
						//	@Override
						//	public void run() {
								if (filterObjects(o)){
									ObjectData data = new ObjectData();
									final Map<String,Object> finalQuery = getNewQuery();
									finalQuery.put("function",f);
									finalQuery.put("object",o);
									final String finalName = f+'.'+o;
									data.realtype = db.distinctInCollection("untypedsignatures", "realtype", finalQuery).iterator().next();
									final Set<String> code = new TreeSet<String>();
									final Set<String> paths = new TreeSet<String>();
									for (int k = 7; k > 0; k--) {
										final boolean lastK = (k == 1);
										finalQuery.put("k", k);
										db.findInCollectionAndApply("untyped", finalQuery, new applyToResults() {
											@Override
											public void apply(DBObject obj) {
												String path = (String) obj.get("trace");
												if (!paths.contains(path)) {
													code.add((String) obj.get("code"));
												}
												paths.add(path);
												if (!lastK) {
													paths.add(path.substring(path.indexOf(',')+1));
													path = path.substring(0, path.length()-1);
													paths.add(path.substring(0,path.lastIndexOf(',')+1));
												}
											}
										});
										finalQuery.remove("k");
									}
									data.codes = convertTracelets(code);
									final Map<String,Integer> mapping = new TreeMap<String,Integer>();	
									db.findInCollectionAndApply("mapping", finalQuery, new applyToResults() {
										@Override
										public void apply(DBObject obj) {
											String type = (String) obj.get("type");
											Integer base = (Integer) obj.get("base");
											mapping.put(type, base);						
										}
									});
									data.mapping = mapping;
									synchronized (Matcher.this.objects) {
										Matcher.this.objects.put(finalName, data);					
									}
								}								
						//	}							
						//});
					}
					//latchFuncs.countDown();
			//	}
			//});
		}
		//try {
		//	latchFuncs.await();
		//} catch (InterruptedException e) {
		//	e.printStackTrace();
		//}
		//executor.shutdown();
		//try {
		//	executor.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
		//} catch (InterruptedException e) {
		//	e.printStackTrace();
		//}
		//ExecutorService executor2 = Executors.newFixedThreadPool(this.numThreads);
		for (final String f: db.distinctInCollection("siblings", "function", getNewQuery())) {
			//executor2.execute(new Runnable() {
			//	@Override
			//	public void run() {
					Map<String,Object> siblingsQuery = getNewQuery();
					siblingsQuery.put("function",f);
					for (String o: db.distinctInCollection("siblings", "object", siblingsQuery)) {
						Map<String,Object> innerSiblingsQuery = getNewQuery();
						innerSiblingsQuery.put("function",f);
						innerSiblingsQuery.put("object", o);
						db.findInCollectionAndApply("siblings", innerSiblingsQuery, new applyToResults() {
							@Override
							public void apply(DBObject obj) {
								String obj1 = ((String)obj.get("function"))+'.'+((String)obj.get("object"));
								String obj2 = ((String)obj.get("function"))+'.'+((String)obj.get("sibling"));
								mergeObjectDatas(obj1,obj2);
							}
						});
					}
			//	}
			//});
		}
		//executor2.shutdown();
		//try {
		//	executor2.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
		//} catch (InterruptedException e) {
		//	e.printStackTrace();
		//}
	}

	private class valueComparator implements Comparator<String> {
		Map<String, Double> values;
		public valueComparator(Map<String,Double> vals) {
			this.values = vals;
		}
		@Override
		public int compare(String arg0, String arg1) {
			Double val0 = values.get(arg0);
			Double val1 = values.get(arg1);
			if (val0.equals(val1)) {
				return arg0.compareTo(arg1);
			}
			return val0.compareTo(val1);
		}
	}
	
	// probability multiplication
	public void matches() {
		final FileWriter output;
		try {
			output = new FileWriter(outputDir+binary+".matches");
			output.write("Number of objects: "+this.objects.size()+"\n");
			//ExecutorService executor = Executors.newFixedThreadPool(this.numThreads);
			for (final String object: this.objects.keySet()) {
				//executor.execute(new Runnable() {
				//	@Override
				//	public void run() {
				final ObjectData data = Matcher.this.objects.get(object);
				final ConcurrentMap<String, Double> probs = new ConcurrentHashMap<String,Double>();
				final ConcurrentMap<String, Double> probsWithPenalty = new ConcurrentHashMap<String,Double>();
				for (String type: data.mapping.keySet()) {
					if (!Matcher.this.predictors.containsKey(type)) {
						synchronized (Matcher.this.predictors) {
							if (!Matcher.this.predictors.containsKey(type)) {
								Matcher.this.predictors.put(type, getNewPredictor());
							}
						}
					}
				//}
				//for (String type: data.mapping.keySet()) {
					synchronized (Matcher.this.predictors) {
						VMMPredictor predictor = Matcher.this.predictors.get(type);
						synchronized (predictor) {
							double prob = 1;
							for (IntSequence code: data.codes) {
								prob += predictor.logEval(code);
							}
							if (data.mapping.get(type) == 0) {
								probs.put(type,prob);
							} else {
								probsWithPenalty.put(type,prob);
							}
						}								
					}
				}
				//executor.execute(new Runnable() {
				//	@Override
				//	public void run() {
				final Map<String, Double> sortedProbs = new TreeMap<String,Double>(new valueComparator(probs));
				final Map<String, Double> sortedProbsWithPenalty = new TreeMap<String,Double>(new valueComparator(probsWithPenalty));
				sortedProbs.putAll(probs);
				sortedProbsWithPenalty.putAll(probsWithPenalty);
				synchronized (output) {
					try {
						output.write("\n"+object+" ("+data.realtype+")\t[]\n");
						for (String type: sortedProbs.keySet()) {
							output.write("\t"+type+" : "+sortedProbs.get(type)+"\t[]\n");
						}
						for (String type: sortedProbsWithPenalty.keySet()) {
							output.write("\t"+type+" : "+sortedProbsWithPenalty.get(type)+"\t[]\n");
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				//	}
				//});
			}
			//executor.shutdown();
			//try {
			//	executor.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
			//} catch (InterruptedException e) {
			//	e.printStackTrace();
			//}
			output.write("\n");
			output.flush();
			output.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	// position counting
	private class positionsComparator implements Comparator<String> {
		Map<String,Map<Integer,Integer>> values;
		public positionsComparator(Map<String,Map<Integer,Integer>> vals) {
			this.values = vals;
		}
		@Override
		public int compare(String arg0, String arg1) {
			Map<Integer,Integer> val0 = values.get(arg0);
			Map<Integer,Integer> val1 = values.get(arg1);
			for (int i = 0; i < val0.keySet().size(); i++) {
				if (val0.get(i) != val1.get(i)) {
					return val1.get(i).compareTo(val0.get(i));
				}
			}
			return 0;
		}
	}
	private class positionsComparatorEx implements Comparator<String> {
		positionsComparator comp;
		public positionsComparatorEx(positionsComparator comp) {
			this.comp = comp;
		}
		@Override
		public int compare(String arg0, String arg1) {
			int res = comp.compare(arg0, arg1);
			if (res != 0) {
				return res;
			}
			return arg0.compareTo(arg1);
		}
	}
	public void matches() {
		FileWriter output;
		try {
			output = new FileWriter(binary+".matches");
			output.write("Number of objects: "+this.objects.size()+"\n");
			for (String object: this.objects.keySet()) {
				ObjectData data = this.objects.get(object);
				Map<String,Map<Integer,Integer>> rankings = new TreeMap<String,Map<Integer,Integer>>();
				for (String type: data.mapping.keySet()) {
					Map<Integer,Integer> ranks = new TreeMap<Integer,Integer>();
					for (int i=0; i<data.mapping.keySet().size(); i++) {
						ranks.put(i, 0);
					}
					rankings.put(type, ranks);
				}
				output.write("\n"+object+" ("+data.realtype+")\t[]\n");
				for (IntSequence code: data.codes) {
					Map<String, Double> probs = new TreeMap<String,Double>();
					for (String type: data.mapping.keySet()) {
						VMMPredictor predictor = this.predictors.get(type);
						probs.put(type,predictor.logEval(code));
					}
					Map<String, Double> sortedProbs = new TreeMap<String,Double>(new valueComparator(probs));
					sortedProbs.putAll(probs);
					int rank = -1;
					double last = -1;
					for (String type: sortedProbs.keySet()) {
						if (last != sortedProbs.get(type)) {
							rank++;
							last = sortedProbs.get(type);
						}
						rankings.get(type).put(rank, rankings.get(type).get(rank)+1);
					}
				}
				positionsComparator posComp = new positionsComparator(rankings);
				Map<String,Map<Integer,Integer>> sortedRankings = new TreeMap<String,Map<Integer,Integer>>(new positionsComparatorEx(posComp));
				sortedRankings.putAll(rankings);
				int rank = 1;
				String lastType = null;
				for (String type: sortedRankings.keySet()) {
					if ((null != lastType) && (posComp.compare(lastType, type) != 0)) {
						rank++;
					}
					lastType = type;
					output.write("\t"+type+" : "+rank+"\t[]\n");
				}
			}
			output.write("\n");
			output.flush();
			output.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	*/
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int numThreads = 1;
		try {
			numThreads = Integer.parseInt(args[1]);
		} catch (NumberFormatException e) {}
		new Matcher(args[0],numThreads).matches();
	}

}