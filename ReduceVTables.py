from imports import *

class ReduceVTables:

	def reduce(self,ea,binary,function):
		# collect for each object all field accesses and all function calls for that object
		def summarizeObjects(calls):
			lineArgs = {}
			objects = {}
			for k in calls.keys():
				isJmp = (GetMnem(int(k)) != 'jmp')
				for record in calls[k]:
					#print record
					# record = {'target':<funcName>:<objectExpression>, 'offset':<num>, 'args':<num>}
					target = record['target']
					target = target[target.find(':')+1:]
					if target != '00h':
						offset = record['offset']
						args = record['args']
						if offset % 4 != 0:
							raise Exception('Offset not multiple of 4')
						offset /= 4 # entry number inside table
						if target not in objects.keys():
							objects[target] = {}
						if k not in lineArgs.keys():
							lineArgs[k] = (isJmp,args)
						else:
							prev = lineArgs[k]
							if prev[0]:
								if not isJmp or (args<prev[1]):
									lineArgs[k] = (isJmp,args)
							else:
								if not isJmp:
									lineArgs[k] = (isJmp,min(args,prev[1]))
						if offset not in objects[target].keys():
							objects[target][offset] = [k]
						else:
							objects[target][offset].append(k)
			for t in objects:
				for o in objects[t]:
					objects[t][o] = min(map(lambda l:lineArgs[l],objects[t][o]))		
			#print objects
			return objects
						
		def printMappings(m):
			for k in m.keys():
				print k+':'
				for c in m[k].keys():
					print '\t'+hex(int(c))+'\t'+str(m[k][c])
			
		def isVTableOption(da,vtable, obj,data):
			#print 'table size: '+str(vt[vtable].getNumOfFuncsInVTable())
			#print 'maximal offset of obj: '+str(max(objs[obj]))
			#   reduce by vfunction offset
			#print hex(vtable)[:-1]+' '+str(len(vt[vtable].functions))
			if len(vt[vtable].functions) <= max(data.keys()):
				#print 'table at '+hex(int(vtable))+' eliminated for obj at '+obj+' because offset is out of bounds ('+str(len(vt[vtable].functions))+'<'+str(max(data.keys()))+')'
				return (False,0)
			#   reduce by number of arguments to vfunction
			#   reduce by field offset
			from analysisUtils import isNotLibrary
			isNotLibrary = isNotLibrary()
			fields = da.getFields()
			allocated = vt[vtable].alloc
			if vt[vtable].foundAllocation:
				if isNotLibrary or (allocated > 0):
					if obj in fields.keys():
						offset = max(map(lambda x:x[1],fields[obj]))+4 # +4 to reach end of field
						if allocated < offset:
							#print 'table at '+hex(int(vtable))+' eliminated for obj at '+obj+' because field offset '+hex(offset)+' is beyond vtable allocated size '+hex(allocated)
							return (False,0)
			for offset in data.keys():
				record = data[offset]
				#print str(record[1])+'\t'+str(vt[vtable].functions[offset].num_of_args)
				if record[1] < vt[vtable].functions[offset].num_of_args:
					#print 'table at '+hex(int(vtable))+' eliminated for obj at '+obj+' because number of arguments for function at offset '+str(offset)+' is not sufficient ('+str(vt[vtable].functions[offset].num_of_args)+'>'+str(record[1])+')'
					return (True,1)
			return (True,0)
		
		# create initial mapping for each object (contains all vtables)
		def createMapping(objects,vt,da):
			#print objects
			from analysisUtils import getAllAllocatedVirtualTables
			mapping = {}
			initialVTs = getAllAllocatedVirtualTables()
			#print initialVTs
			for k in objects.keys():
				# should we allow for classes without a size?
				# currently tables without a size have 0 size. could change it to -1 to filter by it:
				#	mapping[k] = filter(lambda x: vt[x].alloc >=0, vt.keys()[:])
				mapping[k] = initialVTs.copy()
			#printMappings(mapping)
			#print mapping
			for k in mapping.keys():
				mapping[k] = map(lambda x: (x,isVTableOption(da,x,k,objects[k])), mapping[k])
				mapping[k] = filter(lambda x:x[1][0],mapping[k])
				mapping[k] = map(lambda x: (x[0],x[1][1]), mapping[k])	
			for k in mapping.keys():
				tmp = mapping[k]
				mapping[k] = {}
				for p in tmp:
					mapping[k][p[0]] = p[1]
			return mapping
		
		# all objects in same call location should have same types. unite all objects in the same call site
		# union (not intersection) because in case of inheritance some objects might only have part of the tree (due to other constraints)
		def uniteObjectInSameCall(binary,func,mapping):
			class hashList(list):
				def __hash__(self):
					return hash(str(self))
			def partitionObjects(binary,func):
				db = getDB(binary)
				partition = {}
				for r in db.siblings.find({'binary':get_root_filename(),'function':func}):
					if r['object'] not in partition.keys():
						partition[r['object']] = set()
					partition[r['object']].add(r['sibling'])	
				changed = True
				while changed:
					changed = False
					for o in partition.keys():
						trans = set()
						for s in map(lambda x:partition[x],partition[o]):
							trans = trans.union(s)
						if not trans.issubset(partition[o]):
							partition[o] = partition[o].union(trans)
							changed = True
				return set(map(lambda x:hashList(sorted(x)),partition.values()))
			partition = partitionObjects(binary,func)
			#print partition
			for p in partition:
				m = set(mapping[p[0]].keys())
				for s in p:
					m = m.intersection(mapping[s].keys())
				newMapping = {}
				for k in m:
					newMapping[k] = min(map(lambda y:mapping[y][k],filter(lambda x:k in mapping[x],p)))
				for s in p:
					mapping[s] = newMapping
			return mapping
		
		"""	
		# add comment to IDA
		def setLineComments(oics,mappings):
			allLines = set()
			for p in oics.keys():
				allLines = allLines.union(oics[p].keys())
			for l in allLines:
				line = int(l)
				comment = GetCommentEx(line,0)
				if comment == None:
					comment = ''
				commentHeader = 'possible call targets: '
				if commentHeader in comment:
					comment = comment[:comment.find(commentHeader)]
				subComment = ''
				for p in oics.keys():
					if l in oics[p].keys():
						subComment += '['
						oic = oics[p]
						mapping = mappings[p]
						obj = filter(lambda x:x[:x.rfind('.')] in mapping.keys(),oic[l])[0]
						o = obj[obj.rfind('.')+1:]
						for f in mapping[obj[:obj.rfind('.')]]:
							subComment += hex(int(f)+int(o))+', '
						subComment = subComment[:-2]+'], '
				comment += commentHeader+'{'+subComment[:-2]+'}'
				MakeComm(int(l),comment)
		"""
		
		"""
		# keep only unique mappings. if mapping is completely contained inside another mapping, no need to save it
		def summarizeMappings(mappings):
			sumMappings = []
			for m in mappings.values():
				exists = False
				toRemove = []
				for mother in sumMappings:
					if len(set(m.keys()).symmetric_difference(set(mother.keys()))) == 0:
						if len(filter(lambda k: not set(m[k]).issubset(set(mother[k])),m.keys())) == 0:
							exists = True
						if len(filter(lambda k: not set(mother[k]).issubset(set(m[k])),m.keys())) == 0:
							toRemove.append[mother]
				for m in toRemove:
					sumMappings.remove(m)
				if not exists:
					sumMappings.append(m)
			return sumMappings
		"""
		
		"""
		# create all possible combinations of types for objects
		def allPossibleTypeCombinations(sumMappings):
			from itertools import product
			combinations = set()
			for m in sumMappings:
				tmpM = {}
				for k in m.keys():
					tmpM[k] = map(lambda x: str(k)+'='+hex(x)[:-1],m[k])
				for p in product(*(tmpM.values())):
					combinations.add(p)
			return combinations
		"""
		
		from vtables import VirtualTableFinder 
		vt = VirtualTableFinder().find_virtual_tables()
		from FindClassSize import TypeRuler
		TypeRuler().findClassSizes(vt.values())

		"""
		oics = {}
		mappings = {}
		
		pnum = 0
		for p in getAllPathsInFunc(ea):
			print pnum
			pnum+=1
			f.analyzeFunctionByPath(p)
			#f.analyzeFunction()

			#calls = f.getCalls()
			#print calls
			#fields = f.getFields()
			#print fields
				
			objects = summarizeObjects(f.getCalls())

			oics[p] = f.getObjectsInCalls()
			#print oic
			
			mappings[p] = uniteObjectInSameCall(oics[1],createMapping(objects,vt,f))
			#printMappings(mapping)
		
		setLineComments(oics,mappings)
		"""
		
		from analysisUtils import getDataAnalyzer
		f = getDataAnalyzer(ea)
		if f:
			objects = summarizeObjects(f.getCalls())
			#print objects
			#oic = f.getObjectsInCalls()
			mapping = createMapping(objects,vt,f)
			#print mapping
			uniteObjectInSameCall(binary,function,mapping)
			#printMappings(mapping)
			#setLineComments({1:oic},{1:mapping})
			return mapping
		
		"""
		#pathnum = 0
		for m in summarizeMappings(mappings):
			#print '\npath #'+str(pathnum)+':'
			#pathnum += 1
			printMappings(m)
		
		for c in allPossibleTypeCombinations(summarizeMappings(mappings)):
			print c
		"""
		
if __name__ == '__main__':
	#print ReduceVTables().reduce(0x412C10,'BigToy.exe','sub_412C10')['EAX2']
	print 'Done!'