__author__ = 'Gili'

import matches
from sklearn import preprocessing
from sklearn import svm
import numpy as np
import sys, re
from multiprocessing import Pool


class RunSvm:

    def load_labels(self, db, binary, pool):
        self.labels = list()
        self.type_traces = list()
        #self.k1s = list()
        self.features = set()
        processesTypes = pool.map_async(matches.loadTypeSignature,map(lambda t:(binary,t),db.typesignatures.find({"binary":binary}).distinct('typename')))
        # i = 0
        for r in processesTypes.get():
            self.labels += [r[0]]
            self.features.update(r[1])
            self.type_traces += [r[1]]
            #self.k1s += ([r[2]] * len(r[1]))
        self.features = sorted(list(self.features))
        print len(self.features)
        print "labels:", len(self.labels)
        print self.labels
        self.train = np.asarray(self._get_attributes(self.type_traces,pool))
        print 'length of trainset: ', len(self.train)
        self.test_trainset()

    def test_trainset(self):
        print '********************starting test*************'
        features_num = len(self.features)
        i = 0
        for vec in self.train:
            if len(vec) != features_num:
                print i, ': wrong number of features, features number is: ', len(vec)
            j = 0
            if (np.any(np.isnan(vec))):
                print 'Nan should be in this vector: ', i
            for bNan in np.isnan(vec):
                if bNan:
                    print i, ': NaN element present', j
                    j += 1
            i += 1
        print 'done testing the trainset**********************************************************************************'

    def load_test(self, db, binary, pool):
        self.test_names = list()
        self.types = dict()
        candidates = list()
        processesObjects = map(lambda f:matches.loadFunctionSignatures(binary,f,pool),db.untypedsignatures.find({"binary":binary}).distinct('function'))
        for rs in processesObjects:
            for r in rs.get():
                obj = r[0][r[0].rfind('.')+1:]
                if not re.match('^\[EBP[^\[\]]*\]$',obj): # filter irrelevant objects
                    self.test_names += [r[0]]
                    traces = list()
                    for k in sorted(r[1].keys()):
                        traces += r[1][k]
                    candidates += [traces]
                    self.types[r[0]] = r[2]
        print 'objects: ', len(self.test_names)
        # TODO: temporarily hard coded
        self.types = {
            'sub_413140.[ESP+04h]' : 'F',
            'sub_412A70.[ESP+04h]' : 'B',
            'sub_413770.[ESP+04h]' : 'E',
            'sub_4129A0.[ESP+04h]' : 'A',
            'sub_412AD0.[ESP+04h]' : 'C',
            'sub_412C10.EAX2' : 'A',
            'sub_414E20.EAX2' : 'E',
            'sub_4134F0.[ESP+04h]' : 'A',
            'sub_412F50.EAX2' : 'B',
            'sub_412D20.EAX2' : 'B1',
            'sub_4132D0.EAX2' : 'G',
            'sub_412E30.EAX2' : 'B2',
            'sub_413630.[ESP+04h]' : 'C',
            'sub_414D80.[ESP+04h]' : 'E',
            'sub_413060.EAX2' : 'C',
            'sub_4136A0.[ESP+04h]': 'D',
            'sub_4131F0.EAX2' : 'D',
            'sub_4137C0.[ESP+04h]' : 'E',
            'sub_413570.[ESP+04h]' : 'B',
            'sub_412B60.[ESP+04h]' : 'D',
            'sub_414F00.EAX2' : 'F',
            'sub_414F00.EAX4' : 'F'
        }

        self.test = np.asarray(self._get_attributes(candidates,pool))

    def _get_attributes(self, lTracelets,pool):
        #res = list()
        res = map(lambda t: map(lambda l: min(map(lambda x: x[0], pool.map(matches.damerau_levenshtein_distance, map(lambda o:(l,o),t)))), self.features), lTracelets)
        #for tracelets in lTracelets:
        #    res_tracelet = list()
        #    res_tracelets = map(lambda l: min(map(lambda o: matches.damerau_levenshtein_distance(l, o)[0], tracelets)), self.features)
        #    #for label in self.features:
        #    #    res_tracelet += [min(map(lambda o: matches.damerau_levenshtein_distance(label, o)[0], tracelets))]
        #    print len(res_tracelet)
        #    res += [res_tracelet]
        return res

    def setup(self, binary, procs):
        pool = Pool(processes=procs)
        db = matches.getDB(binary)
        print 'loading trainset'
        self.load_labels(db, binary, pool)
        print 'training'
        self.classifier = svm.LinearSVC()
        #self.classifier = svm.SVC()
        self.classifier.fit(self.train, self.labels)
        print self.train
        print self.labels
        print 'loading testset'
        self.load_test(db, binary, pool)
        pool.close()
        pool.join()

    def generate_results(self, max_rank = None):
        #flag the normalization of decision function
        result = list()
        for name, pred in zip(self.test_names, self.classifier.decision_function(self.test)):
            positive = [x for x in zip(pred, self.labels) if x[0] >= 0]
            negative = [x for x in zip(pred, self.labels) if x[0] < 0]
            result += [(name, sorted(positive, key=lambda x: x[0]) + sorted(negative, key=lambda x: x[0], reverse=True))]
        return result


if __name__ == '__main__':
    sys.argv = ['matches.py','BigToy.exe','4','debug_out.txt']
    names_map = {
        'off_417AF4' : 'D',
        'off_418034' : 'G',
        'off_417B18' : 'E',
        'off_417AAC' : 'B',
        'off_417C10' : 'B1',
        'off_417AD0' : 'C',
        'off_417A84' : 'A',
        'off_417B94' : 'F',
        'off_417B3C' : 'E2',
        'off_417B5C' : 'B2'
    }
    print "start"
    # read arguments
    if len(sys.argv) < 4:
        print 'Usage: python matches.py <binaryName> <# of processes> <output file>'
        sys.exit()
    binary = sys.argv[1]
    end = ''
    i = 0
    if binary.startswith('\''):
        end = '\''
    if binary.startswith('\"'):
        end = '\"'
    if end != '':
        binary = binary [1:]
        while not binary.endswith(end):
            i += 1
            if len(sys.argv) < (4+i):
                print 'Usage: python matches.py <binaryName> <# of processes> <output file>'
                sys.exit()
            binary += ' '+sys.argv[1+i]
        binary = binary[:-1]
    procs = int(sys.argv[2+i])
    outputfilename = sys.argv[3+i]
    end = ''
    if outputfilename.startswith('\''):
        end = '\''
    if outputfilename.startswith('\"'):
        end = '\"'
    if end != '':
        outputfilename = outputfilename [1:]
        while not outputfilename.endswith(end):
            i += 1
            if len(sys.argv) < (4+i):
                print 'Usage: python matches.py <binaryName><# of processes> <output file>'
                sys.exit()
            outputfilename += ' '+sys.argv[3+i]
        outputfilename = outputfilename[:-1]
    if len(sys.argv) != 4+i:
        print 'Usage: python matches.py <binaryName> <# of processes> <output file>'
        sys.exit()

    sv = RunSvm()
    print "setup"
    sv.setup(binary, procs)
    print "test", sv.test
    #print "train:"
    #print sv.train
    #print "labels:", sv.labels
    res = sv.generate_results()
    print res
    print outputfilename
    with open(outputfilename, 'w') as out:
        for r in res:
            out.write(r[0]+' ('+sv.types[r[0]]+')\n')
            for t in r[1]:
                out.write('\t'+names_map[t[1]]+' : '+str(t[0])+'\n')
        out.write('\n')
