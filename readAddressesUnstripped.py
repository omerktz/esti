from idc import *
from idaapi import *
from idautils import *
#from idaCache import *

autoWait()

from vtables import VirtualTableFinder


fOut = open('/home/omerkatz/vtsPomela/benchmarks/matchResultsUnstripped/'+get_root_filename()+'.addresses','w')

for funcea in Functions():
	fOut.write(Name(funcea)+'\t'+hex(funcea)+'\n')
for v in VirtualTableFinder().find_virtual_tables().values():
	if Name(v.ea) != '':
		name = Name(v.ea)
		if Demangle(name,0):
			name = Demangle(name,0)
			if name.startswith('const '):
				name = name[len('const '):]
			if name.endswith("::`vftable'"):
				name = name[:-len("::`vftable'")]
		fOut.write(name+'\t'+hex(v.ea)+'\n')
fOut.close()

print 'Done!'

qexit(0)
