from imports import *
from func_utils import getFunctionStartAddress

interestingFunctions = None
def getInterestingFunctions():
	global interestingFunctions
	if not interestingFunctions:
		from func_utils import shouldAnalyze,getFunctionStartAddress
		results = doForAllFunctionsUnsorted(shouldAnalyze)
		interesting = set(filter(lambda f:results[f],results.keys()))
		tmp = set()
		for f in interesting:
			tmp = tmp.union(set([getFunctionStartAddress(x.frm) for x in XrefsTo(f)]))
		tmp = tmp - interesting
		checked = set()
		while len(tmp) != 0:
			f = tmp.pop()
			checked.add(f)
			if GetMnem(f) != 'jmp':
				interesting.add(f)
			else:
				tmp = tmp.union(set([getFunctionStartAddress(x.frm) for x in XrefsTo(f)])-checked)
		interestingFunctions = interesting
	return interestingFunctions

#cache of DataAnalyzers for functions
dataAnalyzers = {}
def getDataAnalyzer(f):
	global dataAnalyzers
	fnc = get_func(f)
	if not fnc:
		return None
	func = fnc.startEA
	#print hex(func)[:-1]
	if func not in dataAnalyzers.keys():
		from FunctionProvider import Function
		from DataAnalyzer import DataAnalyzer
		#print "\tAnalyzing "+hex(func)[:-1]
		da = DataAnalyzer(Function(func))
		#print "\t\tAnalysis finished"
		da.analyzeFunction()
		dataAnalyzers[func] = da
	return dataAnalyzers[func]

def analyzeFunction(f):
	checkMemory()
	print "Analyzing "+hex(f)[:-1]
	getDataAnalyzer(f)

def analyzeAllFunctions():
	doForAllFunctions(analyzeFunction)
	#return dataAnalyzers

def doForAllFunctions(f):
	#results = {}
	#funcs = set()
	#for funcea in Functions():
	#	funcs.add(funcea)
	#from func_utils import count_branches,calc_deps
	#for funcea in sorted(Functions()): #sorted(funcs, key=lambda f:max(map(lambda x:count_branches(x),calc_deps(f)))):
	#	results[funcea] = f(funcea)
	pool = Pool(processes=int(sys.argv[2]))
	results = dict(zip(sorted(Functions()), pool.map(f, sorted(Functions()))))
	pool.close()
	pool.join()
	return results
		
def doForAllFunctionsUnsorted(f):
	#results = {}
	#for funcea in Functions():
	#	results[funcea] = f(funcea)
	pool = Pool(processes=int(sys.argv[2]))
	results = dict(zip(Functions(), pool.map(f, Functions())))
	pool.close()
	pool.join()
	return results

def isNotLibrary():
	return get_root_filename().endswith('.exe')
		
allocatedCache = None
def getAllAllocatedVirtualTables():
	global allocatedCache
	if not allocatedCache:
		from vtables import VirtualTableFinder
		#print isNotLibrary()
		if not isNotLibrary():
			allocatedCache = set(VirtualTableFinder().find_virtual_tables().keys())
		else:
			from FindClassSize import TypeRuler
			from func_utils import getFunctionStartAddress
			vts = VirtualTableFinder().find_virtual_tables().values()
			constructors = {}
			allConstructors = set()
			commonConstructors = set()
			for vt in vts:
				#print hex(vt.ea)
				constructors[vt.ea] = set(map(lambda x:get_func(x).startEA,filter(lambda y:get_func(y),TypeRuler().getFilteredConstructorsOfVT(vt))))
				commonConstructors = commonConstructors.union(allConstructors.intersection(constructors[vt.ea]))
				allConstructors = allConstructors.union(constructors[vt.ea])
			#print 'constructors:'
			#for k in constructors.keys():
			#	print hex(k)[:-1] + ' ' + str(map(lambda x:hex(x)[:-1],constructors[k]))
			#print ''
			result = set()
			for k in constructors.keys():
				#print hex(k)
				for c in constructors[k]:
					#print '\t'+hex(c)
					if c in commonConstructors:
						result.add(k)
						break
					xrefs = set([x.frm for x in XrefsTo(c)])
					checked = set()
					while len(xrefs) != 0:
						x = xrefs.pop()
						checked.add(x)
						if GetMnem(x) == 'jmp':
							f = get_func(x)
							if not f:
								result.add(k)
								break
							else:
								xrefs = xrefs.union([x.frm for x in XrefsTo(f.startEA)])-checked
						else:
							x = getFunctionStartAddress(x)
							da = getDataAnalyzer(x)
							if da:
								def isVT(x):
									if x.endswith('h'):
										x = x[:-1]
									if x.startswith('0x'):
										x = x[2:]
									try:
										x = int(x,16)
										return x in VirtualTableFinder().find_virtual_tables().keys()
									except ValueError:
										return False
								add = False
								for s in da.getExitPointStates():
									if len(filter(lambda s3:isVT(s3),map(lambda s2:s2.memory['ECX0'],filter(lambda s1:'ECX0' in s1.memory.keys(),s)))) == 0:
										add = True
										break
								if add:
									result.add(k)
									break
							if x not in allConstructors or x in commonConstructors:
								result.add(k)
								break
			allocatedCache = result
	return allocatedCache
	
def getAddressFromOperand(opnd):
	if match('^0x[0-9a-fA-F]+$',opnd):
		return long(opnd,16)
	if match('^[0-9a-fA-F]+h$',opnd):
		return long(opnd[:-1],16)
	if match('^[0-9]+$',opnd):
		return long(opnd)
	if LocByName(opnd) != 0xffffffffffffffff:
		return LocByName(opnd)			
	return None
	
# find all calls in function.
def collectCalls(ea):
	calls = set()
	if get_func(ea):
		from FunctionProvider import Function
		f = Function(get_func(ea).startEA)
		for b in f.getBlocks():
			for c in f.getBlockCommands(b):
				if c.startswith('call '):
					targetStr = c[5:]
					if match('^[scdefg]s\:.*',targetStr):
						targetStr = targetStr[3:]
					address = getAddressFromOperand(targetStr)
					if address:
						calls.add(address)
	return calls
	
def findMain():
	main = None
	if LocByName('main') != 0xffffffffffffffff:
		main = LocByName('main')
	if LocByName('_main') != 0xffffffffffffffff:
		main = LocByName('_main')
	if LocByName('__initenv') != 0xffffffffffffffff:
		l = LocByName('__initenv')
		xs = set()
		for x in XrefsTo(l):
			xs.add(x.frm)
		if len(xs) == 1:
			x = xs.pop()
			while GetMnem(x) != 'call':
				x += ItemSize(x)
			main = getAddressFromOperand(GetOpnd(x,0))
	if main:
		while GetMnem(main) == 'jmp':
			main = getAddressFromOperand(GetOpnd(main,0))
			if not main:
				break
	return main
	
def outsideMain():
	main = findMain()
	outside = set()
	if main:
		#print 'main is at '+hex(main)[:-1]+'\n'
		tmp = set()
		for x in XrefsTo(main):
			tmp.add(x.frm)
		parents = set()
		while len(tmp) != 0:
			f = tmp.pop()
			if f not in parents:
				#print 'parents\t'+hex(f)
				parents.add(f)
				for x in XrefsTo(f):
					if get_func(x.frm):
						xStart = get_func(x.frm).startEA
						if xStart not in parents:
							tmp.add(xStart)
		outside = set()
		while len(parents) != 0:
			f = parents.pop()
			if f not in outside:
				#print 'kids\t'+hex(f)
				outside.add(f)
				if GetMnem(f) == 'jmp':
					o = getAddressFromOperand(GetOpnd(f,0))
					if o:
						if o != main:
							if o not in outside:
								parents.add(o)
				else:
					for c in collectCalls(f):
						if c != main:
							if c not in outside:
								parents.add(c)
	return outside
		
#lastBranches = 0
#lastlastBranches = 0
#funcsDone = 0
allObjects = True
typenames = {}
def getTracesFromFunc(ea,k,skipFuncs = []):
	def checkMemory():
		if Process().get_memory_percent() > 50:
			for k in dataAnalyzers.keys():
				del dataAnalyzers[k]
			dataAnalyzers = {}
			import gc
			gc.collect()
	def getVtName(t):
		global typenames
		if t not in typenames.keys():
			typename = "Unknown"
			#if db.typed.find({"binary": binary, "type": t, "typename": {"$ne": typename}}).count() > 0:
			#	typename = db.typed.find({"binary": binary, "type": t, "typename": {"$ne": typename}}).distinct("typename")[0]
			try:
				vtea = long(t,16)
				if Name(vtea) != '':
					typename = Name(vtea)
					if Demangle(typename,0):
						typename = Demangle(typename,0)
						if typename.startswith('const '):
							typename = typename[len('const '):]
						if typename.endswith("::`vftable'"):
							typename = typename[:-len("::`vftable'")]
			except:
				pass
			typenames[t] = typename
		return typenames[t]
	def getVtNameNum(t):
		global typenames
		if t not in typenames.keys():
			typename = "Unknown"
			#if db.typed.find({"binary": binary, "type": t, "typename": {"$ne": typename}}).count() > 0:
			#	typename = db.typed.find({"binary": binary, "type": t, "typename": {"$ne": typename}}).distinct("typename")[0]
			if Name(t) != '':
				typename = Name(t)
				if Demangle(typename,0):
					typename = Demangle(typename,0)
					if typename.startswith('const '):
						typename = typename[len('const '):]
					if typename.endswith("::`vftable'"):
						typename = typename[:-len("::`vftable'")]
			typenames[t] = typename
		return typenames[t]
	#global lastBranches
	#global lastlastBranches
	#global funcsDone
	#from func_utils import count_branches,calc_deps
	#currentBranches = max(map(lambda x:count_branches(x),calc_deps(ea)))
	#if max(map(lambda x:count_branches(x),calc_deps(ea))) > 10:
	#	return
	#checkMemory()
	#print hex(ea)
	if ea not in skipFuncs:
		# removed this line to analyze all functions:
		#print (ea in getInterestingFunctions())
		if ea in getInterestingFunctions():
			print "Extracting "+hex(ea)#[:-1]+' (mem: '+str(Process().get_memory_percent())+') '+str(funcsDone)+' '+str(lastlastBranches)+' '+str(lastBranches)+' '+str(currentBranches)
			from split2k import ksplitGraphObject
			from VTRefiner import VTRefiner
			from GetKnownTypes import TypeFinder
			binary = get_root_filename()
			name = str(GetFunctionName(ea))
			if name in ['','None']:
				name = hex(ea)#[:-1]
			vtr = VTRefiner(ea)
			types = TypeFinder().getKnownTypes(ea)
			#print types
			#print name
			#print binary
			#print types.keys()
			#print ''
			db = getDB(binary)
			if len(vtr.getPossibleObjects()) > 0:
				from ReduceVTables import ReduceVTables
				initialMapping = ReduceVTables().reduce(ea, binary, name)
				if not initialMapping:
					raise Exception('initial mapping failed')
				for o in vtr.getPossibleObjects():
					#print o
					for r in ksplitGraphObject(k,vtr.refineGraph([o]),name):
						#print r['code']
						if db.untyped.find({"object": o, "function": name, "binary": binary, "trace": r['trace'], "code": r['code']}).count()==0:
							realtypes = 'Unknown'
							if o in types.keys():
								#print o
								#print types[o]
								for t in types[o]:
									#print t
									typename = getVtName(t)
									if db.typed.find({"object": o, "function": name, "binary": binary, "trace": r['trace'], "code": r['code'], "type": t}).count()==0:
										db.typed.insert({"object": o, "function": name, "binary": binary, "trace": r['trace'], "code": r['code'], "k": r['code'].count(';'), "type": t, "typename": typename})
								realtypes = str(list(types[o]))
							db.untyped.insert({"object": o, "function": name, "binary": binary, "trace": r['trace'], "code": r['code'], "k": r['code'].count(';'), "types": "Unknown", "realtypes": realtypes})
					if o not in initialMapping.keys():
						raise Exception('object '+o+' missing from initial mapping')
					for t in initialMapping[o].keys():
						db.mapping.insert({'binary':binary,'function':name,'object':o,'type':getVtNameNum(t),'base':initialMapping[o][t]})
			global allObjects
			#print allObjects
			if allObjects:
				#checkMemory()
				for o in vtr.getOtherObjects():
					typenames = {}
					for t in types[o]:
						#if db.typed.find({"binary": binary, "type": t, "typename": {"$ne": "Unknown"}}).count() > 0:
						#	typenames[t] = db.typed.find({"binary": binary, "type": t, "typename": {"$ne": "Unknown"}}).distinct("typename")[0]
						#else:
						typenames[t] = getVtName(t)
					for r in ksplitGraphObject(k,vtr.refineGraph([o]),name):
						for t in types[o]:
							if db.typed.find({"object": o, "function": name, "binary": binary, "trace": r['trace'], "code": r['code'], "type": t}).count()==0:
								db.typed.insert({"object": o, "function": name, "binary": binary, "trace": r['trace'], "code": r['code'], "k": r['code'].count(';'), "type": t, "typename": typenames[t]})
			# calculate sibling objects (objects that appear in the smae call site)
			da = getDataAnalyzer(ea)
			siblingObjects = {}
			for l in da.getCalls().keys():
				oics = da.getCalls()[l]
				oics = set(filter(lambda o: o['object']!= '00h', map(lambda oic: hashableDict({"binary":binary,"line":hex(l)[:-1],"function":hex(getFunctionStartAddress(long(l))),"object":oic['target'][oic['target'].find(':')+1:],"offset":oic['offset']/4}), oics)))
				for oic in oics:
					db.objectsInCalls.insert(oic)
				oics = set(map(lambda oic: oic['object'],oics))
				for oic in oics:
					if oic not in siblingObjects.keys():
						siblingObjects[oic] = set()
					siblingObjects[oic] = siblingObjects[oic].union(oics)
			for o in siblingObjects.keys():
				for s in siblingObjects[o]:
					db.siblings.insert({"binary": binary, "function": name, "object": o, "sibling": s})
			#if currentBranches != lastBranches:
			#	lastlastBranches = lastBranches
			#	lastBranches = currentBranches
			#funcsDone+=1
				
def get7traces(f):
	getTracesFromFunc(f,7)

def analysisUtils_main():
	autoWait()
	start = time()
	print 'Getting all allocated VTables'
	getAllAllocatedVirtualTables()
	print '\tDone'
	print 'Calculating class sizes'
	from vtables import VirtualTableFinder 
	from FindClassSize import TypeRuler
	TypeRuler().findClassSizes(VirtualTableFinder().find_virtual_tables().values())
	print '\tDone'
	print 'Finding interesting functions'
	getInterestingFunctions()
	print '\tDone'
	#print map(lambda x:hex(x),getAllAllocatedVirtualTables())
	#print map(lambda x:hex(x)[:-1],outsideMain())
	#getTracesFromFunc(ScreenEA(),5)
	#analyzeAllFunctions()
	doForAllFunctions(get7traces)
	print 'Finished all functions'
	#getTracesFromFunc(ScreenEA(),1)
	#print sorted(getInterestingFunctions())
	end = time()
	from OrganizeData import createTypeSignatures,createUntypedSignatures
	b = get_root_filename()
	createTypeSignatures(b)
	createUntypedSignatures(b)
	print 'runtime: ' + str(end-start)
	print 'Done!'
	#f = open("/home/omerkatz/vtsPomela/benchmarks/binaries",'a')
	#f.write(b+'\n')
	#f.close()
	#from idaapi import qexit
	print 'reading addresses' 
	from readAddressesStripped import readAddressesStripped_main
	readAddressesStripped_main()
	print 'extracting vtables'
	from vtables import vtables_main
	vtables_main()
	qexit(0)
if __name__ == '__main__':
	analysisUtils_main()