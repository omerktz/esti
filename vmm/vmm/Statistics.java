package vmm;

import java.util.Arrays;
import java.util.Collection;

public class Statistics 
{
    public static double getMean(Collection<Double> data)
    {
        double sum = 0.0;
        for(double a : data)
            sum += a;
        return sum/data.size();
    }

    public static double getVariance(Collection<Double> data)
    {
        double mean = getMean(data);
        double temp = 0;
        for(double a :data)
            temp += (mean-a)*(mean-a);
        return temp/data.size();
    }

    public static double getStdDev(Collection<Double> data)
    {
        return Math.sqrt(getVariance(data));
    }

    public static double median(Collection<Double> data) 
    {
		Double[] dataArray = (Double[]) data.toArray();
		
		Arrays.sort(dataArray);
		
		if (dataArray.length % 2 == 0) 
		{
		   return (dataArray[(dataArray.length / 2) - 1] + dataArray[dataArray.length / 2]) / 2.0;
		} 
		else 
		{
		   return dataArray[dataArray.length / 2];
		}
    }
}