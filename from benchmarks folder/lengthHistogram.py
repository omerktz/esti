from re import match
from pymongo import MongoClient
db = MongoClient()
import sys
fOut = open(sys.argv[2],'w')
fIgnore = open(sys.argv[3],'w')

def filterObjects(o):
	if o.startswith('[EBP'):
		return False
	def is_object(o):
		if '[' not in o and ']' not in o:
			return True
		matched = match('^\[(.+)\]([\+\-]0[0-9a-fA-F]+h)?$',o)
		if matched:
			return is_object(matched.groups()[0])
		else:
			return False
	return is_object(o)

histograms = {}
objects = {}

d = 'OmerK_Types_'+sys.argv[1].replace(' ','_').replace('.','_')
histogram = {}
numObjects = 0
for f in db[d].untypedsignatures.distinct('function'):
	for o in db[d].untypedsignatures.find({'function':f}).distinct('object'):
		if filterObjects(o):
			topK = max(db[d].untypedsignatures.find({'function':f,'object':o}).distinct('k'))
			#topK = max(db[d].untypedsignatures.find({'function':f,'object':o}).distinct('k')) if db[d].typed.find({'function':f,'object':o}).count() == 0 else 0
			if topK < 3:
				fIgnore.write(sys.argv[1]+'\t'+f+'.'+o+'\n')
			else:
				if topK not in histogram.keys():
					histogram[topK] = 0
				histogram[topK] += 1
				numObjects += 1
if len(histogram.keys()) > 0:
   histograms[d[12:]] = histogram
   objects[d[12:]] = numObjects

maxK = 0
for b in histograms.keys():
	maxK = max(maxK,max(histograms[b].keys()))
for b in histograms.keys():
	for i in range(1,maxK+1):
		if i not in histograms[b].keys():
			histograms[b][i] = 0

fOut.write('file\tobjects')
for i in range(1,maxK+1):
	fOut.write('\t'+str(i))
fOut.write('\tsanity\n')
for b in histograms.keys():
	fOut.write(b+'\t'+str(objects[b]))
	sum = 0
	for i in range(1,maxK+1):
		sum += histograms[b][i]
		fOut.write('\t'+str(histograms[b][i]))
	fOut.write('\t'+str(sum==objects[b])+'\n')

fOut.close()
fIgnore.close()
