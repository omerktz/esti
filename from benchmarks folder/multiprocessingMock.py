def Pool(processes=None):
    class poolClass():
        def map(self,f,xs):
            result = []
            for x in xs:
                result.append(f(x))
            return result
        
        def map_async(self,f,xs):
            class asyncResult():
                def get(self):
                    return map(f,xs)
            return asyncResult()
        
        def close(self):
            pass
        def join(self):
            pass
        
    return poolClass()
    