from pymongo import MongoClient

db = MongoClient().OmerK_SavedTypes

def transfer(name):
	strippedNames = {}
	unstrippedNames = {}
	types = {}

	fStrippedNames = open('./matchResults/'+name+'.addresses','r')
	line = fStrippedNames.readline()
	while line != '':
		s = line.split('\t')
		strippedNames[s[0].strip()] = s[1].strip()
		line = fStrippedNames.readline()
	fStrippedNames.close()
	strippedNames['OmerK-EMPTY'] = 0

	fUnstrippedNames = open('./matchResultsUnstripped/'+name+'.addresses','r')
	line = fUnstrippedNames.readline()
	while line != '':
		s = line.split('\t')
		unstrippedNames[s[1].strip()] = s[0].strip()
		line = fUnstrippedNames.readline()
	fUnstrippedNames.close()
	unstrippedNames[0] = 'OmerK-EMPTY'

	fTypes = open('./matchResultsUnstripped/'+name+'.matches','r')
	line = fTypes.readline()
	while not line.startswith('Total:'):
		s = line.split('\t')
		if line.strip() != '':
			if not line.startswith('\t'):
				object = line[:line.find(' ')].strip()
				type = line[line.find(' ')+1:line.find('\t')].strip()[1:-1]
				types[object] = type
		line = fTypes.readline()
	fTypes.close()

	import os
	os.rename('./matchResults/'+name+'.matches','./matchResults/'+name+'.matches.bak')
	fIn = open('./matchResults/'+name+'.matches.bak','r')
	fOut = open('./matchResults/'+name+'.matches','w')

	line = fIn.readline()
	fOut.write(line.strip()+'\n')
	line = fIn.readline()
	fOut.write('\n')

	line = fIn.readline()
	while line: #not line.startswith('Total:'):
		if line.strip() == '':
			fOut.write('\n')
		else:
			if line.startswith('\t'):
				type = line[:line.find(' : ')].strip()
				rest = line[line.find(' : '):].strip()
				
				if type in strippedNames.keys() and strippedNames[type] in unstrippedNames.keys():
					fOut.write('\t'+unstrippedNames[strippedNames[type]]+' '+rest+'\n')
				else:
					fOut.write('\t'+type+' '+rest+'\n')
			else:
				func = line[:line.find('.')]
				if func in strippedNames.keys() and strippedNames[func] in unstrippedNames.keys():
					func = unstrippedNames[strippedNames[func]]
				object = line[line.find('.'):line.find('(')].strip()
				rest = line[line.rfind(')'):].strip()
				objType = 'Unknown'
				if db[sys.argv[1]].find({"object":func+object}).count() > 0:
					objType = db[sys.argv[1]].find({"object":func+object}).distinct('type')[0]
				else:
					if (func+object) in types.keys():
						objType = types[func+object]
				#print func+object
				#print objType
				fOut.write(func+object+' ('+objType+rest+'\n')
		line = fIn.readline()

	fIn.close()
	fOut.close()

	try:
		os.remove(sys.argv[1]+'.calls.bak')
	except OSError:
		pass
	os.rename(sys.argv[1]+'.calls',sys.argv[1]+'.calls.bak')
	fIn = open(sys.argv[1]+'.calls.bak','r')
	fOut = open(sys.argv[1]+'.calls','w')
	line = fIn.readline()
	while line != '':
		line = line.strip()
		s = line.split('\t')
		if name.replace(' ','_').replace('.','_') == s[0]:
			if s[1] in strippedNames.keys() and strippedNames[s[1]] in unstrippedNames.keys():
				fOut.write(s[0]+'\t'+unstrippedNames[strippedNames[s[1]]]+'\t'+s[2]+'\t'+s[3]+'\t'+s[4]+'\n')
			else:
				fOut.write(line+'\n')
		else:
			fOut.write(line+'\n')
		line = fIn.readline()
	fIn.close()
	fOut.close()

	try:
		os.remove(sys.argv[1]+'.ignores.bak')
	except OSError:
		pass
	os.rename(sys.argv[1]+'.ignores',sys.argv[1]+'.ignores.bak')
	fIn = open(sys.argv[1]+'.ignores.bak','r')
	fOut = open(sys.argv[1]+'.ignores','w')
	line = fIn.readline()
	while line != '':
		line = line.strip()
		s = line.split('\t')
		func = s[1][:s[1].find('.')]
		if func in strippedNames.keys() and strippedNames[func] in unstrippedNames.keys():
			fOut.write(s[0]+'\t'+unstrippedNames[strippedNames[func]]+s[1][s[1].find('.'):]+'\n')
		else:
			fOut.write(line+'\n')
		line = fIn.readline()
	fIn.close()
	fOut.close()

import os
import sys

transfer(sys.argv[1])
try:
	os.remove(sys.argv[1]+'.calls.bak')
except OSError:
	pass
try:
	os.remove(sys.argv[1]+'.ignores.bak')
except OSError:
	pass
