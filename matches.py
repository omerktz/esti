from imports import *

costReadWriteReplace = 5
def getEditCost(t1,t2):
	global costReadWriteReplace
	if t1==t2:
		return 0
	if (t1.startswith('Read') or t1.startswith('Write')) and (t2.startswith('Read') or t2.startswith('Write')):
		if t1[t1.find('Offset'):] == t2[t2.find('Offset'):]:
			return costReadWriteReplace
	return 10

def damerau_levenshtein_distance(seq1, seq2, k1s, last):
	# Conceptually, this is based on a len(seq1) + 1 * len(seq2) + 1 matrix.
	# However, only the current and two previous rows are needed at once,
	# so we only store those.
	oneago = None
	seq1firstIsOptional = int(not seq1[0].startswith('ReturnedFromFunction-'))
	seq2firstIsOptional = int(not seq2[0].startswith('ReturnedFromFunction-'))
	thisrow = map(lambda n:(n*10,0),range(seq2firstIsOptional, len(seq2) + seq2firstIsOptional)) + [(0,0)]
	for x in xrange(len(seq1)):
		# Python lists wrap around for negative indices, so put the
		# leftmost column at the *end* of the list. This matches with
		# the zero-indexed strings and saves extra calculation.
		twoago, oneago, thisrow = oneago, thisrow, [(0,0)] * len(seq2) + [((x + seq1firstIsOptional)*10,0)]
		for y in xrange(len(seq2)):
			delcost = (oneago[y][0] + 10,oneago[y][1]+(seq1[x] not in k1s))
			addcost = (thisrow[y - 1][0] + 10,thisrow[y - 1][1])
			subcost = (oneago[y - 1][0] + getEditCost(seq1[x],seq2[y]),oneago[y - 1][1]+2*(seq1[x]!=seq2[y] and seq1[x].startswith('CallFunction-') and seq2[y].startswith('CallFunction-'))+(seq1[x] not in k1s))
			thisrow[y] = min(delcost, addcost, subcost)
			# This block deals with transpositions
			if (x > 0 and y > 0 and seq1[x] == seq2[y - 1]
				and seq1[x-1] == seq2[y] and seq1[x] != seq2[y]):
				thisrow[y] = min(thisrow[y], (twoago[y - 2][0] + 10,twoago[y - 2][1]))
	return min(thisrow[len(seq2) - 1],last)

	
def damerau_levenshtein_distance_TrackActions(seq1, seq2, last):
	# Conceptually, this is based on a len(seq1) + 1 * len(seq2) + 1 matrix.
	# However, only the current and two previous rows are needed at once,
	# so we only store those.
	oneago = None
	seq1firstIsOptional = int(not seq1[0].startswith('ReturnedFromFunction-'))
	seq2firstIsOptional = int(not seq2[0].startswith('ReturnedFromFunction-'))
	thisrow = map(lambda n:(n*10,[]),range(seq2firstIsOptional, len(seq2) + seq2firstIsOptional)) + [(0,[])]
	for x in xrange(len(seq1)):
		# Python lists wrap around for negative indices, so put the
		# leftmost column at the *end* of the list. This matches with
		# the zero-indexed strings and saves extra calculation.
		twoago, oneago, thisrow = oneago, thisrow, [(0,[])] * len(seq2) + [((x + seq1firstIsOptional)*10,[])]
		for y in xrange(len(seq2)):
			delcost = (oneago[y][0] + 10,oneago[y][1]+['d'])
			addcost = (thisrow[y - 1][0] + 10,thisrow[y - 1][1]+['a'])
			edit = getEditCost(seq1[x],seq2[y])
			subcost = (oneago[y - 1][0] + edit,oneago[y - 1][1]+(['s'] if edit>0 else ['n']))
			thisrow[y] = min(delcost, addcost, subcost,key=lambda x:x[0])
			# This block deals with transpositions
			if (x > 0 and y > 0 and seq1[x] == seq2[y - 1]
				and seq1[x-1] == seq2[y] and seq1[x] != seq2[y]):
				thisrow[y] = min(thisrow[y], (twoago[y - 2][0] + 10,twoago[y - 2][1]+['r']),key=lambda x:x[0])
	return min(thisrow[len(seq2) - 1],last,key=lambda x:x[0])

def damerau_levenshtein_distance_NoTracking(seq1, seq2, last):
	# Conceptually, this is based on a len(seq1) + 1 * len(seq2) + 1 matrix.
	# However, only the current and two previous rows are needed at once,
	# so we only store those.
	oneago = None
	seq1firstIsOptional = int(not seq1[0].startswith('ReturnedFromFunction-'))
	seq2firstIsOptional = int(not seq2[0].startswith('ReturnedFromFunction-'))
	thisrow = map(lambda n:n*10,range(seq2firstIsOptional, len(seq2) + seq2firstIsOptional)) + [0]
	for x in xrange(len(seq1)):
		# Python lists wrap around for negative indices, so put the
		# leftmost column at the *end* of the list. This matches with
		# the zero-indexed strings and saves extra calculation.
		twoago, oneago, thisrow = oneago, thisrow, [0] * len(seq2) + [(x + seq1firstIsOptional)*10]
		for y in xrange(len(seq2)):
			delcost = oneago[y] + 10
			addcost = thisrow[y - 1] + 10,
			subcost = oneago[y - 1] + getEditCost(seq1[x],seq2[y])
			thisrow[y] = min(delcost, addcost, subcost)
			# This block deals with transpositions
			if (x > 0 and y > 0 and seq1[x] == seq2[y - 1]
				and seq1[x-1] == seq2[y] and seq1[x] != seq2[y]):
				thisrow[y] = min(thisrow[y], twoago[y - 2] + 10)
	return min(thisrow[len(seq2) - 1],last)

"""
def levenshtein_distance(s1, s2):
    if s1 == s2:
        return 0
    rows = len(s1)+1
    cols = len(s2)+1
    if not s1:
        return cols-1
    if not s2:
        return rows-1
    prev = None
    cur = range(cols)
    for r in xrange(1, rows):
        prev, cur = cur, [r] + [0]*(cols-1)
        for c in xrange(1, cols):
            deletion = prev[c] + 1
            insertion = cur[c-1] + 1
            edit = prev[c-1] + (0 if s1[r-1] == s2[c-1] else 1)
            cur[c] = min(edit, deletion, insertion)
    return cur[-1]
"""

def concatParents((b,t,targets)):
	db = getDB(b).hierarchy
	types = set()
	nextTypes = set([t])
	result = set()
	while len(nextTypes) != 0:
		typeVals = nextTypes.pop()
		if typeVals not in types:
			types.add(typeVals)
			nextTypes = nextTypes.union(set(db.find({"binary":b,"type":typeVals}).distinct('parent')))
			result = result.union(targets[typeVals])
	return (t,result)

def concatSiblings(b,c,candidates):
	db = getDB(b).siblings
	result = {}
	function = c[:c.rfind('.')]
	siblings = set([c])
	checked = set()
	while len(siblings) != 0:
		x = siblings.pop()
		checked.add(x)
		if x in candidates.keys():
			for k in candidates[x].keys():
				if k not in result.keys():
					result[k] = set(candidates[x][k])
				else:
					result[k] = result[k].union(set(candidates[x][k]))
		for r in db.find({"binary":b,"function":function,"object":x[x.rfind('.')+1:]}).distinct('sibling'):
			r = function+'.'+r
			if r not in checked:
				siblings.add(r)
	return result

def extendK1s(k1s):
	k1s = set(map(lambda c:c[:-1],k1s))
	added = set()
	for k in k1s:
		if k.startswith('ReadField'):
			added.add('Write'+k[4:])
		if k.startswith('WriteField'):
			added.add('Read'+k[5:])
	return k1s.union(added)

def loadTypeSignature((b,t)):
	db = getDB(b).typesignatures
	return (t,db.find({'binary':b,'typename':t}).distinct('code'),extendK1s(db.find({'binary':b,'typename':t,'k':1}).distinct('code')))

def loadObjectSignature((b,f,o)):
	db = getDB(b).untypedsignatures
	def getCodes():
		ks = {}
		codes = {}
		for k in db.find({'binary':b,'function':f,'object':o}).distinct('k'):
			codes[k] = db.find({'binary':b,'function':f,'object':o,'k':k}).distinct('code')
			ks[k] = db.find({'binary':b,'function':f,'object':o,'k':k}).count()
		return (ks,codes)
	data = getCodes()
	def getMapping():
		mapping = {}
		for r in getDB(b).mapping.find({'binary':b,'function':f,'object':o}):
			mapping[r['type']] = r['base']
		return mapping
	return (f+'.'+o,data[1],db.find({'binary':b,'function':f,'object':o}).distinct('realtype')[0],getMapping(),data[0])

def loadFunctionSignatures(b,f,pool):
	db = getDB(b).untypedsignatures
	return pool.map_async(loadObjectSignature,map(lambda o:(b,f,o),db.find({'binary':b,'function':f}).distinct('object')))

def calcTraceTraceMatch(partsI,j,k1s,score):
	partsJ = j.split(';')[:-1]
	#if len(set(partsI).intersection(set(partsJ))) == 0:
	#	returnm in(max(len(partsI),len(partsJ))*10,score)
	return damerau_levenshtein_distance(partsI,partsJ,k1s,score)
		
def calcTraceTypeMatch(i,t,coverage,k1s):
	global costReadWriteReplace
	if i in t:
		coverage[i] = 0
		return
	partsI = i.split(';')[:-1]
	lenI = len(partsI)
	score = (len(partsI)*10,len(partsI))
	for j in sorted(map(lambda x:(x,abs(x.count(';')-lenI)),t),key=lambda y:y[1]):
		#if j[1]*costReadWriteReplace >= score:
		if (j[1]-1)*costReadWriteReplace >= score:
			break
		score = calcTraceTraceMatch(partsI,j[0],k1s,score)
	score = score[0]+(score[1]*20)
	coverage[i] = score#/float(pow(len(partsI),2))
		
def calcCoverageMatch(c,t,k1s):
	"""
	def calcScore(p1,p2):
		#p1 = map(lambda p: p[:p.find('-')] if p.startswith('Used') else p, p1)
		#p2 = map(lambda p: p[:p.find('-')] if p.startswith('Used') else p, p2)
		return damerau_levenshtein_distance(p1,p2)
	"""
	coverage = {}
	for i in c:
		calcTraceTypeMatch(i,t,coverage,k1s)
	return sum(coverage.values())
	
def calcObjectTypeMatch((binary,oTraces,t,targets,k1s,base)):
	tTraces = set()
	tK1s = set()
	if t in targets.keys():
		tTraces = targets[t]
		tK1s = k1s[t]
	results = {}
	for k in oTraces.keys():
		results[k] = calcCoverageMatch(oTraces[k],tTraces,tK1s)
	results['base'] = base*150
	return (t,results)
		
def splitByK(data):
	result = {}
	for d in data:
		k = d.count(';')
		if k not in result:
			result[k] = set([d])
		else:
			result[k].add(d)
	return result	
	
def calcObjectMatches(binary,o,candidates,targets,mapping,k1s,pool,addSiblings):
	if addSiblings:
		return pool.map_async(calcObjectTypeMatch,map(lambda t:(binary,concatSiblings(binary,o,candidates),t,targets,k1s,mapping[t]),mapping.keys()))
	else:
		return pool.map_async(calcObjectTypeMatch,map(lambda t:(binary,splitByK(candidates[o]),t,targets,k1s,mapping[t]),mapping.keys()))		

def matches_main():
	start = time()
	
	
	candidates = {}
	targets = {}
	types = {}
	mapping = {}
	k1s = {}
	ks = {}

	if len(sys.argv) < 5:
		print 'Usage: python matches.py <binaryName> <1=types, 2=objects> <# of processes> <output file>'
		sys.exit()
	binary = sys.argv[1]
	end = ''
	i = 0
	if binary.startswith('\''):
		end = '\''
	if binary.startswith('\"'):
		end = '\"'
	if end != '':
		binary = binary [1:]
		while not binary.endswith(end):
			i += 1
			if len(sys.argv) < (5+i):
				print 'Usage: python matches.py <binaryName> <1=types, 2=objects> <# of processes> <output file>'
				sys.exit()
			binary += ' '+sys.argv[1+i]
		binary = binary[:-1]
	tORo = int(sys.argv[2+i])
	procs = int(sys.argv[3+i])
	outputfilename = sys.argv[4+i]
	end = ''
	if outputfilename.startswith('\''):
		end = '\''
	if outputfilename.startswith('\"'):
		end = '\"'
	if end != '':
		outputfilename = outputfilename [1:]
		while not outputfilename.endswith(end):
			i += 1
			if len(sys.argv) < (5+i):
				print 'Usage: python matches.py <binaryName> <1=types, 2=objects> <# of processes> <output file>'
				sys.exit()
			outputfilename += ' '+sys.argv[4+i]
		outputfilename = outputfilename[:-1]
	if len(sys.argv) != 5+i:
		print 'Usage: python matches.py <binaryName> <1=types, 2=objects> <# of processes> <output file>'
		sys.exit()
	outputfile = open(outputfilename,'w')

	pool = Pool(processes=procs)

	db = getDB(binary)

	processesTypes = pool.map_async(loadTypeSignature,map(lambda t:(binary,t),db.typesignatures.find({"binary":binary}).distinct('typename')))
	processesObjects = map(lambda f:loadFunctionSignatures(binary,f,pool),db.untypedsignatures.find({"binary":binary}).distinct('function'))

	withEmpty = True

	for r in processesTypes.get():
		targets[r[0]]=r[1]
		k1s[r[0]]=r[2]
	if withEmpty:
		if tORo == 2:
			targets['OmerK-EMPTY'] = set()
			k1s['OmerK-EMPTY'] = set()
	baseMapping = targets.keys()
	for rs in processesObjects:
		for r in rs.get():
			obj = r[0][r[0].rfind('.')+1:]
			if not match('^\[EBP[^\[\]]*\]$',obj):
				candidates[r[0]]=r[1]
				types[r[0]]=r[2]
				mapping[r[0]]=r[3]#.intersection(baseMapping)
				if withEmpty:
					mapping[r[0]]['OmerK-EMPTY'] = 0
				ks[r[0]] = r[4]
	
	results = {}
	
	withParents = {}
	for r in pool.map(concatParents,map(lambda t:(binary,t,targets),targets.keys())):
		withParents[r[0]]=r[1] 
	
	total = 0
	determined = 0
	firstplace = 0
	secondplace = 0
	thirdplace = 0
	empty = 0
	emptys = set()
	notype = 0
	notypes = set()
	rest = 0
	others = set()
	allsame = 0
	sames = set()
	
	outputfile.write('Number of objects: '+str(len(candidates.keys()))+'\n\n')
	results = {}

	tmp = None
	if tORo == 2:
		tmp = map(lambda o:(o,calcObjectMatches(binary,o,candidates,withParents,mapping[o],k1s,pool,True)),candidates.keys())
	if tORo == 1:
		mappingTmp = {}
		for t in withParents.keys():
			mappingTmp[t] = 0
		tmp = map(lambda o:(o,calcObjectMatches(binary,o,withParents,withParents,mappingTmp,k1s,pool,False)),withParents.keys())		
	if tmp:
		for r in tmp:
			o = r[0]
			results[o]={}
			for x in r[1].get():
				results[o][x[0]]=x[1]
		def calcScore(data):
			score = data['base']
			for k in filter(lambda k:k!='base',data.keys()):
				score += data[k]/float(pow(k,2))
			return score
		def sumKs(data):
			minK = min(data.keys())
			maxK = max(data.keys())
			avgK = 0
			count = sum(data.values())
			for k in data.keys():
				avgK += k*data[k]
			avgK /= count
			return 'count='+str(count)+', min='+str(minK)+', max='+str(maxK)+', avg='+str(avgK)
		def printVector(data):
			result = 'base:'+str(data['base'])+', '
			for k in sorted(filter(lambda k:k!='base',data.keys()), reverse=True):
				result += str(k)+':'+str(data[k])+', '
			return '['+result[:-2]+']'
		def prepareVector(v):
			result = [v['base']]
			for k in sorted(filter(lambda k:k!='base',v.keys()),reverse=True):
				result.append(v[k])
			return result
		for o in sorted(results.keys()):
			if tORo == 2:
				outputfile.write(o+' ('+types[o]+')\t'+' ['+sumKs(ks[o])+']\n')
			if tORo == 1:
				outputfile.write(o+'\n')
			#for t in sorted(results[o].keys(), key=lambda k:prepareVector(results[o][k])):
			#	outputfile.write('\t'+t+' : '+printVector(results[o][t])+'\n')
			for t in sorted(results[o].keys(), key=lambda k:calcScore(results[o][k])):
				outputfile.write('\t'+t+' : '+str(calcScore(results[o][t]))+'\t'+printVector(results[o][t])+'\n')
			outputfile.write('\n')
			if tORo == 2:
				if o.endswith('.ECX0'):
					total += 1
					#if len(filter(lambda x:set(x.vlaues()) == set([0]),results[o].values())) > 0: 
					if min(map(lambda x:calcScore(x),results[o].values())) == 0:
						determined += 1
					else:
						if len(results[o].keys()) == (1 if withEmpty else 0):
							empty += 1
							emptys.add(o)
						else:
							realtype = o[:o.find('@@')]
							if realtype.startswith('??'):
								realtype = realtype[2:]
								if len(realtype) > 0:
									while realtype[0] not in ['0','1','2','3','4','5','6','7','8','9']:
										realtype = realtype[1:]
										if len(realtype) == 0:
											break
								if len(realtype) > 0:
									while realtype[0] in ['0','1','2','3','4','5','6','7','8','9']:
										realtype = realtype[1:]
										if len(realtype) == 0:
											break
							else:
								if realtype.startswith('?'):
									realtype = realtype[realtype.find('@')+1:]
							if '@' in realtype:
								realtypeParts = realtype.split('@')
								realtype = ''
								for p in realtypeParts:
									if len(p)!= 0:
										realtype = p+'::'+realtype
								realtype = realtype[:-2]
							if realtype not in results[o].keys():
								notype += 1
								notypes.add(o)
							else:
								#scores = sorted(list(set(map(lambda x:results[o][x],filter(lambda y:y!='OmerK-EMPTY',results[o].keys())))))
								scores = sorted(list(set(map(lambda x:calcScore(results[o][x]),filter(lambda y:y!='OmerK-EMPTY',results[o].keys())))))
								if len(scores) == 1:
									allsame += 1
									sames.add(o)
								else:	
									tagged = False
									#score = results[o][realtype]
									score = calcScore(results[o][realtype])
									if score == min(scores):
										firstplace += 1
										tagged = True
									else:
										if len(scores) > 2:
											if score in scores[:2]:
												secondplace += 1
												tagged = True
											else:
												if len(scores) > 3:
													if score in scores[:3]:
														thirdplace += 1
														tagged = True
									if not tagged:
										rest += 1	
										others.add(o)	
	if tORo == 2:	
		outputfile.write('\n')
		outputfile.write('Total:'+str(total)+'\n')
		outputfile.write('determined:'+str(determined)+'\n')
		outputfile.write('all the same:'+str(allsame)+'\n')
		for x in sames:
			outputfile.write('\t'+x+'\n')
		outputfile.write('not a type (probably bug):'+str(notype)+'\n')
		for x in notypes:
			outputfile.write('\t'+x+'\n')
		outputfile.write('no result (bug):'+str(empty)+'\n')
		#for x in emptys:
		#	outputfile.write('\t'+x+'\n')
		outputfile.write('1st place:'+str(firstplace)+'\n')
		outputfile.write('2nd place (more than 2 scores):'+str(secondplace)+'\n')
		outputfile.write('3nd place (more than 3 scores):'+str(thirdplace)+'\n')
		outputfile.write('others:'+str(rest)+'\n')
		for x in others:
			outputfile.write('\t'+x+'\n')
	
	outputfile.close()
	
	end = time()
	print 'runtime: ' + str(end-start)
	
	"""
	thisProcess = Process().pid
	for proc in process_iter():
		try:
			if proc.name() == 'python.exe':
				if proc.pid != thisProcess:
					proc.kill()
		except psutil.AccessDenied:
			pass
	"""
	
	pool.close()
	pool.join()
	
	print 'Done!'
	
if __name__ == '__main__':
	matches_main()