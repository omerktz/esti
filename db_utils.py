def getDB(binary = None):
    from pymongo import MongoClient
    while True:
        try:
            db = MongoClient()
            break
        except:
            from time import sleep
            sleep(10)
    if binary:
        if '.' in binary:
            binary = binary.replace('.','_')
        if ' ' in binary:
            binary = binary.replace(' ','_')
        #print binary
        db = db['OmerK_Types_'+binary]
    return db

