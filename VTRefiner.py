from imports import *

# this class is used as a bridge between virtual tables code and tracelets existing code
class VTRefiner:
	
	allActions = True
	subobjects = False
	
	class BBVertex:
		def __init__(self,_id,name,ea,size):
			self.id = _id
			self.vals = {}
			self.vals['id'] = _id
			self.vals['name'] = name
			self.succs = set()
			self.preds = set()
			self.start_addr = ea
			self.size = int(size)
		def addPred(self,p):
			self.preds.add(p)
		def addSucc(self,s):
			self.succs.add(s)
		def __getitem__(self,key):
			return self.vals[key]
		def __setitem__(self,key,value):
			self.vals[key]=value
		def successors(self):
			return self.succs
		def predecessors(self):
			return self.preds
		def __hash__(self):
			return hash(self.id)
		def __eq__(self,other):
			return self.id==other.id
		def split(self,codes):
			# split vertex to several vertices, each containing a single code line, while maintaining original control flow
			# return list of new vertices to add
			#print str(self.id) + ' ' + hex(self.start_addr)[:-1] + ' ' + str(codes)
			others = []
			if len(codes) > 0:
				name = self.vals['name']
				name = name[:name.rfind('.')+1]
				record = codes.pop(0)
				#print hex(record[0])[:-1]
				self.vals['code'] = record[1]
				self.start_addr = record[0]
				self.size = ItemSize(self.start_addr)
				if len(codes) > 0:
					i=1
					while len(codes) > 0:
						record = codes.pop(0)
						other = self.__class__(self.id,name+str(i),record[0],ItemSize(record[0]))
						other['code'] = record[1]
						others.append(other)
						i+=1
					others[len(others)-1].succs = self.succs.copy()
					for s in self.succs:
						s.preds.remove(self)
						s.preds.add(others[len(others)-1])
					self.succs = [others[0]]
					others[0].preds = [self]
					for i in xrange(0,len(others)-1):
						others[i].succs = [others[i+1]]
					for i in xrange(1,len(others)):
						others[i].preds = [others[i-1]]
			return others
		def eliminateIfEmpty(self):
			# if vertex has no code, remove it from cfg
			if 'code' not in self.vals.keys():
				for p in self.preds:
					p.succs = p.succs.union(self.succs)
					p.succs.remove(self)
				for s in self.succs:
					s.preds = s.preds.union(self.preds)
					s.preds.remove(self)
				return True
			return False
	class Graph:
		# recreate igraph api for tracelets code
		BFSITER_OUT=1
		BFSITER_IN=2
		BFSITER_ALL=3
		def __init__(self,vs):
			self.vs = vs
		def size(self):
			return len(self.vs)
		def bfsiter(self,vid,mode=1,advanced=False):
			if vid < len(self.vs):
				blocks = [(self.vs[vid],0,None)]
				if mode < 1 or mode > 3:
					raise Exception("invalid mode")
				while len(blocks) > 0:
					v = blocks.pop(0)
					if mode == 1:
						blocks = blocks + map(lambda x:(x,v[1]+1,v[0]),v[0].successors())
					else:
						if mode == 2:
							blocks = blocks + map(lambda x:(x,v[1]+1,v[0]),v[0].predecessors())
						else:
							blocks = blocks + map(lambda x:(x,v[1]+1,v[0]),v[0].successors()) + map(lambda x:(x,v[1]+1),v[0].predecessors())
					if advanced:
						yield v
					else:
						yield v[0]
			raise StopIteration
			
	def __init__(self, ea, withParams = True, inter = False):
		self.ea = ea
		self.withParams = withParams
		self.inter = inter
		self.flow = FlowChart(get_func(ea))
		#self.da = DataAnalyzer(Function(ea))
		#self.da.analyzeFunction()
		from analysisUtils import getDataAnalyzer
		self.da = getDataAnalyzer(ea)
		from vtables import VirtualTableFinder
		self.vts = VirtualTableFinder().find_virtual_tables()
		from GetKnownTypes import TypeFinder
		self.known = TypeFinder().getKnownTypes(self.ea)
		self.calls = self.da.getCalls()
		#print self.calls
		self.fields = self.da.getFields()
		# collect known object and virtual objects
		self.objects = set()#set(self.known.keys()).union(set(self.fields.keys()))
		for c in self.calls.values():
			self.objects = self.objects.union(filter(lambda o:o!='00h',map(lambda x:x['target'][x['target'].find(':')+1:],c)))
		def isNumber(n):
			if n.startswith('[') and n.endswith(']'):
				n = n[1:-1]
			base = 10
			if n.endswith('h'):
				n = n[:-1]
				base = 16
			if n.startswith('0x'):
				n = n[2:]
				base = 16
			try:
				long(n,base)
				return True
			except ValueError:
				return False
		def isStack(o):
			return o in ['ESP','EBP']
		def containsFunctions(o):
			return ('(' in o or ')' in o)
		self.objects = filter(lambda o: not containsFunctions(o), filter(lambda o: not isStack(o), filter(lambda o: not isNumber(o) ,self.objects)))
		self.otherObjects = set(filter(lambda o: o not in self.objects, self.known.keys()))
		self.otherObjects = filter(lambda o: not containsFunctions(o), filter(lambda o: not isStack(o), filter(lambda o: not isNumber(o) ,self.otherObjects)))
		self.funcName = GetFunctionName(ea)
		# create initial graph for function
		self.vs = []
		for index in xrange(0,self.flow.size):
			b = self.flow[index]
			self.vs.append(self.BBVertex(b.id,self.funcName+"."+str(b.id)+".0",b.startEA,b.endEA-b.startEA))
		for index in xrange(0,self.flow.size):
			for s in self.flow[index].succs():
				self.vs[index].addSucc(self.vs[s.id])
				self.vs[s.id].addPred(self.vs[index])
			for s in self.flow[index].preds():
				self.vs[index].addPred(self.vs[s.id])
				self.vs[s.id].addSucc(self.vs[index])
		self.cachedGraphs = {}
		self.cachedSubObjects = {}

	# return all objects found in function
	def getPossibleObjects(self):
		return self.objects

	# return all code lines from block v which are relevant to object o
	# code line can be either ReadField, WriteField, CallFunction
	def getRelevantCode(self,v,o,originalO):
		from StateUtils import simplifyExpression,inlineValues
		from FunctionProvider import LineId
		codes = []
		#if o in self.objects:
		startLine = LineId(v,0)
		endLine = LineId(v,v.size)
		actions = {}
		for obj in filter(lambda obj: obj in o,self.fields.keys()):
			for r in self.fields[obj]:
				if (r[0]<endLine and r[0]>=startLine):
					if r[2]:
						if VTRefiner.allActions or (r[1] != 0):
							actions[r[0]] = ['ReadFieldAtOffset'+str(r[1])]
					else:
						actions[r[0]] = ['WriteFieldAtOffset'+str(r[1])]
		vfuncCalls = set()
		for l in filter(lambda x:(x<endLine and x>=startLine),self.calls.keys()):
			for r in self.calls[l]:
				if r['target'][r['target'].find(':')+1:] in o:
					actions[l] = ['CallFunctionAtOffest'+str(r['offset'])]
					vfuncCalls.add(l)
					break
		if self.withParams:
			for l in filter(lambda x:(x<endLine and x>=startLine),self.da.getAllCalls()):
				#print l
				#print str(l)
				isArg = False
				isThis = False
				argIndex = []
				prevStates = filter(lambda x:x<l,self.da.getStatesPerLine().keys())
				states = None
				if len(prevStates) > 0:
					lastState = max(prevStates)
					states = self.da.getStatesPerLine()[lastState]
				else:
					from State import State
					states = [State(0)]
				#print len(states)
				for state in states:
					#print state.topOfArgs[0]
					esp = state.registers['esp']
					espHasFunc = ('(' in esp) or (')' in esp)
					top = 'ESP'
					if len(state.topOfArgs) != 0:
						top = state.topOfArgs[0]
					topHasFunc = ('(' in top) or (')' in top)
					if (not espHasFunc) or topHasFunc:
						if not match('^ESP\+[0-9a-fA-F]+h$',esp):
							i=1
							while esp != 'ESP' and esp != top:
								#print esp+' \t'+str(state.topOfArgs)
								if esp in state.stack.keys():
									if state.stack[esp] in o:
										isArg = True
										argIndex.append(i)
								esp = simplifyExpression(esp+'+4')
								i+=1
					if state.registers['ecx'] in o:
						isThis = True
					if isArg and isThis:
						break
				OpHex(long(l),0)
				opnd = GetOpnd(long(l),0)
				OpOff(long(l),0,0)
				try:
					line = long(opnd,16)
					while GetMnem(line)=='jmp':
						OpHex(long(l),0)
						opnd = GetOpnd(line,0)
						OpOff(long(l),0,0)
						line = long(opnd,16)
				except ValueError:
					pass
				if LocByName(opnd) != 0xffffffffffffffff:
					line = LocByName(opnd)
					while GetMnem(line)=='jmp':
						OpHex(long(l),0)
						opnd = GetOpnd(line,0)
						OpOff(long(l),0,0)
						line = LocByName(opnd)				
				if match('^d?word ptr ',opnd) or match('^byte ptr ',opnd):
					opnd = opnd[opnd.find('ptr ')+4:]
				if match('^[scdefg]s:',opnd):
					opnd = opnd[3:]
				if opnd.startswith('[') and opnd.endswith(']'):
					opnd = opnd[1:-1]
				def replaceOriginalO(target):
					matchedObjs = set()
					for obj in originalO:
						if obj in target:
							matchedObjs.add(obj)
					if '00h' in target:
						matchedObjs.add('00h')
					if len(matchedObjs) == 0:
						return target
					longestObj = max(map(lambda x:len(x),matchedObjs))
					matchedObjs = filter(lambda obj:len(obj)==longestObj,matchedObjs)
					if len(matchedObjs) > 1:
						raise Exception('found 2 maximal object expression in target function expression')
					return target.replace(matchedObjs.pop(),'OBJECT')
				targets = set(map(lambda s:inlineValues(opnd,s),states))
				targets = set(map(lambda s:replaceOriginalO(s),targets))
				target = 'UndeterminedTarget'
				if len(targets) == 1:
					target = targets.pop()
					if match('^[0-9a-fA-F]+h$',target):
						target = target[:-1]
						try:
							line = long(target,16)
							if GetFunctionName(line) != '':
								target = GetFunctionName(line)
						except ValueError:
							pass
					demangled = Demangle(target,0)
					if demangled:
						demangled = demangled[:demangled.rfind('(')]
						demangled = demangled[demangled.rfind(' ')+1:]
						if len(demangled) > 0:
							target = demangled
				if isArg or isThis:
					if l not in actions.keys():
						actions[l] = []
					if isThis:
						if target not in ['new','delete']:
							if VTRefiner.allActions or (l not in vfuncCalls):
								if len(filter(lambda x:x.startswith('CallFunctionAtOffest'),actions[l])) == 0:
									actions[l].append('CallFunction-'+target)
								actions[l].append('UsedAsThisArgument-'+target)
					if isArg:
						for i in set(argIndex):
							actions[l].append('UsedAsCallParameter-#'+str(i)+'-'+target)
					actions[l].reverse()
				returned = False
				for s in map(lambda s:[s.registers['eax'],s.registers['ecx'],s.registers['edx']],self.da.getStatesPerLine()[l]):
					if len(filter(lambda obj: obj in s, o)) != 0:
						returned = True
				if returned:
					if l not in actions.keys():
						actions[l] = []
					actions[l].append('ReturnedFromFunction-'+target)
				if self.inter:
					for state in states:
						opnd = inlineValues(GetOpnd(long(l),0),state)
						if opnd.startswith('[') and opnd.endswith(']'):
							try:
								ea = long(opnd[1:-2],16)
								f = get_func(ea)
								if f:
									if f.startEA == ea: 
										if l not in actions.keys():
											actions[l] = []
										actions[l].append('InterCall'+opnd)
										break
							except ValueError:
								pass
						if LocByName(opnd) != 0xffffffffffffffff:
							if l not in actions.keys():
								actions[l] = []
							actions[l].append('InterCall'+opnd)
							break
		for l in sorted(actions.keys()):
			for c in actions[l]:
				codes.append((long(l),c))
		return codes
	
	def includeSubObjects(self,o):
		if not VTRefiner.subobjects:
			return o
		if o not in self.cachedSubObjects.keys():
			newO = set()
			allObjs = set()
			nextObjs = set(o[:])
			while len(nextObjs) > 0:
				obj = nextObjs.pop()
				if obj not in allObjs:
					allObjs.add(obj)
					newO.add(obj)
					if obj in self.fields.keys():
						for f in self.fields[obj]:
							nextObjs.add('['+obj+'+'+hex(f[1])[2:]+'h]')
					for l in self.calls.keys():
						for r in self.calls[l]:
							if r['target'][r['target'].find(':')+1:] == obj:
								nextObjs.add(self.da.getStatesPerLine()[l][0].registers['eax'])
								break
			self.cachedSubObjects[o] = list(newO)
		return self.cachedSubObjects[o]
	
	# return usage graph for object o as seen in function
	def refineGraph(self,o,trace2code = None):
		if o in self.cachedGraphs.keys():
			return self.cachedGraphs[str(sorted(o))]
		def copyVS(vs):
			newVS = []
			index = {}
			for i in xrange(0,len(vs)):
				v = vs[i]
				newV = VTRefiner.BBVertex(v.id,v.vals['name'],v.start_addr,v.size)
				newVS.append(newV)
				index[v.id] = newV
			for i in xrange(0,len(vs)):
				v = vs[i]
				newV = newVS[i]
				for s in v.succs:
					newV.addSucc(index[s.id])
				for p in v.preds:
					newV.addPred(index[p.id])
			return newVS
		newO = self.includeSubObjects(o)
		vs = copyVS(self.vs)
		toAdd = []
		for index in xrange(0,len(vs)):
			#print str(index)+'/'+str(len(vs))
			vertex = vs[index]
			#print hex(vertex.start_addr)[:-1]
			codes = self.getRelevantCode(vertex,newO,o)
			toAdd = toAdd + vertex.split(codes)
		vs = vs + toAdd
		toRemove = []	
		for v in vs:
			if v.eliminateIfEmpty():
				toRemove.append(v)
			else:
				if trace2code != None:
					trace2code[v['name']] = v['code']
		for v in toRemove:
			while v in vs:
				vs.remove(v)
		for i in xrange(0,len(vs)):
			vs[i].index = i
		g = self.Graph(vs)
		self.cachedGraphs[str(sorted(o))] = g
		return g;
	
	def getOtherObjects(self):
		return self.otherObjects

if __name__ == '__main__':
	def getGraphForObject(obj):
		vtr = VTRefiner(ScreenEA())
		print vtr.getPossibleObjects()
		print vtr.getOtherObjects()
		return vtr.refineGraph(obj)
	#vtr = VTRefiner(True,True)
	#print vtr.getPossibleObjects()
	#g = vtr.refineGraph('ECX0')
	g = getGraphForObject(['ESP-0344h'])
	print len(g.vs)
	#for (v,d,f) in g.bfsiter(0,VTRefiner.Graph.BFSITER_OUT,True):
	for v in g.vs:
		#print (v,d,f)
		#print v.vals
		#print hex(v.start_addr)[:-1]
		#print v['name'] + ' ' + str(map(lambda x:x['name'],v.successors()))
		print v['name'] + ': ' + v['code'] + ' (' + hex(v.start_addr)[:-1] + ')'
		print '\t'+str(map(lambda x:x['name'],v.successors()))
		#print v.predecessors()
		pass
	print 'Done!'