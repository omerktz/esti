from imports import *

class VirtualTableFinder():

	class Addressable:
		TYPE_NAME = "Addressable"
		def __init__(self, ea, num_of_args=None, name=None):
			self.ea = ea
			self.name = name if name else "%s_%x" % (self.__class__.TYPE_NAME,ea)
			self.num_of_args = num_of_args if ((num_of_args != None) and (num_of_args >= 0)) else 0
	
		def __hash__(self):
			return self.ea
	
		def __eq__(self, other):
			return isinstance(other, self.__class__) and self.ea == other.ea
	
		def __repr__(self):
			return "%s@%s(0x%x)" % (self.name, str(self.num_of_args) if (self.num_of_args != None) else '?', self.ea)
	
	
	class ClassFunction(Addressable):
		TYPE_NAME="ClassFunction"
		def __init__(self, ea, num_of_args=None, name=None):
			from func_utils import get_function_args_size
			VirtualTableFinder.Addressable.__init__(self, ea, num_of_args if (num_of_args != None) else get_function_args_size(ea), name if name else GetFunctionName(ea))
	
	class Constructor(ClassFunction):
		TYPE_NAME="Constructor"
	
	class VirtualFunction(ClassFunction):
		TYPE_NAME="VirtualFunction"
		def __init__(self, ea, num_of_args=None, name=None):
			VirtualTableFinder.ClassFunction.__init__(self, ea, num_of_args, name)
			self.virtual_tables = []
		
		def bind_to_table(self, table):
			if not table in self.virtual_tables:
				self.virtual_tables.append(table)
	
	class VirtualTable(Addressable):
		TYPE_NAME="VirtualTable"
	
		def __init__(self, ea, functions, classes=[], name=None):
			VirtualTableFinder.Addressable.__init__(self, ea, name)
			self.functions = functions
			self.classes = classes
			self.alloc = 0
			self.foundAllocation = False
			for func in self.functions:
				func.bind_to_table(self)
	
		def bind_to_class(self, cls, vtable_offset=0):
			if not cls in self.classes:
				self.classes.append( (vtable_offset, cls) )
	
		def get_main_class(self):
			for vtable_offset, cls in self.classes:
				if vtable_offset == 0:
					return cls
			return None
	
		def __len__(self):
			return len(self.functions)
	
		def __iter__(self):
			for func in self.functions:
				yield func
	
		def __repr__(self):
			s = VirtualTableFinder.Addressable.__repr__(self)
			s += "\n"
			for func in self:
				s += "\t"
				s += str(func)
				s += "\n"
			cls = self.get_main_class()
			if cls:
				s += cls
			return s
			
		def isInTable(self,ea):
			return ea in map(lambda x:x.ea, self.functions)
		
	UNKNOWN_OFFSET = 0xFFFFFFFF
	class VirtualClass(Addressable):
		TYPE_NAME="VirtualClass"
	
		def __init__(self, constructor, virtual_tables=[]):
			VirtualTableFinder.Addressable.__init__(self, constructor.ea, constructor.name)
			self.constructor = constructor
			for offset, vtable in virtual_tables:
				vtable.bind_to_class(offset, vtable)
	
	
	
	def get_data_refs(self,ea):
		from_refs  = [x for x in DataRefsFrom(ea)]
		to_refs   = [x for x in DataRefsTo(ea)]
	
		from_ref = None
		if from_refs:
			from_ref = from_refs[0]
		return (from_ref, to_refs)
	
	virtual_tables = None
	def find_virtual_tables(self):
		def isVTassigned(to_refs,ea):
			def getOpnd(ea,n):
				OpHex(ea,n)
				o = GetOpnd(ea,n)
				OpOff(ea,n,0)
				if n==1:
					if o.endswith('h'):
						o=o[:-1]
					if o.startswith('0x'):
						o=o[2:]
					try:
						o=long(o,16)
					except ValueError:
						pass
				#print o
				return o
			from analysisUtils import isNotLibrary
			if isNotLibrary():
				to_refs = filter(lambda x:GetMnem(x)=='mov',to_refs)
				to_refs = filter(lambda x:getOpnd(x,1)==ea,to_refs)
				to_refs = filter(lambda x:match('^(d?word ptr )?\[.+\]$',getOpnd(x,0)),to_refs)
				return len(to_refs)!=0
			return True
		if not VirtualTableFinder.virtual_tables:
			virtual_functions = {}
			virtual_tables    = {}
			#classes = {}
			#constructors = {}
	
			# enumerate the entire data base and look for possible vtables.
			curr_ea = get_imagebase()
			while curr_ea != BADADDR:
				#print hex(curr_ea)
				# get the head flags
				curr_flags = GetFlags(curr_ea)
				# check if its data or code.
				if isData(curr_flags):
					# check if this location has code references if not
					# its not interesting
					from_ref, to_refs = self.get_data_refs(curr_ea)
					
					#  if it is referenced and it references a function it might
					#  be a virtual table
					if from_ref and to_refs:# and GetFunctionName(from_ref):
						if isVTassigned(to_refs,curr_ea):
							vtable_ea = curr_ea
		
							# if we have a single reference to the table.
							# its probably the main constructor.
							"""
							vtable_class_constructors = []
							for ea in to_refs:
								constructor = constructors.setdefault(ea,VirtualTableFinder.Constructor(ea))
								vtable_class_constructors.append(constructor)
							"""
		
							vfunctions = []
							vtable_ea = curr_ea
		
							# get the functions
							while curr_ea and from_ref:# and GetFunctionName(from_ref):
								
								vfunc = virtual_functions.setdefault(from_ref, VirtualTableFinder.VirtualFunction(from_ref))
								# add the virtual function
								if (len(GetMnem(from_ref).strip()) > 0): #and (NextHead(PrevHead(from_ref)) == from_ref) # should eliminate rtti
									vfunctions.append(vfunc)
		
								# There should be no references to the middle
								# of a virtual table
								#curr_ea = NextHead(curr_ea)
								curr_size = ItemSize(curr_ea)
								curr_ea += curr_size
								from_ref, to_refs = self.get_data_refs(curr_ea)
								if to_refs:
									# go back as we are about to advance
									curr_ea -= curr_size
									break
		
							# create the virtual table
							if len(vfunctions) > 0:
								virtual_tables[vtable_ea] = VirtualTableFinder.VirtualTable(vtable_ea, vfunctions)
		
								"""
								# check for 1 constructor
								if len(vtable_class_constructors) == 1:
									cls = classes.setdefault(ea, VirtualTableFinder.VirtualClass(vtable_class_constructors[0], [(0, virtual_tables[vtable_ea] )]))
								"""
							
				# advance to the next head 
				#print hex(curr_ea)+'\t'+hex(NextHead(curr_ea))
				curr_ea = NextHead(curr_ea)
			"""
			# remove vtable that with first entry pointing to another vtable, probably RTTI or some other shit like that
			vteas = virtual_tables.keys()
			remove = set()
			for vt in virtual_tables.keys():
				if virtual_tables[vt].functions[0].ea in vteas:
					remove.add(vt)
			for vt in remove:
				del virtual_tables[vt]
			"""
			VirtualTableFinder.virtual_tables = virtual_tables
			
		return VirtualTableFinder.virtual_tables

def vtables_main():
	autoWait()
	vtables = VirtualTableFinder().find_virtual_tables().values()
	#for vtable in vtables:
	#	print vtable
	#print len(vtables)
	f = open(get_root_filename()+'.sizes','w')
	#from idaapi import get_root_filename
	f.write(get_root_filename()+'\t'+str(len(vtables))+'\n')
	f.close()
	from db_utils import getDB
	b = get_root_filename()
	unstrippedNames = {}
	fUnstrippedNames = open('./matchResultsUnstripped/'+b+'.addresses','r')
	line = fUnstrippedNames.readline()
	while line != '':
		s = line.split('\t')
		unstrippedNames[s[1].strip()] = s[0].strip()
		line = fUnstrippedNames.readline()
	fUnstrippedNames.close()
	db = getDB(b).vtables
	for v in vtables:
	 for i in xrange(0,len(v.functions)):
	  db.insert({"binary":b,"vtable":hex(v.ea)[2:],"vtableName":unstrippedNames[hex(v.ea)] if hex(v.ea) in unstrippedNames.keys() else Name(v.ea)[2:] ,"index":i,"function":v.functions[i].ea if len(Name(v.functions[i].ea))>0 else None})
	#from idaapi import qexit
	qexit(0)
	print 'Done!'

if __name__ == '__main__':
	vtables_main()