import numpy
from re import match
import sys
import os
from pymongo import MongoClient
from mimify import File
if os.environ.get('OMERK_PROFILING'):
	from multiprocessingMock import Pool
else:
	from multiprocessing import Pool

def filterObjects(o):
	if binary in ignores.keys():
		if o in ignores[binary]:
			return False
	o = o[o.rfind('.')+1:]
	if o.startswith('[EBP'):
		return False
	def is_object(o):
		if '[' not in o and ']' not in o:
			return True
		matched = match('^\[(.+)\]([\+\-]0[0-9a-fA-F]+h)?$',o)
		if matched:
			return is_object(matched.groups()[0])
		else:
			return False
	return is_object(o)

def countFuncsForCallLoc(callLoc):
	global resolvedCalls
	function = set(db.objectsInCalls.find({'binary':binary,'line':callLoc}).distinct('function')).pop()
	count = set()
	total = set()
	for matchedObject in db.objectsInCalls.find({'binary':binary,'line':callLoc,'function':function}).distinct('object'):
		if match('^0[0-9A-Fa-f]+h$',matchedObject) and len(matchedObject) == 8:
		  if matchedObject[1:-1] in db.vtables.distinct('vtable'):
			offset = set(db.objectsInCalls.find({'binary':binary,'line':callLoc,'function':function,'object':matchedObject}).distinct('offset')).pop()
			count.add(1)
			total.add(numFuncsPerOffset[offset])				  
		else:		
		  if filterObjects(matchedObject):
			  offset = set(db.objectsInCalls.find({'binary':binary,'line':callLoc,'function':function,'object':matchedObject}).distinct('offset')).pop()
			  fullObj = unstripped2stripped[function]+'.'+matchedObject
			  if fullObj in objects.keys():
				  matchedTypes = objects[fullObj]
				  matchedFuncs = set(map(lambda t: typesCache[t][offset],filter(lambda y: offset in typesCache[y].keys(), filter(lambda x: x in typesCache.keys(),matchedTypes))))
				  if len(matchedFuncs) > 0:
					count.add(len(matchedFuncs))
					total.add(numFuncsPerOffset[offset])
	if len(count) > 0:
		if len(count) != 1:
			print 'WARNING: function up-to count inconsistent among objects (callLoc='+str(callLoc)+')'
		resolvedCalls += 1
		return (min(count),100*min(count)/min(total))
	return None

objects = {}
def statFile(fileInput):
	global resolvedCalls
	fIn = open(folder+'matchResults/'+fileInput,'r')
	fIn.readline()
	fIn.readline()
	line = fIn.readline()
	object = None
	type = None
	lastScore = ''
	position = -1
	total = 0
	lastLineEmpty = False
	countUpTo = True
	upTo = set()
	finalScore = False
	#fOut.write('name\ttype\tscore_position\ttotal_scores\tfound_0\ttypes_up_to\tvariance\tmean\tvmr\n')
	types = 0
	exact = 0
	unmatched = 0
	totalObjects = 0
	matchedObjects = 0
	while line: # != '':
		#print line
		if line.strip() == '':
			if lastLineEmpty:
				break
			if filterObjects(object):
				totalObjects += 1
				if type != 'Unknown':
					#fOut.write(str(object)+'\t'+str(type)+'\t'+str(position)+'\t'+str(total)+'\t'+str(otherMatched)+'\t'+str(upTo)+'\t'+str(numpy.var(scores))+'\t'+str(numpy.mean(scores))+'\t'+str(0 if numpy.mean(scores)==0 else (numpy.var(scores)/numpy.mean(scores)))+'\n')
					if position == -1:
						unmatched += 1
					else:
						if types == 1:
							exact += 1
						objects[object] = upTo
						matchedObjects += 1
			object = None
			type = None
			lastScore = ''
			position = -1
			total = 0
			lastLineEmpty = True
			upTo = set()
			countUpTo = True
			finalScore = False
			types = 0
			rank = 0
		else:
			lastLineEmpty = False
			if line.startswith('\t'):
				tmp = line[1:]
				tmp = tmp[:tmp.find('\t')].strip()
				name = tmp[:tmp.rfind(':')-1]
				score = tmp[tmp.rfind(':')+2:]
				if name != 'OmerK-EMPTY':
					types += 1
					if score != lastScore:
						if finalScore:
							countUpTo = False
						lastScore = score
						total += 1
					if name == type:
						position = total
						finalScore = True
					if countUpTo:
						upTo.add(name)
			else:
				object = line[:line.find(' ')]
				tmp = line[len(object)+1:]
				type = tmp[:tmp.find('\t')][1:-1]
		line = fIn.readline()

	pool = Pool(processes=int(sys.argv[3]))
	
	callLocations = db.objectsInCalls.find({'binary':binary}).distinct('line')
	numCalls = len(callLocations)
	tmp = filter(lambda x:x, map(countFuncsForCallLoc,callLocations))
	uptos = map(lambda x:x[0], tmp)
	uptosRatio = map(lambda x:x[1], tmp)
	pool.close()
	pool.join()
	#print uptos
	
	# calculate data for figure
	uptosHistogram = {}
	for i in xrange(int(sizes[binary])+1):
		uptosHistogram[i] = 0
	for i in uptos:
		uptosHistogram[i] += 1
	#print uptosHistogram
	count = 0
	for i in sorted(uptosHistogram.keys()):
		count += uptosHistogram[i]
		uptosHistogram[i] = count
	for i in uptosHistogram.keys():
		if len(uptos) > 0:
				uptosHistogram[i] = 100*uptosHistogram[i]/len(uptos)
		else:
				uptosHistogram[i] = 100

	fIn.close()
	return (uptosHistogram,str(resolvedCalls)+'/'+str(numCalls))

def createSubPlot(p,h,b,t,i):
	x = sorted(h.keys())
	y = map(lambda i:h[i],x)
	plt.tick_params(axis='both', which='major', labelsize=20)
	plt.tick_params(axis='both', which='minor', labelsize=10)
	p.set_yticks(numpy.arange(0,110,10))
	xticks = numpy.arange(0,max(h.keys())+1,1)
	p.set_xticks(xticks,map(lambda x: str(x) if x % 5 == 0 else '', xticks))
	p.fill_between(x, 0, y, facecolor='blue', alpha=0.3)
	p.plot(x, y, color='blue', linewidth=3.0, label=b)
	p.grid(True)
	p.set_ylabel('% of Call Sites covered', fontsize=30)
	p.set_xlabel('Number of Analyzed Fucntions', fontsize=30)
	p.set_title(b+' ('+t+')', fontsize=30)

	
folder = sys.argv[1]
if not folder.endswith('/'):
	folder += '/'
histograms = {}
for b in os.listdir(folder):
	if b.endswith('.funcs.ranks'):
		if len([l for l in open(folder+b,'r')]) == 3:
			binary = b[:-12]
			print binary
			resolvedCalls = 0
			sizes = {}
			fSizes = open(folder+binary+'.sizes','r')
			line = fSizes.readline()
			while line != '':
				s = line.strip().split('\t')
				sizes[s[0]] = s[1]
				line = fSizes.readline()
			fSizes.close()
			ignores = {}
			fIgnores = open(folder+binary+'.ignores','r')
			line = fIgnores.readline()
			while line != '':
				s = line.strip().split('\t')
				if s[0] not in ignores.keys():
				ignores[s[0]] = set()
				ignores[s[0]].add(s[1])
				line = fIgnores.readline()
			fIgnores.close()
			unstripped2stripped = {}
			fUnstrippedNames = open(folder+'matchResultsUnstripped/'+binary+'.addresses','r')
			line = fUnstrippedNames.readline()
			while line != '':
				s = line.split('\t')
				unstripped2stripped[s[1].strip()] = s[0].strip()
				line = fUnstrippedNames.readline()
			fUnstrippedNames.close()   
			db = MongoClient()['OmerK_Types_'+binary.replace(' ','_').replace('.','_')]   
			typesCache = {}
			numFuncsPerOffset = {}
			offsets = set()
			for typename in db.vtables.find({'binary':binary}).distinct('vtableName'):
				typeFuncs = {}
				for f in db.vtables.find({'binary':binary,'vtableName':typename}):
				typeFuncs[f['index']] = f['function']
				offsets.add(f['index'])
				typesCache[typename] = typeFuncs
			for i in offsets:
			  numFuncsPerOffset[i] = len(set(map(lambda x: typesCache[x][i], filter(lambda t:i in typesCache[t].keys(),typesCache.keys()))))

			histograms[binary] = statFile(binary+'.matches')

print len(histograms.keys())

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
numRows = 5
numCols = 7
width = 20
height = 10
fig,ax = plt.subplots(numRows,numCols,figsize=(numCols*width,numRows*height))
i=0
j=0
for b in histograms.keys():
	(h,t) = histograms[b]
	createSubPlot(ax[i][j],h,b,t,i)
	j+=1
	if j == numCols:
		j=0
		i+=1
plt.savefig(folder+sys.argv[2],format='png',transparent=True)

