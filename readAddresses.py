#from idc import *
#from idaapi import *
#from idautils import *
from idaCache import *

from vtables import VirtualTableFinder


fIn = open('/home/omerkatz/vtsPomela/benchmarks/matchResultsStripped/'+get_root_filename()+'.matches','r')
fOut = open('/home/omerkatz/vtsPomela/benchmarks/matchResultsStripped/'+get_root_filename()+'.addresses','w')

line = fIn.readline()
line = fIn.readline()
line = fIn.readline()
names = set()
while not line.startswith('Total:'):
	if line.strip() != '':
		if line.startswith('\t'):
			names.add(line[:line.find(':')].strip())
		else:
			names.add(line[:line.find('.')].strip())
	line = fIn.readline()
fIn.close()

for n in names:
	if n != 'OmerK-EMPTY':
		addr = LocByName(n)
		if addr != 0xffffffffffffffff:
			fOut.write(n+'\t'+hex(addr)+'\n')
for v in VirtualTableFinder().find_virtual_tables().values():
	if Name(v.ea) != '':
		name = Name(v.ea)
		if Demangle(name,0):
			name = Demangle(name,0)
			if name.startswith('const '):
				name = name[len('const '):]
			if name.endswith("::`vftable'"):
				name = name[:-len("::`vftable'")]
		fOut.write(name+'\t'+hex(v.ea)+'\n')
fOut.close()

print 'Done!'

qexit(0)
