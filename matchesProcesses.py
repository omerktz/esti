from imports import *
from multiprocessing import Process,Manager

def getEditCost(t1,t2):
	if t1==t2:
		return 0
	if (t1.startswith('Read') or t1.startswith('Write')) and (t2.startswith('Read') or t2.startswith('Write')):
		if t1[t1.find('-'):] == t2[t2.find('-'):]:
			return 5
	return 10
	
def damerau_levenshtein_distance(seq1, seq2):
	# Conceptually, this is based on a len(seq1) + 1 * len(seq2) + 1 matrix.
	# However, only the current and two previous rows are needed at once,
	# so we only store those.
	oneago = None
	seq1firstIsOptional = int(not seq1[0].startswith('ReturnedFromFunction-'))
	seq2firstIsOptional = int(not seq2[0].startswith('ReturnedFromFunction-'))
	thisrow = map(lambda n:n*10,range(seq2firstIsOptional, len(seq2) + seq2firstIsOptional)) + [0]
	for x in xrange(len(seq1)):
		# Python lists wrap around for negative indices, so put the
		# leftmost column at the *end* of the list. This matches with
		# the zero-indexed strings and saves extra calculation.
		twoago, oneago, thisrow = oneago, thisrow, [0] * len(seq2) + [(x + seq1firstIsOptional)*10]
		for y in xrange(len(seq2)):
			delcost = oneago[y] + 10
			addcost = thisrow[y - 1] + 10
			subcost = oneago[y - 1] + getEditCost(seq1[x],seq2[y])
			thisrow[y] = min(delcost, addcost, subcost)
			# This block deals with transpositions
			if (x > 0 and y > 0 and seq1[x] == seq2[y - 1]
				and seq1[x-1] == seq2[y] and seq1[x] != seq2[y]):
				thisrow[y] = min(thisrow[y], twoago[y - 2] + 10)
	return thisrow[len(seq2) - 1]

"""
def levenshtein_distance(s1, s2):
    if s1 == s2:
        return 0
    rows = len(s1)+1
    cols = len(s2)+1
    if not s1:
        return cols-1
    if not s2:
        return rows-1
    prev = None
    cur = range(cols)
    for r in xrange(1, rows):
        prev, cur = cur, [r] + [0]*(cols-1)
        for c in xrange(1, cols):
            deletion = prev[c] + 1
            insertion = cur[c-1] + 1
            edit = prev[c-1] + (0 if s1[r-1] == s2[c-1] else 1)
            cur[c] = min(edit, deletion, insertion)
    return cur[-1]
"""
	
def concatParents(b,t,targets):
	db = getDB(b).hierarchy
	types = set()
	nextTypes = set([t])
	result = set()
	while len(nextTypes) != 0:
		typeVal = nextTypes.pop()
		if typeVal not in types:
			types.add(typeVal)
			nextTypes = nextTypes.union(set(db.find({"binary":b,"type":typeVal}).distinct('parent')))
			result = result.union(targets[typeVal])
	return result


def concatSiblings(b,c,candidates):
	db = getDB(b).siblings
	result = set()
	function = c[:c.rfind('.')]
	for r in db.find({"binary":b,"function":function,"object":c[c.rfind('.')+1:]}).distinct('sibling'):
		obj = function+'.'+r
		if obj in candidates.keys():
			result = result.union(candidates[obj])
	return result

def loadTypeSignature(b,t,res):
	res[t] = getDB(b).typesignatures.find({'binary':b,'typename':t}).distinct('code')

def loadObjectSignature(b,f,o,res,types):
	db = getDB(b).untypedsignatures
	obj = f+'.'+o
	res[obj] = db.find({'binary':b,'function':f,'object':o}).distinct('code')
	types[obj] = db.find({'binary':b,'function':f,'object':o}).distinct('realtype')[0]

def loadFunctionSignatures(b,f,res,types):
	db = getDB(b).untypedsignatures
	processes = []
	for o in db.find({'binary':b,'function':f}).distinct('object'):
		p = Process(target=loadObjectSignature,args=(b,f,o,res,types))
		p.start()
		processes.append(p)
	for p in processes:
		p.join()

def calcTraceTraceMatch(partsI,j,scores):
	partsJ = j.split(';')[:-1]
	scores[j] = damerau_levenshtein_distance(partsI,partsJ)
		
def calcTraceTypeMatch(i,t,coverage):
	partsI = i.split(';')[:-1]
	scores = {}
	for j in t:
		calcTraceTraceMatch(partsI,j,scores)
	coverage[i] = min(scores.values())/float(pow(len(partsI),2))
		
def calcCoverageMatch(c,t):
	"""
	def calcScore(p1,p2):
		#p1 = map(lambda p: p[:p.find('-')] if p.startswith('Used') else p, p1)
		#p2 = map(lambda p: p[:p.find('-')] if p.startswith('Used') else p, p2)
		return damerau_levenshtein_distance(p1,p2)
	"""
	coverage = {}
	for i in c:
		calcTraceTypeMatch(i,t,coverage)
	return sum(coverage.values())
	
def calcObjectTypeMatch(binary,oTraces,t,targets,results):
	results[t] = calcCoverageMatch(oTraces,targets[t])
		
def calcObjectMatches(binary,o,candidates,targets,results,finished):
	oTraces = concatSiblings(binary,o,candidates)
	processes = []
	for t in targets.keys():
		p = Process(target=calcObjectTypeMatch,args=(binary,oTraces,t,targets,results))
		p.start()
		processes.append(p)
	for p in processes:
		p.join()
	finished.append(o)
	print 'Finished '+str(len(finished))+'/'+str(len(candidates))

if __name__ == '__main__':
	start = time()

	m = Manager()
	
	candidates = m.dict()
	targets = m.dict()

	types = m.dict()

	if len(sys.argv) != 4:
		print 'Usage: python matches.py <binaryName> <1=types, 2=objects> <output file>'
		sys,exit()
	binary = sys.argv[1]
	tORo = int(sys.argv[2])
	outputfile = open(sys.argv[3],'w')
	
	db = getDB(binary)

	processesTypes = []
	for t in db.typesignatures.find({"binary":binary}).distinct('typename'):
		p = Process(target=loadTypeSignature,args=(binary,t,targets))
		p.start()
		processesTypes.append(p)
	
	processesObjects = []
	for f in db.untypedsignatures.find({"binary":binary}).distinct('function'):
		p = Process(target=loadFunctionSignatures,args=(binary,f,candidates,types))
		p.start()
		processesObjects.append(p)

	for p in processesTypes:
		p.join()
	for p in processesObjects:
		p.join()

	results = {}

	withParents = {}
	for t in targets.keys():
		withParents[t] = concatParents(binary,t,targets)
	
	if tORo == 2:
		outputfile.write('Number of objects: '+str(len(candidates.keys()))+'\n\n')
		processes = []
		finished = m.list()
		for o in candidates.keys():
			results[o] = m.dict()
			p = Process(target=calcObjectMatches,args=(binary,o,candidates,withParents,results[o],finished))
			p.start()
			processes.append(p)
		for p in processes:
			p.join()
		for o in sorted(candidates.keys()):
			outputfile.write(o+' ('+types[o]+')\n')
			for t in sorted(targets.keys(), key=results[o].get):
				outputfile.write('\t'+t+' : '+str(results[o][t])+'\n')
			if types[o] not in db.typed.distinct("typename"):
				outputfile.write('Weird\n')
			else:
				if types[o] in filter(lambda t: results[o][t]==min(results[o].values()),results[o].keys()):
					if max(results[o].values())==min(results[o].values()):
						outputfile.write('Undetermined\n')
					else:
						outputfile.write('Good\n')
				else:
					outputfile.write('Bad\n')
			outputfile.write('\n')
	if tORo == 1:
		for o in targets.keys():
			results[o] = {}
			outputfile.write(o+'\n')
			for t in targets.keys():
				results[o][t] = calcCoverageMatch(targets[o].keys(),concatParents(t,targets))
			for t in sorted(targets.keys(), key=results[o].get):
				outputfile.write('\t'+t+' : '+str(results[o][t])+'\n')
			outputfile.write('\n')
		outputfile.write('==============================\n')
		for o in targets.keys():
			results[o] = {}
			outputfile.write(o+'\n')
			for t in targets.keys():
				results[o][t] = calcCoverageMatch(concatParents(o,targets),concatParents(t,targets))
			for t in sorted(targets.keys(), key=results[o].get):
				outputfile.write('\t'+t+' : '+str(results[o][t])+'\n')
			outputfile.write('\n')
	
	outputfile.close()
	
	end = time()
	print 'runtime: ' + str(end-start)
	
	thisProcess = Process().pid
	for proc in process_iter():
		try:
			if proc.name() == 'python.exe':
				if proc.pid != thisProcess:
					proc.kill()
		except AccessDenied:
			pass
			
	print 'Done!'
