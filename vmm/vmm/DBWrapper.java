package vmm;

import java.net.UnknownHostException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

public class DBWrapper {

	private final DB db;
	
	public DBWrapper(String binary) {
		MongoClient mongoClient = null;
		try {
			mongoClient = new MongoClient();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		this.db = mongoClient.getDB("OmerK_Types_"+binary.replaceAll("[ \\.]", "_"));
	}
	
	public DBCursor findInCollection(String collection) {
		return findInCollection(collection, Collections.<String, Object> emptyMap());
	}
	
	public DBCursor findInCollection(String collection, Map<String,Object> queryMap) {
		DBCollection coll = db.getCollection(collection);
		BasicDBObject query = new BasicDBObject();
		for (String k: queryMap.keySet()) {
			query = query.append(k,queryMap.get(k));
		}
		return coll.find(query);
	}

	public interface applyToResults {
		public void apply(DBObject obj);
	}
	
	public void findInCollectionAndApply(String collection, Map<String,Object> queryMap, applyToResults callback) {
		DBCollection coll = db.getCollection(collection);
		BasicDBObject query = new BasicDBObject();
		for (String k: queryMap.keySet()) {
			query = query.append(k,queryMap.get(k));
		}
		DBCursor cursor = coll.find(query);
		try {
		   while(cursor.hasNext()) {
			   callback.apply(cursor.next());
		   }
		} finally {
		   cursor.close();
		}		
	}
	
	public Set<String> distinctInCollection(String collection, String key) {
		return distinctInCollection(collection, key, Collections.<String, Object> emptyMap());
	}
	
	public Set<String> distinctInCollection(String collection, String key, Map<String,Object> queryMap) {
		DBCollection coll = db.getCollection(collection);
		BasicDBObject query = new BasicDBObject();
		for (String k: queryMap.keySet()) {
			query = query.append(k,queryMap.get(k));
		}
		return new TreeSet<String>(coll.distinct(key,query));
	}
	
	public Set<Integer> distinctIntegersInCollection(String collection, String key, Map<String,Object> queryMap) {
		DBCollection coll = db.getCollection(collection);
		BasicDBObject query = new BasicDBObject();
		for (String k: queryMap.keySet()) {
			query = query.append(k,queryMap.get(k));
		}
		return new TreeSet<Integer>(coll.distinct(key,query));
	}
	
	public long countInCollection(String collection) {
		return countInCollection(collection, Collections.<String, Object> emptyMap());
	}
	
	public long countInCollection(String collection, Map<String,Object> queryMap) {
		DBCollection coll = db.getCollection(collection);
		BasicDBObject query = new BasicDBObject();
		for (String k: queryMap.keySet()) {
			query = query.append(k,queryMap.get(k));
		}
		return coll.count(query);
	}	
}
