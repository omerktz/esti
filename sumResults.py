import sys
fIn = open(sys.argv[1],'r')
fOut = open(sys.argv[2],'w')

fIn.readline()
fIn.readline()
line = fIn.readline()
object = None
type = None
lastScore = ''
position = -1
total = 0
found0 = False
otherMatched = False
lastLineEmpty = False
countUpTo = True
upTo = 0
finalScore = False
while line != '':
	#print line
	if line.strip() == '':
		if lastLineEmpty:
			break
		fOut.write(str(object)+'\t'+str(type)+'\t'+str(position)+'\t'+str(total)+'\t'+str(otherMatched)+'\t'+str(upTo)+'\n')
		object = None
		type = None
		lastScore = ''
		position = -1
		total = 0
		found0 = False
		otherMatched = False
		lastLineEmpty = True
		upTo = 0
		countUpTo = True
		finalScore = False
	else:
		lastLineEmpty = False
		if line.startswith('\t'):
			tmp = line[1:]
			tmp = tmp[:tmp.find('\t')].strip()
			(name,score) = tmp.split(' : ')
			if name != 'OmerK-EMPTY':
				if score == '0.0':
					found0 = True
				if score != lastScore:
					if finalScore:
						countUpTo = False
					lastScore = score
					total += 1
				if name == type:
					position = total
					finalScore = True
					if found0:
						if score != '0.0':
							otherMatched = True
				if countUpTo:
					upTo += 1
		else:
			object = line[:line.find(' ')]
			tmp = line[len(object)+1:]
			type = tmp[:tmp.find('\t')][1:-1]
	line = fIn.readline()

fIn.close()
fOut.close()