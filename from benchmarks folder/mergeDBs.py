from pymongo import MongoClient
db = MongoClient()
newDb = 'mergedAnalyzer'
target = 'Analyzer.exe'
refs = ['Dualizer.exe','commandlineDecimater.exe','mconvert.exe','Smoothing.exe','mkbalancedpm.exe']

def loadTargetDict():
	strippedNames = {}
	unstrippedNames = {}

	fStrippedNames = open('/home/omerkatz/vtsPomela/benchmarks/matchResultsStripped/'+target+'.addresses','r')
	line = fStrippedNames.readline()
	while line != '':
		s = line.split('\t')
		strippedNames[s[1].strip()] = s[0].strip()
		line = fStrippedNames.readline()
	fStrippedNames.close()

	fUnstrippedNames = open('/home/omerkatz/vtsPomela/benchmarks/matchResultsUnstripped/'+target+'.addresses','r')
	line = fUnstrippedNames.readline()
	while line != '':
		s = line.split('\t')
		unstrippedNames[s[0].strip()] = s[1].strip()
		line = fUnstrippedNames.readline()
	fUnstrippedNames.close()

	result = {}
	for t in unstrippedNames.keys():
		if unstrippedNames[t] in strippedNames.keys():
			result[t] = (unstrippedNames[t],strippedNames[unstrippedNames[t]])
	
	return result

targetDict = loadTargetDict()

refDicts = {}
def translateType(b,(t,n)):
	global targetDict
	def loadRefDict(b):
		global refDicts
		if b in refDicts.keys():
			return refDicts[b]
		
		name = b.replace('_','.')
		print name

		strippedNames = {}
		unstrippedNames = {}

		fStrippedNames = open('/home/omerkatz/vtsPomela/benchmarks/matchResultsStripped/'+name+'.addresses','r')
		line = fStrippedNames.readline()
		i = 0
		while line != '':
			i+=1
			s = line.split('\t')
			strippedNames[s[0].strip()] = s[1].strip()
			line = fStrippedNames.readline()
		fStrippedNames.close()

		fUnstrippedNames = open('/home/omerkatz/vtsPomela/benchmarks/matchResultsUnstripped/'+name+'.addresses','r')
		line = fUnstrippedNames.readline()
		while line != '':
			s = line.split('\t')
			unstrippedNames[s[1].strip()] = s[0].strip()
			line = fUnstrippedNames.readline()
		fUnstrippedNames.close()

		result = {}
		for n in strippedNames.keys():
			if strippedNames[n] in unstrippedNames.keys():
				result[n] = unstrippedNames[strippedNames[n]]
		refDicts[b] = result
		return result
	

	unstrippedName = loadRefDict(b)[n]

	if unstrippedName not in targetDict.keys():
		return None
	
	return targetDict[unstrippedName]

for d in db.database_names():
	if d.startswith('OmerK_Types_'):
		if d == ('OmerK_Types_'+target.replace(' ','_').replace('.','_')):
			for r in db[d].mapping.find():
				r.pop('_id')
				r['binary']=newDb
				db['OmerK_Types_'+newDb].mapping.insert(r)
			for r in db[d].siblings.find():
				r.pop('_id')
				r['binary']=newDb
				db['OmerK_Types_'+newDb].siblings.insert(r)
			for r in db[d].untypedsignatures.find():
				r.pop('_id')
				r['binary']=newDb
				db['OmerK_Types_'+newDb].untypedsignatures.insert(r)
			for r in db[d].untyped.find():
				r.pop('_id')
				r['binary']=newDb
				db['OmerK_Types_'+newDb].untyped.insert(r)
			for r in db[d].typesignatures.find():
				r.pop('_id')
				r['binary']=newDb
				db['OmerK_Types_'+newDb].typesignatures.insert(r)
		if d in map(lambda x:'OmerK_Types_'+x.replace(' ','_').replace('.','_'),refs):
			for r in db[d].typesignatures.find():
				newType = translateType(d[12:],(r['type'],r['typename']))
				if newType:
					r.pop('_id')
					r['binary']=newDb
					r['type'] = newType[0]
					r['typename'] = newType[1]
					db['OmerK_Types_'+newDb].typesignatures.insert(r)

print 'Done!'
