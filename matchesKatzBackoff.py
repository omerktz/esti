from __future__ import unicode_literals

import sys
import os
from re import match
from time import time
#if os.environ.get('OMERK_PROFILING'):
from multiprocessingMock import Pool
#else:
#	from multiprocessing import Pool

class hashableList(list):
	def __hash__(self):
		return hash(str(self))

def getDB(binary = None):
	from pymongo import MongoClient
	while True:
		try:
			db = MongoClient()
			break
		except:
			from time import sleep
			sleep(10)
	if binary:
		if '.' in binary:
			binary = binary.replace('.','_')
		if ' ' in binary:
			binary = binary.replace(' ','_')
		#print binary
		db = db['OmerK_Types_'+binary]
	return db

##########################################################################################################

class ModelI(object):
	"""
	A processing interface for assigning a probability to the next word.
	"""

	def __init__(self):
		'''Create a new language model.'''
		raise NotImplementedError()

	def prob(self, word, context):
		'''Evaluate the probability of this word in this context.'''
		raise NotImplementedError()

	def logprob(self, word, context):
		'''Evaluate the (negative) log probability of this word in this context.'''
		raise NotImplementedError()

	def choose_random_word(self, context):
		'''Randomly select a word that is likely to appear in this context.'''
		raise NotImplementedError()

	def generate(self, n):
		'''Generate n words of text from the language model.'''
		raise NotImplementedError()

	def entropy(self, text):
		'''Evaluate the total entropy of a message with respect to the model.
		This is the sum of the log probability of each word in the message.'''
		raise NotImplementedError()
	
# Natural Language Toolkit: Language Models
#
# Copyright (C) 2001-2014 NLTK Project
# Authors: Steven Bird <stevenbird1@gmail.com>
#		  Daniel Blanchard <dblanchard@ets.org>
#		  Ilia Kurenkov <ilia.kurenkov@gmail.com>
# URL: <http://nltk.org/>
# For license information, see LICENSE.TXT

from itertools import chain
from math import log

from nltk.probability import (FreqDist,
	ConditionalProbDist,
	ConditionalFreqDist,
	LidstoneProbDist,
	WittenBellProbDist)
from nltk.util import ngrams

from nltk import compat


def _estimator(fdist, *estimator_args, **estimator_kwargs):
	"""
	Default estimator function using a SimpleGoodTuringProbDist.
	"""
	# can't be an instance method of NgramModel as they
	# can't be pickled either.
	return LidstoneProbDist(fdist, *estimator_args, **estimator_kwargs)

def wt_estimator(fdist, bins):
	"""
	Default estimator function using a SimpleGoodTuringProbDist.
	"""
	# can't be an instance method of NgramModel as they
	# can't be pickled either.
	return WittenBellProbDist(fdist, bins)


@compat.python_2_unicode_compatible
class NgramModel(ModelI):
	"""
	A processing interface for assigning a probability to the next word.
	"""

	def __init__(self, n, train, pad_left=True, pad_right=False,
				 estimator=None, *estimator_args, **estimator_kwargs):
		"""
		Create an ngram language model to capture patterns in n consecutive
		words of training text.  An estimator smooths the probabilities derived
		from the text and may allow generation of ngrams not seen during
		training.

			>>> from nltk.corpus import brown
			>>> from nltk.probability import LidstoneProbDist
			>>> est = lambda fdist, bins: LidstoneProbDist(fdist, 0.2)
			>>> lm = NgramModel(3, brown.words(categories='news'), estimator=est)
			>>> lm
			<NgramModel with 91603 3-grams>
			>>> lm._backoff
			<NgramModel with 62888 2-grams>
			>>> lm.entropy(['The', 'Fulton', 'County', 'Grand', 'Jury', 'said',
			... 'Friday', 'an', 'investigation', 'of', "Atlanta's", 'recent',
			... 'primary', 'election', 'produced', '``', 'no', 'evidence',
			... "''", 'that', 'any', 'irregularities', 'took', 'place', '.'])
			... # doctest: +ELLIPSIS
			0.5776...

		:param n: the order of the language model (ngram size)
		:type n: int
		:param train: the training text
		:type train: list(str) or list(list(str))
		:param pad_left: whether to pad the left of each sentence with an (n-1)-gram of empty strings
		:type pad_left: bool
		:param pad_right: whether to pad the right of each sentence with an (n-1)-gram of empty strings
		:type pad_right: bool
		:param estimator: a function for generating a probability distribution
		:type estimator: a function that takes a ConditionalFreqDist and
			returns a ConditionalProbDist
		:param estimator_args: Extra arguments for estimator.
			These arguments are usually used to specify extra
			properties for the probability distributions of individual
			conditions, such as the number of bins they contain.
			Note: For backward-compatibility, if no arguments are specified, the
			number of bins in the underlying ConditionalFreqDist are passed to
			the estimator as an argument.
		:type estimator_args: (any)
		:param estimator_kwargs: Extra keyword arguments for the estimator
		:type estimator_kwargs: (any)
		"""

		# protection from cryptic behavior for calling programs
		# that use the pre-2.0.2 interface
		assert(isinstance(pad_left, bool))
		assert(isinstance(pad_right, bool))

		# make sure n is greater than zero, otherwise print it
		assert (n > 0), n

		# For explicitness save the check whether this is a unigram model
		self.is_unigram_model = (n == 1)
		# save the ngram order number
		self._n = n
		# save left and right padding
		self._lpad = ('',) * (n - 1) if pad_left else ()
		self._rpad = ('',) * (n - 1) if pad_right else ()

		if estimator is None:
			estimator = wt_estimator

		cfd = FreqDist()

		# set read-only ngrams set (see property declaration below to reconfigure)
		self._ngrams = set()

		# If given a list of strings instead of a list of lists, create enclosing list
		if (train is not None) and len(train) > 0 and isinstance(train[0], compat.string_types):
			train = [train]

		for sent in train:
			raw_ngrams = ngrams(sent, n, pad_left, pad_right, pad_symbol='')
			for ngram in raw_ngrams:
				self._ngrams.add(ngram)
				context = tuple(ngram[:-1])
				token = ngram[-1]
				cfd[(context, token)] += 1

		self._probdist = estimator(cfd, cfd.B()+1)

		# recursively construct the lower-order models
		if not self.is_unigram_model:
			self._backoff = NgramModel(n-1, train,
										pad_left, pad_right,
										estimator,
										*estimator_args,
										**estimator_kwargs)

			self._backoff_alphas = dict()
			# For each condition (or context)
			for ctxt in set(map(lambda (c,w): c,cfd.iterkeys())):
				backoff_ctxt = ctxt[1:]
				backoff_total_pr = 0.0
				total_observed_pr = 0.0

				# this is the subset of words that we OBSERVED following
				# this context.
				# i.e. Count(word | context) > 0
				for word in self._words_following(ctxt, cfd):
					total_observed_pr += self.prob(word, ctxt)
					# we also need the total (n-1)-gram probability of
					# words observed in this n-gram context
					backoff_total_pr += self._backoff.prob(word, backoff_ctxt)

				assert (0 <= total_observed_pr <= 1), total_observed_pr
				# beta is the remaining probability weight after we factor out
				# the probability of observed words.
				# As a sanity check, both total_observed_pr and backoff_total_pr
				# must be GE 0, since probabilities are never negative
				beta = 1.0 - total_observed_pr

				# backoff total has to be less than one, otherwise we get
				# an error when we try subtracting it from 1 in the denominator
				assert (0 <= backoff_total_pr < 1), backoff_total_pr
				alpha_ctxt = beta / (1.0 - backoff_total_pr)

				self._backoff_alphas[ctxt] = alpha_ctxt

	def _words_following(self, context, cond_freq_dist):
		for ctxt, word in cond_freq_dist.iterkeys():
			if ctxt == context:
				yield word

	def prob(self, word, context):
		"""
		Evaluate the probability of this word in this context using Katz Backoff.

		:param word: the word to get the probability of
		:type word: str
		:param context: the context the word is in
		:type context: list(str)
		"""
		context = tuple(context)
		if (context + (word,) in self._ngrams) or (self.is_unigram_model):
			return self._probdist.prob((context, word))
		else:
			return self._alpha(context) * self._backoff.prob(word, context[1:])

	def _alpha(self, context):
		"""Get the backoff alpha value for the given context
		"""
		error_message = "Alphas and backoff are not defined for unigram models"
		assert not self.is_unigram_model, error_message

		if context in self._backoff_alphas:
			return self._backoff_alphas[context]
		else:
			return 1

	def logprob(self, word, context):
		"""
		Evaluate the (negative) log probability of this word in this context.

		:param word: the word to get the probability of
		:type word: str
		:param context: the context the word is in
		:type context: list(str)
		"""
		return -log(self.prob(word, context), 2)

	@property
	def ngrams(self):
		return self._ngrams

	@property
	def backoff(self):
		return self._backoff

	@property
	def probdist(self):
		return self._probdist

	def choose_random_word(self, context):
		'''
		Randomly select a word that is likely to appear in this context.

		:param context: the context the word is in
		:type context: list(str)
		'''

		return self.generate(1, context)[-1]

	# NB, this will always start with same word if the model
	# was trained on a single text

	def generate(self, num_words, context=()):
		'''
		Generate random text based on the language model.

		:param num_words: number of words to generate
		:type num_words: int
		:param context: initial words in generated string
		:type context: list(str)
		'''

		text = list(context)
		for i in range(num_words):
			text.append(self._generate_one(text))
		return text

	def _generate_one(self, context):
		context = (self._lpad + tuple(context))[- self._n + 1:]
		if context in self:
			return self[context].generate()
		elif self._n > 1:
			return self._backoff._generate_one(context[1:])
		else:
			return '.'

	def entropy(self, text):
		"""
		Calculate the approximate cross-entropy of the n-gram model for a
		given evaluation text.
		This is the average log probability of each word in the text.

		:param text: words to use for evaluation
		:type text: list(str)
		"""

		e = 0.0
		text = list(self._lpad) + text + list(self._rpad)
		for i in range(self._n - 1, len(text)):
			context = tuple(text[i - self._n + 1:i])
			token = text[i]
			e += self.logprob(token, context)
		return e / float(len(text) - (self._n - 1))

	def perplexity(self, text):
		"""
		Calculates the perplexity of the given text.
		This is simply 2 ** cross-entropy for the text.

		:param text: words to calculate perplexity of
		:type text: list(str)
		"""

		return pow(2.0, self.entropy(text))

	def __contains__(self, item):
		return tuple(item) in self._probdist.freqdist

	def __getitem__(self, item):
		return self._probdist[tuple(item)]

	def __repr__(self):
		return '<NgramModel with %d %d-grams>' % (len(self._ngrams), self._n)


def teardown_module(module=None):
	from nltk.corpus import brown
	brown._unload()

##############################################################################################################################

def concatParents((b,t,targets)):
	db = getDB(b).hierarchy
	types = set()
	nextTypes = set([t])
	result = set()
	while len(nextTypes) != 0:
		typeVals = nextTypes.pop()
		if typeVals not in types:
			types.add(typeVals)
			nextTypes = nextTypes.union(set(db.find({"binary":b,"type":typeVals}).distinct('parent')))
			result = result.union(targets[typeVals])
	return (t,result)

def concatSiblings(b,c,candidates):
	db = getDB(b).siblings
	result = set()
	function = c[:c.rfind('.')]
	siblings = set([c])
	checked = set()
	while len(siblings) != 0:
		x = siblings.pop()
		checked.add(x)
		if x in candidates.keys():
			result = result.union(set(candidates[x]))
		for r in db.find({"binary":b,"function":function,"object":x[x.rfind('.')+1:]}).distinct('sibling'):
			r = function+'.'+r
			if r not in checked:
				siblings.add(r)
	return result

def loadTypeSignature((b,t)):
	db = getDB(b).typesignatures
	return (t,convertTracelets(db.find({'binary':b,'typename':t}).distinct('code')))

def loadObjectSignature((b,f,o)):
	db = getDB(b).untypedsignatures
	def getMapping():
		mapping = {}
		for r in getDB(b).mapping.find({'binary':b,'function':f,'object':o}):
			mapping[r['type']] = r['base']
		return mapping
	return (f+'.'+o,convertTracelets(db.find({'binary':b,'function':f,'object':o}).distinct('code')),db.find({'binary':b,'function':f,'object':o}).distinct('realtype')[0],getMapping())

def loadFunctionSignatures(b,f,pool):
	db = getDB(b).untypedsignatures
	return pool.map_async(loadObjectSignature,map(lambda o:(b,f,o),db.find({'binary':b,'function':f}).distinct('object')))

def trainModel((t,codes)):
	return (t,NgramModel(7, list(codes[t]), False, False))

letters = {}
def getNumForLetter(l):
	global letters
	if l not in letters.keys():
		letters[l] = len(letters.keys())
	return letters[l]

def convertTracelet(tr):
	return hashableList(map(getNumForLetter,filter(lambda x:len(x)>0,tr.split(';'))))

def convertTracelets(trs):
	return map(convertTracelet,trs)

def calcObjectTypeMatch((binary,oTraces,t,model)):
	res = 1
	for tr in oTraces:
		for i in range(len(tr)):
			res *= model.prob(tr[i], tr[:i])
	return (t,res)
		
def calcObjectMatches(binary,o,candidates,mapping,models,pool,addSiblings):
	codes = candidates[o]
	if addSiblings:
		codes = concatSiblings(binary,o,candidates)
	return pool.map_async(calcObjectTypeMatch,map(lambda t:(binary,codes,t,models[t] if t in models.keys() else trainModel((t, dict([(t,[])])))[1]),mapping.keys()))	

def matches_main():
	start = time()
	
	
	candidates = {}
	targets = {}
	types = {}
	mapping = {}

	if len(sys.argv) < 5:
		print 'Usage: python matches.py <binaryName> <1=types, 2=objects> <# of processes> <output file>'
		sys.exit()
	binary = sys.argv[1]
	end = ''
	i = 0
	if binary.startswith('\''):
		end = '\''
	if binary.startswith('\"'):
		end = '\"'
	if end != '':
		binary = binary [1:]
		while not binary.endswith(end):
			i += 1
			if len(sys.argv) < (5+i):
				print 'Usage: python matches.py <binaryName> <1=types, 2=objects> <# of processes> <output file>'
				sys.exit()
			binary += ' '+sys.argv[1+i]
		binary = binary[:-1]
	tORo = int(sys.argv[2+i])
	procs = int(sys.argv[3+i])
	outputfilename = sys.argv[4+i]
	end = ''
	if outputfilename.startswith('\''):
		end = '\''
	if outputfilename.startswith('\"'):
		end = '\"'
	if end != '':
		outputfilename = outputfilename [1:]
		while not outputfilename.endswith(end):
			i += 1
			if len(sys.argv) < (5+i):
				print 'Usage: python matches.py <binaryName> <1=types, 2=objects> <# of processes> <output file>'
				sys.exit()
			outputfilename += ' '+sys.argv[4+i]
		outputfilename = outputfilename[:-1]
	if len(sys.argv) != 5+i:
		print 'Usage: python matches.py <binaryName> <1=types, 2=objects> <# of processes> <output file>'
		sys.exit()
	outputfile = open(outputfilename,'w')

	pool = Pool(processes=procs)

	db = getDB(binary)

	processesTypes = pool.map_async(loadTypeSignature,map(lambda t:(binary,t),db.typesignatures.find({"binary":binary}).distinct('typename')))
	processesObjects = map(lambda f:loadFunctionSignatures(binary,f,pool),db.untypedsignatures.find({"binary":binary}).distinct('function'))

	for r in processesTypes.get():
		targets[r[0]]=r[1]
	for rs in processesObjects:
		for r in rs.get():
			obj = r[0][r[0].rfind('.')+1:]
			if not match('^\[EBP[^\[\]]*\]$',obj):
				candidates[r[0]]=r[1]
				types[r[0]]=r[2]
				mapping[r[0]]=r[3]
	
	withParents = {}
	for r in pool.map(concatParents,map(lambda t:(binary,t,targets),targets.keys())):
		withParents[r[0]]=r[1] 
	
	models = {}		
	for r in pool.map(trainModel,map(lambda t:(t,withParents),targets.keys())):
		models[r[0]]=r[1] 
	
	outputfile.write('Number of objects: '+str(len(candidates.keys()))+'\n\n')

	results = {}

	tmp = None
	if tORo == 2:
		tmp = map(lambda o:(o,calcObjectMatches(binary,o,candidates,mapping[o],models,pool,True)),candidates.keys())
	if tORo == 1:
		mappingTmp = {}
		for t in withParents.keys():
			mappingTmp[t] = 0
		tmp = map(lambda o:(o,calcObjectMatches(binary,o,withParents,mappingTmp,models,pool,False)),withParents.keys())
	if tmp:
		for r in tmp:
			o = r[0]
			results[o]={}
			for x in r[1].get():
				results[o][x[0]]=x[1]
		for o in sorted(results.keys()):
			if tORo == 2:
				outputfile.write(o+' ('+types[o]+')\t'+' []\n')
			if tORo == 1:
				outputfile.write(o+'\n')
			for t in sorted(filter(lambda t: mapping[o][t]==0,results[o].keys()), key=lambda k:results[o][k]):
				outputfile.write('\t'+t+' : '+str(results[o][t])+'\t[]\n')
			for t in sorted(filter(lambda t: mapping[o][t]!=0,results[o].keys()), key=lambda k:results[o][k]):
				outputfile.write('\t'+t+' : '+str(results[o][t])+'\t[]\n')
			outputfile.write('\n')
	outputfile.close()
	
	end = time()
	print 'runtime: ' + str(end-start)
	
	"""
	thisProcess = Process().pid
	for proc in process_iter():
		try:
			if proc.name() == 'python.exe':
				if proc.pid != thisProcess:
					proc.kill()
		except psutil.AccessDenied:
			pass
	"""
	
	pool.close()
	pool.join()
	
	print 'Done!'
	
if __name__ == '__main__':
	matches_main()