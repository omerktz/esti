import numpy
from re import match
import sys
import os
from pymongo import MongoClient

def statFile(fileInput):
	fIn = open(folder+'/matchResults/'+fileInput,'r')
	
	db = MongoClient()[fileInput[:-8].replace(' ','_').replace('.','_')].typed
	
	def filterObjects(o,b):
		b = b[:-8]
		if b in ignores.keys():
			if o in ignores[b]:
				return False
		o = o[o.rfind('.')+1:]
		if o.startswith('[EBP'):
			return False
		def is_object(o):
			if '[' not in o and ']' not in o:
				return True
			matched = match('^\[(.+)\]([\+\-]0[0-9a-fA-F]+h)?$',o)
			if matched:
				return is_object(matched.groups()[0])
			else:
				return False
		return is_object(o)

	fIn.readline()
	fIn.readline()
	line = fIn.readline()
	object = None
	type = None
	lastScore = ''
	position = -1
	total = 0
	#found0 = False
	#otherMatched = False
	lastLineEmpty = False
	countUpTo = True
	upTo = 0
	finalScore = False
	#scores = []
	objects = {}
	#fOut.write('name\ttype\tscore_position\ttotal_scores\tfound_0\ttypes_up_to\tvariance\tmean\tvmr\n')
	types = 0
	exact = 0
	unmatched = 0
	totalObjects = 0
	matchedObjects = 0
	while line: # != '':
		#print line
		if line.strip() == '':
			if lastLineEmpty:
				break
			if filterObjects(object,fileInput):
				totalObjects += 1
				if type != 'Unknown':
					if db.find({'function':object[:object.rfind('.')],'object':object[object.rfind('.')+1:]}).count() > 0:
						objects[object] = (1,1,1)
						matchedObjects += 1
					else:
						#fOut.write(str(object)+'\t'+str(type)+'\t'+str(position)+'\t'+str(total)+'\t'+str(otherMatched)+'\t'+str(upTo)+'\t'+str(numpy.var(scores))+'\t'+str(numpy.mean(scores))+'\t'+str(0 if numpy.mean(scores)==0 else (numpy.var(scores)/numpy.mean(scores)))+'\n')
						if position == -1:
							unmatched += 1
						else:
							if types == 1:
								exact += 1
							objects[object] = (position,upTo,types)
							matchedObjects += 1
			object = None
			type = None
			lastScore = ''
			position = -1
			total = 0
			#found0 = False
			#otherMatched = False
			lastLineEmpty = True
			upTo = 0
			countUpTo = True
			finalScore = False
			#scores = []
			types = 0
			rank = 0
		else:
			lastLineEmpty = False
			if line.startswith('\t'):
				tmp = line[1:]
				tmp = tmp[:tmp.find('\t')].strip()
				name = tmp[:tmp.rfind(':')-1]
				score = tmp[tmp.rfind(':')+2:]
				if name != 'OmerK-EMPTY':
					types += 1
					#if score == '0.0':
					#	found0 = True
					if score != lastScore:
						if finalScore:
							countUpTo = False
						lastScore = score
						total += 1
					if name == type:
						position = total
						finalScore = True
						#if found0:
						#	if score != '0.0':
						#		otherMatched = True
					if countUpTo:
						upTo += 1
					#scores.append(float(score))
			else:
				object = line[:line.find(' ')]
				tmp = line[len(object)+1:]
				type = tmp[:tmp.find('\t')][1:-1]
		line = fIn.readline()

	#lengths = map(lambda o:len(objects[o]),objects.keys())
	positions = map(lambda o:objects[o][0],objects.keys())
	uptos = map(lambda o:objects[o][1],objects.keys())
	uptosRatio = map(lambda o:100*objects[o][1]/float(objects[o][2]),objects.keys())
	
	# calculate data for figure
	uptosHistogram = {}
	for i in xrange(int(sizes[binary])+1):
		uptosHistogram[i] = 0
	for i in uptos:
		uptosHistogram[i] += 1
	count = 0
	for i in sorted(uptosHistogram.keys()):
		count += uptosHistogram[i]
		uptosHistogram[i] = count
	for i in uptosHistogram.keys():
		if matchedObjects > 0:
				uptosHistogram[i] = 100*uptosHistogram[i]/matchedObjects
		else:
				uptosHistogram[i] = 100	
	fIn.close()
	return (uptosHistogram,str(matchedObjects)+'/'+str(totalObjects))

def createSubPlot(p,h,b,t,i):
	x = sorted(h.keys())
	y = map(lambda i:h[i],x)
	plt.tick_params(axis='both', which='major', labelsize=20)
	plt.tick_params(axis='both', which='minor', labelsize=10)
	p.set_yticks(numpy.arange(0,110,10))
	xticks = numpy.arange(0,max(h.keys())+1,1)
	p.set_xticks(xticks,map(lambda x: str(x) if x % 5 == 0 else '', xticks))
	p.fill_between(x, 0, y, facecolor='blue', alpha=0.3)
	p.plot(x, y, color='blue', linewidth=3.0, label=b)
	p.grid(True)
	p.set_ylabel('% of Objects covered', fontsize=30)
	p.set_xlabel('Number of Analyzed Types', fontsize=30)
	p.set_title(b+' ('+t+')', fontsize=30)



folder = sys.argv[1]
if not folder.endswith('/'):
	folder += '/'
histograms = {}
for b in os.listdir(folder):
	if b.endswith('.funcs.ranks'):
		if len([l for l in open(folder+b,'r')]) == 3:
			binary = b[:-12]
			print binary
			sizes = {}
			fSizes = open(folder+binary+'.sizes','r')
			line = fSizes.readline()
			while line != '':
				s = line.strip().split('\t')
				sizes[s[0]] = s[1]
				line = fSizes.readline()
			fSizes.close()
			ignores = {}
			fIgnores = open(folder+binary+'.ignores','r')
			line = fIgnores.readline()
			while line != '':
				s = line.strip().split('\t')
				if s[0] not in ignores.keys():
					ignores[s[0]] = set()
				ignores[s[0]].add(s[1])
				line = fIgnores.readline()
			fIgnores.close()
	
			histograms[binary] = statFile(binary+'.matches')

print len(histograms.keys())

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
numRows = 5
numCols = 7
width = 20
height = 10
fig,ax = plt.subplots(numRows,numCols,figsize=(numCols*width,numRows*height))
i=0
j=0
for b in histograms.keys():
	(h,t) = histograms[b]
	createSubPlot(ax[i][j],h,b,t,i)
	j+=1	
	if j == numCols:
		j=0
		i+=1
plt.savefig(folder+sys.argv[2],format='png',transparent=True)

