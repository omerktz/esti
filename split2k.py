from imports import *

#starters = {}
#enders = {}

# this is a generator for graphlets, in tracemode (only get "sequences", and not "trees")
def ksplitGraphObject(k,g,func,iterateAllNodes=True):
	global starters
	global enders
	def subTraces(g,Vin,k):#,first,last=""):
		#print k
		#global starters
		#global enders
		#hasKids = False
		if k < 1:
			yield []
			raise StopIteration
		v = g.vs[Vin]
		yield [v]
		#traces = [[v]]
		_iter = g.bfsiter(Vin,g.BFSITER_OUT,True)
		keepGoing = True
		#if first:
		#	#if g.vs[Vin]['code'] != "":
		#		#starters[func].add(v)
		#		#last = v
		#		#first = False
		try:
			while keepGoing:
				(vr,d,_) = _iter.next()
				#print str(Vin)+' '+str(vr.index)+' '+str(d)
				if d >= 2:
					keepGoing = False
				if d == 1:
					#hasKids = True
					newK = k
					#newLast = last
					#newFirst = first
					if g.vs[Vin]['code'] != "":
						newK -= 1
						#last = v
						#newFirst = False
					subtraces = subTraces(g,vr.index,newK)#,newFirst,newLast)
					for st in subtraces:
						t = [v] + st
						yield t
						#traces.append(t)
						#print t
		except StopIteration:
			pass
		#if not hasKids:
			#if last != "":
				#enders[func].add(last)
		#return traces
		raise StopIteration
	#traces = set()
	if k < 1:
		raise StopIteration
	else:
		#combos=set()
		#starters[func] = set()
		#enders[func] = set()
		for VertexIndex in xrange(0,len(list(g.vs)) if iterateAllNodes else 1):
			#print VertexIndex
			seqMaker = subTraces(g,VertexIndex,k)#,VertexIndex==0)			
			for seq in seqMaker:
				#print map(lambda v:hex(v.start_addr)[:-1],seq)
				code =""
				trace =""
				empty = True
				for v in seq:
					if v['code'] != "":
						empty = False
						code += v['code'] + ";"
						trace += hex(v.start_addr)+','#[:-1]+","
				if empty:
					trace = ""
					code = ""
				newT = hashableDict({'code':code,'trace':trace})
				yield newT
				#if not newT in combos:
				#	combos.add(newT)
				#	traces.add(newT)
	raise StopIteration
					
if __name__ == '__main__':
	print "Done!"
