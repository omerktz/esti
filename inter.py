from imports import *

def getRelevantCallsForObject(da,o):
	def getTarget(c):
		return c['target'][c['target'].find(':')+1:]
	calls = da.getCalls()
	rCalls = {}
	for l in calls.keys():
		for c in calls[l]:
			if getTarget(c) == o:
				if l not in rCalls.keys():
					rCalls[l] = []
				rCalls[l].append(c)
	return rCalls

def getRelevantFieldsForObject(da,o):
	if o in da.getFields().keys():
		return da.getFields()[o]
	else:
		return set()

def getRelevantObjectExpressions(states,o,isCall = False):
	from StateUtils import simplifyExpression
	def getRegInitialValue(r):
		if r in ['eax', 'ecx', 'edx']:
			return r.upper()+'0'
		else:
			if r == 'esp':
				if isCall:
					return r.upper()+'+4'
				else:
					return r.upper()
			else:
				return r.upper()
	newOs = set()
	memory = False
	for state in states:
		for r in state.registers.keys():
			if state.registers[r] == o:
				newOs.add(getRegInitialValue(r))
		if o.startswith('[ESP'):
			newOs.add('['+simplifyExpression(getRegInitialValue('esp')+'+'+o[1:-1]+'-('+state.registers['esp']+')')+']')
		for s in state.stack.keys():
			if state.stack[s] == o:
				newOs.add('['+simplifyExpression(getRegInitialValue('esp')+'+'+s+'-('+state.registers['esp']+')')+']')
		for m in state.memory.keys():
			if state.memory[m] == o:
				memory = True
	if memory:
		print 'WARNING:: InterProcedural - object in memory unhandled!'
	return newOs

cachedResults = {}
def interproceduralGetCallsAndFieldsForObject(f,o,depth):
	global cachedResults
	def uniteResults(r1,r2):
		c1 = r1[0]
		f1 = r1[1]
		c2 = r2[0]
		f2 = r2[1]
		f = set(f1).union(set(f2))
		c = {}
		for l in c1.keys():
			c[l] = c1[l]
		for l in c2.keys():
			if l not in c.keys():
				c[l] = c2[l]
		return (c,f)
	startF = get_func(f).startEA
	if startF not in cachedResults.keys():
		cachedResults[startF] = {}
	if o in cachedResults[startF].keys():
		return cachedResults[startF][o]
	if GetMnem(startF) == 'jmp':
		loc = LocByName(GetOpnd(startF,0))
		if loc != 0xffffffffffffffff:
			result = interproceduralGetCallsAndFieldsForObject(loc,o,depth)
			cachedResults[startF][o] = result
			return result
	#print hex(startF)[:-1] + ':'
	from analysisUtils import getDataAnalyzer
	da = getDataAnalyzer(startF)
	#print '\tda done'
	calls = da.getNonVirtualCallTargets()
	#objects = da.getAllObjects()
	#print objects
	states = da.getStatesPerLine()
	rFields = getRelevantFieldsForObject(da,o)
	rCalls = getRelevantCallsForObject(da,o)
	result = (rCalls,rFields)
	from StateUtils import getPreviousState
	for c in calls.keys():
		address = LocByName(calls[c])
		for newO in getRelevantObjectExpressions(getPreviousState(states,c),o,GetMnem(long(c)) == 'call'):
			r = interproceduralGetCallsAndFieldsForObject(address,newO,depth-1)
			result = uniteResults(result,r)
	cachedResults[startF][o] = result
	return result

def deduceClass(data):
	from vtables import VirtualTableFinder
	vts = VirtualTableFinder().find_virtual_tables()
	from FindClassSize import TypeRuler
	TypeRuler().findClassSizes(vts.values())
	calls = data[0].values()
	fields = data[1]
	offsets = []
	args = []
	for call in calls:
		offsets.extend(map(lambda c:c['offset']/4,call))
		args.extend(map(lambda c:c['args'],call))
	maxFunc = max(offsets)
	maxField = max(map(lambda x:x[1],fields))+4
	from analysisUtils import getAllAllocatedVirtualTables
	#print getAllAllocatedVirtualTables()
	options = filter(lambda v: maxFunc <= len(vts[v].functions),getAllAllocatedVirtualTables())
	#print options
	stillAnOption = set()
	for vt in options:
		for o in xrange(len(offsets)):
			if args[o] >= vts[vt].functions[o].num_of_args:
				stillAnOption.add(vt)
	#print stillAnOption
	print maxField
	return filter(lambda v: maxField <= vts[v].alloc,stillAnOption)
	
if __name__ == '__main__':
	print map(lambda x:hex(x)[:-1],deduceClass(interproceduralGetCallsAndFieldsForObject(ScreenEA(),'EAX4',3)))
	print 'Done!'