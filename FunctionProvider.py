from imports import *

from IdaDB import *

FUNCTIONS_DB = FunctionsDB()

class Function:
	def __init__(self, ea, edgeTraversalLimit = 1):
		# get the function's ea
		ea = get_func(ea).startEA
		self.ea = ea
		self.function = FUNCTIONS_DB[ea]				
		self.graph = self.function.getGraph()
		#self.graph.calculateBackedges()
		self.edgeTraversalLimit = 2
		for edge in self.graph.edges:
			edge.data = 0

	def getBlocks(self):
		return self.graph.basic_blocks.values()
	
	def getInitialBlock(self):		
		return self.graph.basic_blocks[self.graph.startEA]
		
	def getInitialBlocks(self):		
		return filter(lambda b:len(self.getBlockInEdges(b))==0,self.graph.basic_blocks.values())
		
	#return a final block (if more than one exists, choose one)
	def getFinalBlock(self):
		b = self.getInitialBlock()
		s = self.getBlockSuccessors(b)
		while len(s) > 0:
			b = s[0]
			s = self.getBlockSuccessors(b)
		return b
		
	def getBlockCode(self,b):
		return b.getCode()
		
	def getBlockCommands(self,b):
		return self.getBlockCode(b).split(';')

	def getBlockSuccessors(self,b, limitlessRepeats = False):
		return map(lambda x: x.target(), filter(lambda y: (y.traverse()<=self.edgeTraversalLimit) or limitlessRepeats,self.getBlockOutEdges(b)))	

	def getBlockOutEdges(self, b):
		return self.graph.bb_out_edges.get(b.start_addr,[])

	def getBlockInEdges(self, b):
		return self.graph.bb_in_edges.get(b.start_addr,[])

	def addEdgePassCount(self, edge):
		edge.data += 1
		# edge = self.graph.getEdge(source, target)
		# edge.data += 1

	def isBackedge(self, edge):
		return edge.is_backedge

	def getEdgePassCount(self, edge):
		return edge.data
		# return self.graph.getEdge(source, target).data

	def getBlockId(self,b):
		return BlockId(b)
	
	def getLineId(self,b,l):
		return LineId(b,l)

class BlockId:
	def __init__(self,b):
		self.block = b
		self.id = int(b.start_addr)
		
	def __cmp__(self,other):
		return self.id.__cmp__(other.id)
		
	def __str__(self):
		return str(self.id)
		
	def __hash__(self):
		return hash(self.id)
		
class LineId:
	def __init__(self,b,l):
		self.b = BlockId(b)
		self.id = l
				
	def __cmp__(self,other):
		if self.b.__cmp__(other.b) == 0:
			return self.id.__cmp__(other.id)
		else:
			return self.b.__cmp__(other.b)
			
	def __str__(self):
		return hex(self)#[:-1]
		
	def __hash__(self):
		return hash(long(self))
		
	def __int__(self):
		return int(long(self))

	def __long__(self):
		return self.b.id+self.id
		
	def __hex__(self):
		return hex(long(self))
