import numpy
from re import match
import sys
import os
from pymongo import MongoClient

def statFile(fileInput):
	fIn = open('./matchResults/'+fileInput,'r')
	
	db = MongoClient()[fileInput[:-8].replace(' ','_').replace('.','_')].typed
	
	def filterObjects(o,b):
		b = b[:-8]
		if b in ignores.keys():
			if o in ignores[b]:
				return False
		o = o[o.rfind('.')+1:]
		if o.startswith('[EBP'):
			return False
		def is_object(o):
			if '[' not in o and ']' not in o:
				return True
			matched = match('^\[(.+)\]([\+\-]0[0-9a-fA-F]+h)?$',o)
			if matched:
				return is_object(matched.groups()[0])
			else:
				return False
		return is_object(o)

	fIn.readline()
	fIn.readline()
	line = fIn.readline()
	object = None
	type = None
	lastScore = ''
	position = -1
	total = 0
	#found0 = False
	#otherMatched = False
	lastLineEmpty = False
	countUpTo = True
	upTo = 0
	finalScore = False
	#scores = []
	objects = {}
	#fOut.write('name\ttype\tscore_position\ttotal_scores\tfound_0\ttypes_up_to\tvariance\tmean\tvmr\n')
	types = 0
	exact = 0
	unmatched = 0
	totalObjects = 0
	matchedObjects = 0
	while line: # != '':
		#print line
		if line.strip() == '':
			if lastLineEmpty:
				break
			if filterObjects(object,fileInput):
				totalObjects += 1
				if type != 'Unknown':
					#fOut.write(str(object)+'\t'+str(type)+'\t'+str(position)+'\t'+str(total)+'\t'+str(otherMatched)+'\t'+str(upTo)+'\t'+str(numpy.var(scores))+'\t'+str(numpy.mean(scores))+'\t'+str(0 if numpy.mean(scores)==0 else (numpy.var(scores)/numpy.mean(scores)))+'\n')
					if position == -1:
						unmatched += 1
					else:
						if types == 1:
							exact += 1
						objects[object] = (position,upTo,types)
						matchedObjects += 1
			object = None
			type = None
			lastScore = ''
			position = -1
			total = 0
			#found0 = False
			#otherMatched = False
			lastLineEmpty = True
			upTo = 0
			countUpTo = True
			finalScore = False
			#scores = []
			types = 0
			rank = 0
		else:
			lastLineEmpty = False
			if line.startswith('\t'):
				tmp = line[1:]
				tmp = tmp[:tmp.find('\t')].strip()
				name = tmp[:tmp.rfind(':')-1]
				score = tmp[tmp.rfind(':')+2:]
				if name != 'OmerK-EMPTY':
					types += 1
					#if score == '0.0':
					#	found0 = True
					if score != lastScore:
						if finalScore:
							countUpTo = False
						lastScore = score
						total += 1
					if name == type:
						position = total
						finalScore = True
						#if found0:
						#	if score != '0.0':
						#		otherMatched = True
					if countUpTo:
						upTo += 1
					#scores.append(float(score))
			else:
				object = line[:line.find(' ')]
				tmp = line[len(object)+1:]
				type = tmp[:tmp.find('\t')][1:-1]
		line = fIn.readline()

	#lengths = map(lambda o:len(objects[o]),objects.keys())
	positions = map(lambda o:objects[o][0],objects.keys())
	uptos = map(lambda o:objects[o][1],objects.keys())
	uptosRatio = map(lambda o:100*objects[o][1]/float(objects[o][2]),objects.keys())
	
	# calculate data for figure
	uptosHistogram = {}
	for i in xrange(int(sizes[sys.argv[1]])+1):
		uptosHistogram[i] = 0
	for i in uptos:
		uptosHistogram[i] += 1
	count = 0
	for i in sorted(uptosHistogram.keys()):
		count += uptosHistogram[i]
		uptosHistogram[i] = count
	for i in uptosHistogram.keys():
		if matchedObjects > 0:
				uptosHistogram[i] = 100*uptosHistogram[i]/matchedObjects
		else:
				uptosHistogram[i] = 100
	
	area = 0
	for x in uptosHistogram.keys():
		area+= uptosHistogram[x]
	area = area/len(uptosHistogram.keys())
	
	#create figure
	x = sorted(uptosHistogram.keys())
	y = map(lambda i:uptosHistogram[i],x)
	import matplotlib
	matplotlib.use('Agg')
	import matplotlib.pyplot as plt
	plt.figure(figsize=(30,15))
	plt.tick_params(axis='both', which='major', labelsize=20)
	plt.tick_params(axis='both', which='minor', labelsize=10)
	plt.yticks(numpy.arange(0,110,10))
	xticks = numpy.arange(0,max(uptosHistogram.keys())+1,1)
	plt.xticks(xticks,map(lambda x: str(x) if x % 5 == 0 else '', xticks))
	plt.fill_between(x, 0, y, facecolor='blue', alpha=0.3)
	plt.plot(x, y, color='blue', linewidth=3.0)
	plt.grid(True)
	plt.ylabel('% of Objects covered', fontsize=30)
	plt.xlabel('Number of Analyzed Types', fontsize=30)
	plt.title(sys.argv[1]+' ('+str(matchedObjects)+'/'+str(totalObjects)+')', fontsize=30)
	plt.savefig(sys.argv[3],format='png',transparent=True)


	#if len(lengths) > 0:
	if len(objects.keys()) > 0:
		name = fileInput[:-8]
		fOut.write(name+'\t'+str(sizes[name])+'\t'+str(totalObjects)+'\t'+str(len(objects.keys())+unmatched)+'\t'+str(unmatched)+'\t'+str(exact)+'\t'+str(area)+'\t'+str(min(positions))+'\t'+str(max(positions))+'\t'+str(numpy.mean(positions))+'\t'+str(numpy.median(positions))+'\t'+str(numpy.var(positions))+'\t'+str(min(uptos))+'\t'+str(max(uptos))+'\t'+str(numpy.mean(uptos))+'\t'+str(numpy.median(uptos))+'\t'+str(numpy.var(uptos))+'\t'+str(min(uptosRatio))+'\t'+str(max(uptosRatio))+'\t'+str(numpy.mean(uptosRatio))+'\t'+str(numpy.median(uptosRatio))+'\t'+str(numpy.var(uptosRatio))+'\n')
	
	fIn.close()

sizes = {}
fSizes = open(sys.argv[1]+'.sizes','r')
line = fSizes.readline()
while line != '':
	s = line.strip().split('\t')
	sizes[s[0]] = s[1]
	line = fSizes.readline()
fSizes.close()

ignores = {}
fIgnores = open(sys.argv[1]+'.ignores','r')
line = fIgnores.readline()
while line != '':
	s = line.strip().split('\t')
	if s[0] not in ignores.keys():
		ignores[s[0]] = set()
	ignores[s[0]].add(s[1])
	line = fIgnores.readline()
fIgnores.close()
	
fOut = open(sys.argv[2],'w')

fOut.write('file\tvtables\tobjects\tchecked-objects\tunmatched\texact\tarea\tposition\t\t\t\t\tup-to\t\t\t\t\n')	
fOut.write('\t\t\t\t\t\t\tmin\tmax\tmean\tmedian\tvariance\tmin\tmax\tmean\tmedian\tvariance\n')	

#for f in os.listdir('.'):
#	if f.endswith('.matches'):
#		print f[:-8]
statFile(sys.argv[1]+'.matches')
	
fOut.close()
