from imports import *

def printDictionary(d,f,i):
	indent = ''
	for _ in range(i):
		indent += '\t'
	for k in d.keys():
		if d[k].__class__() == {}:
			f.write(indent+str(k)+':\n')
			printDictionary(d[k],f,i+1)
		else:
			f.write(indent+str(k)+':'+str(d[k])+'\n')
	f.write(indent+'\n')

def printCollection(db,f):
	for r in db.find():
		printDictionary(r,f,1)

def buildDictionary(f):
	d = {}
	l = f.readline().strip()
	while len(l) > 0:
		val = l[l.find(':')+1:]
		if len(val) == 0:
			val = buildDictionary(f)
		d[l[:l.find(':')]] = val
		l = f.readline().strip()
	return d

def buildCollection(db,f):
	d = buildDictionary(f)
	while len(d) > 0:
		db.insert(d)
		d = buildDictionary(f)

def printDatabase(db,f):
	for n in db.collection_names():
		if n not in ['system.indexes']:
			print '\tprinting collection '+n
			f.write(n+'\n')
			printCollection(db[n],f)
			f.write('\n')
			f.flush()
		
def buildDatabase(db,f):
	l = f.readline()
	while len(l) > 0:
		n = l[:-1]
		print '\tbuilding collection '+n
		db.create_collection(n)
		buildCollection(db[n],f)
		l = f.readline()
	
if __name__ == '__main__':
	if len(sys.argv) != 4:
		print 'Usage: python dbBackup.py <Database name> <output file name> <1=print, 2=build>'
		sys.exit()
	dbname = sys.argv[1]
	filename = sys.argv[2]
	try:
		pORb = int(sys.argv[3])
	except:
		print 'Usage: python dbBackup.py <Database name> <output file name> <1=print, 2=build'
		sys.exit()
	db = getDB()
	if pORb == 1:
		if dbname not in db.database_names():
			print "ERROR: DB doesn't exist! nothing to print"
			sys.exit()
		db = db[dbname]
		print 'printing database '+dbname
		f = open(filename,'w')
		printDatabase(db,f)
		f.close()
	else:
		if pORb == 2:
			if dbname in db.database_names():
				if len(db[dbname].collection_names()) > 0:
					print "ERROR: DB exists and is not empty! cannot overwrite DB"
					sys.exit()
				else:
					print "WARNING: DB exists but was empty"					
			db = db[dbname]
			print 'building database '+dbname
			f = open(filename,'r')
			buildDatabase(db,f)
			f.close()
		else:
			print "Invalid option! Choose print or build"
			print 'Usage: python dbBackup.py <Database name> <output file name> <1=print, 2=build>'
			sys.exit()
