import sys
import os
from re import match, split, findall
from time import time
from db_utils import getDB
from idaCache import *
#try:
#    from idaapi import get_func,get_root_filename,FlowChart,get_imagebase,autoWait
#    from idautils import Functions,XrefsTo,DataRefsTo,DataRefsFrom
#    from idc import GetMnem,GetOpnd,GetFunctionName,Demangle,SegStart,SegEnd,LocByName,ItemSize,Name,OpHex,OpOff,ScreenEA,BADADDR,NextHead,PrevHead,GetFrame,GetStrucSize,GetMemberName,GetMemberSize,GetManyBytes,GetFlags,isData,GetFrameArgsSize,GetFirstMember,GetLastMember,GetStrucNextOff,GetType,GetCommentEx,MakeComm
#except ImportError:
#    pass
from sympy import simplify
from psutil import Process,process_iter,AccessDenied
if os.environ.get('OMERK_PROFILING'):
    from multiprocessingMock import Pool
else:
    from multiprocessing import Pool

class hashableDict(dict):
    def __hash__(self):
        return hash(tuple(sorted(self.items())))

