from imports import *

def printUntyped(b,function,obj):
	f = open('Untyped','w')
	f.write(function+'.'+obj+':\n\n')
	db=getDB(b)
	t=set()
	for r in db.untypedsignatures.find({'function':function,'object':obj}):
		t.add(r['code'])
	for s in sorted(t,key=lambda x:x.count(';')):
		f.write(s+'\n')
	f.close()
	
def printTyped(b,name):
	f = open('Typed-'+name,'w')
	f.write(name+':\n\n')
	db=getDB(b)
	t=set()
	for r in db.typesignatures.find({'typename':name}):
		t.add(r['code'])
	for s in sorted(t,key=lambda x:x.count(';')):
		f.write(s+'\n')
	f.close()
	
def printDistances(b,function,obj,name):
	from matches import damerau_levenshtein_distance
	f = open('Distances-'+name,'w')
	f.write(function+'.'+obj+':\n\n')
	db=getDB(b)
	t=set()
	for r in db.untypedsignatures.find({'function':function,'object':obj}):
		t.add(r['code'])
	s=set()
	for r in db.typesignatures.find({'typename':name}):
		s.add(r['code'])
	for i in sorted(t,key=lambda x:x.count(';')):
		partsI = i.split(';')[:-1]
		scores = {}
		for j in s:
			partsJ = j.split(';')[:-1]
			scores[j] = damerau_levenshtein_distance(partsI,partsJ)/float(pow(len(partsI),2))
		score = min(scores.values())
		f.write(i+' '+str(score)+'\n')
		for j in filter(lambda x:scores[x]==score,scores.keys()):
			f.write('\t'+j+'\n')
	f.close()

types = ['A','B','C','D','E','F']
func = '?ge4@@YAXPAVE@@@Z'
b = 'BigToy.exe'
obj = '[ESP+04h]'

printUntyped(b.func,obj)
for t in types:
	printTyped(b,t)
for t in types:
	printDistances(b,func,obj,t)
