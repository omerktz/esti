from imports import *

def shouldAnalyze(ea):
	if get_func(ea):
		from vtables import VirtualTableFinder
		def cleanAddress(a):
			if a.startswith('0x'):
				a=a[2:]
			if a.endswith('h'):
				a=a[:-1]
			if a.endswith('L'):
				a=a[:-1]
			return a
		vts = map(lambda x:cleanAddress(hex(x)).upper()+'h',VirtualTableFinder().find_virtual_tables().keys())
		for b in FlowChart(get_func(ea)):
			start = b.startEA
			end = b.endEA
			addr = start
			# subroutine marked as ended one line after the real end.
			while (addr<end):
				if GetMnem(addr) in ['call','jmp']:
					opnd = GetOpnd(addr,0)
					if match('^dword ptr ',opnd):
						opnd = opnd[10:]
					#print opnd
					if match('^e(([abcd]x)|([sd]i)|([sb]p))$',opnd):
						return True
					if match('^\[(e(([abcd]x)|([sd]i)|([sb]p)))\+[0-9a-fA-F]+h\]$',opnd):
						return True
				if GetMnem(addr) in ['mov']:
					OpHex(addr,1)
					opnd = GetOpnd(addr,1)
					OpOff(addr,1,0)
					if opnd in vts:
						return True					
				addr=NextHead(addr)
		return False
	return True

functionAddresses = {}
def getFunctionStartAddress(ea):
	global functionAddresses
	if ea not in functionAddresses.keys():
		f = get_func(ea)
		if f:
			functionAddresses[ea] = f.startEA
		else:
			functionAddresses[ea] = ea
	return functionAddresses[ea]

callsCache = {}
def collectCalls(ea):
	from FunctionProvider import Function
	global callsCache
	def clearOpnd(o):
		if match('^[scdefg]s\:',o):
			o = o[3:]
		if match('0x[0-9a-fA-F]+',o):
			try:
				o = long(o,16)
			except ValueError:
				o = 0xffffffffffffffff
		else:
			o = LocByName(o)
		return o
	startF = getFunctionStartAddress(ea)
	if startF not in callsCache.keys():
		calls = set()
		if startF:
			if get_func(startF):
				f = Function(startF)
				for b in f.getBlocks():
					for c in f.getBlockCommands(b):
						if c.startswith('call ') or c.startswith('jmp '):
							calls.add(getFunctionStartAddress(clearOpnd(c[c.find(' ')+1:])))
		callsCache[startF] = calls
	return callsCache[startF]

branchesCache = {}
def count_branches(ea):
	global branchesCache
	if ea not in branchesCache.keys():
		branchesCache[ea] = (len(filter(lambda c:c>1, map(lambda b: len([s for s in b.succs()]), [b for b in FlowChart(get_func(ea))]))) if get_func(ea) else 0)
	return branchesCache[ea]

depsCache = {}
def calc_deps(ea,d=0):
	global depsCache
	if ea not in depsCache.keys():
		depsCache[ea] = {}
	if d not in depsCache[ea].keys():
		deps = set()
		#print ea
		tabs = ''
		for _ in xrange(d):
			tabs+='\t'
		if get_func(ea):
			for b in FlowChart(get_func(ea)):
				start = b.startEA
				end = b.endEA
				addr = start
				# subroutine marked as ended one line after the real end.
				while (addr<end):
					if GetMnem(addr) in ['call','jmp']:
						target = LocByName(GetOpnd(addr,0))
						if target != 0xffffffffffffffff and ((not get_func(target)) or get_func(target).startEA != ea):
							deps.add(target)
							while (target != 0xffffffffffffffff) and (GetMnem(target) == 'jmp'):
								target = LocByName(GetOpnd(target,0))
								if target != 0xffffffffffffffff:
									deps.add(target)
					addr=NextHead(addr)
		extras = set()
		if d < 2:
			for x in deps:
				extras = extras.union(calc_deps(x,d+1))
		depsCache[ea][d] = deps.union(extras)
		depsCache[ea][d].add(ea)
	return depsCache[ea][d]

def get_function_args_size(ea):
	# enable the 2 following lines to remove interfaces from reduction results
	if get_func(ea):
		ea = get_func(ea).startEA
	if GetMnem(ea) == 'jmp':
		return get_function_args_size(LocByName(GetOpnd(ea,0)))
	frameSize = GetFrameArgsSize(ea)
	#print frameSize
	if frameSize != 0xffffffffffffffff:
		return GetFrameArgsSize(ea)
	frame = GetFrame(ea)
	if not frame:
		return 0
	first_member = GetFirstMember(frame)
	last_member = GetLastMember(frame)
	curr_member = first_member
	just_seen_r = False
	first_arg_offset = -1
	while curr_member <= last_member:
		if just_seen_r:
			first_arg_offset = curr_member
		just_seen_r = False
		name = GetMemberName(frame, curr_member)
		#size = GetMemberSize(frame, curr_member)
		just_seen_r = (name and name == " r")

		# print hex(curr_member), name, size, just_seen_r, GetMemberFlag(frame, curr_member)
		curr_member = GetStrucNextOff(frame, curr_member)
	args_size = (curr_member - first_arg_offset) if first_arg_offset != -1 else 0
	
	if args_size == 0:
		sizes = set()
		for x in XrefsTo(ea):
			if GetMnem(x.frm) == 'call':
				nextX = x.frm+ItemSize(x.frm)
				if GetMnem(nextX) == 'add' and GetOpnd(nextX,0) == 'esp':
					sizes.add(int(GetOpnd(nextX,1),16))
		if len(sizes) == 1:
			args_size = sizes.pop()
		
	# print last_member,GetFrameLvarSize(ea)
	# for x in xrange(firs
	# print hex(first_member), hex(last_member), GetMemberName(frame, first_member), GetMemberName(frame, last_member)
	return args_size

# get calling convention according to IDA
def get_function_calling_convention(ea):
	typeVal = ''
	if ea != 0xffffffffffffffffL:
		# if this is a jmp check target for calling convention
		if GetMnem(ea) == 'jmp':
			return get_function_calling_convention(LocByName(GetOpnd(ea,0)))
		else:
			typeVal = GetType(ea)
		if not typeVal:
			typeVal = ''
		else:
			# type is a string format function header. split it to get convention part
			if ' ' in typeVal:
				typeVal = typeVal.split(' ')[1]
			if '(' in typeVal:
				typeVal = typeVal[:typeVal.find('(')]
	return typeVal
	
if __name__ == '__main__':
	#x=set()
	#for f in Functions():
	#	x.add(count_branches(f))
	#print max(x)
	#print get_function_args_size(ScreenEA())
	#print shouldAnalyze(ScreenEA())
	#print get_function_args_size(0x412a00)
	#print calc_deps(ScreenEA())
	print 'Done!'