try:
    from idaapi import get_func,get_root_filename,FlowChart,get_imagebase,autoWait,qexit
    from idautils import Segments,Functions,XrefsTo,DataRefsTo,DataRefsFrom,XrefsFrom
    from idc import isCode,SegStart,SegEnd,GetMnem,GetOpnd,GetFunctionName,Demangle,LocByName,ItemSize,Name,OpHex,OpOff,NextHead,PrevHead,GetFrame,GetMemberName,GetMemberSize,GetManyBytes,GetFlags,isData,GetFrameArgsSize,GetFirstMember,GetLastMember,GetStrucNextOff,GetType
except ImportError:
    pass

autoWait()

debug = False
def debugPrint(s):
    if debug:
        print s

def myHex(n):
    tmp = hex(n)[2:]
    if tmp.endswith('L'):
        tmp = tmp[:-1]
    return tmp

from json import JSONEncoder

from pymongo import ASCENDING, MongoClient


encoder = JSONEncoder()

dbname = 'OmerK_IdaCache_'+get_root_filename().replace(' ','_').replace('.','_')
db = MongoClient()
db.drop_database(dbname)
db = db[dbname]

db.get_imagebase.insert({'base':myHex(get_imagebase())})
db.Functions.insert({'functions':encoder.encode(map(lambda x: myHex(x),[f for f in Functions()]))})

names = set(map(lambda f:GetFunctionName(f),Functions()))
flags = set()
frames = set()

print 'Caching Functions'
for f in Functions():
    debugPrint('\tfunction: '+hex(f))
    # for functions:
    flow = FlowChart(get_func(f))
    ####### FIX
    bbs = map(lambda b: {'id':myHex(b.id),'startEA':myHex(b.startEA), 'endEA':myHex(b.endEA),'succs':[myHex(s.startEA) for s in b.succs()],'preds':[myHex(p.startEA) for p in b.preds()]},[b for b in flow])
    db.FlowChart.insert({'ea':str(f),'flow':encoder.encode(bbs)})
    db.GetType.insert({'ea':str(f),'type':GetType(f)})
print 'Caching Commands'
def handleEA(ea):
    if ea != 0xffffffffffffffff and ea >= get_imagebase():
        debugPrint('\tea: '+hex(ea))
        # for command:
        db.NextHead.insert({'ea':str(ea),'head':myHex(NextHead(ea))})
        db.PrevHead.insert({'ea':str(ea),'head':myHex(PrevHead(ea))})
        db.ItemSize.insert({'ea':str(ea),'size':myHex(ItemSize(ea))})
        db.GetManyBytes.insert({'ea':str(ea),'bytes':encoder.encode([myHex(ord(c)) for c in GetManyBytes(ea,16)]) if GetManyBytes(ea,16) else None})
        flags.add(GetFlags(ea))
        db.DataRefsFrom.insert({'ea':str(ea),'target':encoder.encode([myHex(x) for x in DataRefsFrom(ea)])})
        db.DataRefsTo.insert({'ea':str(ea),'target':encoder.encode([myHex(x) for x in DataRefsTo(ea)])})
        db.GetFlags.insert({'ea':str(ea),'flags':myHex(GetFlags(ea))})
        db.XrefsTo.insert({'ea':str(ea),'target':encoder.encode([{'frm':myHex(x.frm)} for x in XrefsTo(ea)])})
        db.XrefsFrom.insert({'ea': str(ea), 'target': encoder.encode([{'to': myHex(x.to)} for x in XrefsFrom(ea)])})
        db.GetMnem.insert({'ea':str(ea),'mnem':GetMnem(ea)})
        db.get_func.insert({'ea':str(ea),'func':encoder.encode({'startEA':myHex(get_func(ea).startEA),'endEA':myHex(get_func(ea).endEA)}) if get_func(ea) else None})
        db.GetFrameArgsSize.insert({'ea':str(ea),'size':myHex(GetFrameArgsSize(ea))})
        db.GetFrame.insert({'ea':str(ea),'frame':myHex(GetFrame(ea)) if GetFrame(ea) else None})
        frames.add(GetFrame(ea))
        db.GetFunctionName.insert({'ea':str(ea),'name':GetFunctionName(ea)})
        names.add(GetFunctionName(ea))
        db.Name.insert({'ea':str(ea),'name':Name(ea)})
        names.add(Name(ea))
        if isCode(GetFlags(ea)):
            # code
            OpHex(ea,0)
            db.GetOpnd.insert({'ea':str(ea),'n':0,'opnd':GetOpnd(ea,0)})
            OpOff(ea,0,0)  
            OpHex(ea,1)
            db.GetOpnd.insert({'ea':str(ea),'n':1,'opnd':GetOpnd(ea,1)})
            OpOff(ea,1,0)
eas = set()
passedEas = set()
ea = get_imagebase()
while ea != 0xffffffffffffffff:
    eas.add(ea)
    ea = NextHead(ea)
for s in Segments():
    ea = SegStart(s)
    while ea < SegEnd(s):
        eas.add(ea)
        ea = NextHead(ea)
while len(eas) > 0:
    ea = eas.pop()
    passedEas.add(ea)
    if NextHead(ea) != 0xffffffffffffffff:
        if (ea+ItemSize(ea)) not in passedEas:
            eas.add(ea+ItemSize(ea))
        if NextHead(ea) not in passedEas:
            eas.add(NextHead(ea))
    handleEA(ea)
    #if NextHead(ea) != 0xffffffffffffffff:
    #    if (ea+ItemSize(ea)) not in passedEas:
    #        eas.add(ea+ItemSize(ea))
    #    if NextHead(ea) not in passedEas:
    #        eas.add(NextHead(ea))
print 'Caching Names'
for n in names:
    debugPrint('\tname: '+n)
    db.LocByName.insert({'name':n,'loc':myHex(LocByName(n))})
    db.Demangle.insert({'s':n,'d':Demangle(n,0)})
print 'Caching Flags'
for f in flags:
    debugPrint('\tflag: '+hex(f))
    db.isData.insert({'flags':str(f),'data':isData(f)})
print 'Caching Frames'
for f in frames:
    if f:
        debugPrint('\tframe: '+hex(f))
        first = GetFirstMember(f)
        last = GetLastMember(f)
        db.GetFirstMember.insert({'frame':str(f),'member':myHex(first)})
        db.GetLastMember.insert({'frame':str(f),'member':myHex(last)})
        curr = first
        while curr <= last:
            db.GetMemberName.insert({'frame':str(f),'member':str(curr),'name':GetMemberName(f,curr)})
            db.GetMemberSize.insert({'frame':str(f),'member':str(curr),'size':myHex(GetMemberSize(f,curr)) if GetMemberSize(f,curr) else myHex(GetStrucNextOff(f,curr)-curr)})
            db.GetStrucNextOff.insert({'frame':str(f),'member':str(curr),'next':myHex(GetStrucNextOff(f,curr))})
            curr = GetStrucNextOff(f,curr)
print 'Building Indexes'
#db.get_imagebase.ensure_index('base')
#db.Functions.ensure_index('functions')
db.FlowChart.ensure_index('ea')
db.GetType.ensure_index('ea')
db.NextHead.ensure_index('ea')
db.PrevHead.ensure_index('ea')
db.ItemSize.ensure_index('ea')
db.GetManyBytes.ensure_index('ea')
db.DataRefsFrom.ensure_index('ea')
db.DataRefsTo.ensure_index('ea')
db.GetFlags.ensure_index('ea')
db.XrefsTo.ensure_index('ea')
db.GetMnem.ensure_index('ea')
db.get_func.ensure_index('ea')
db.GetFrameArgsSize.ensure_index('ea')
db.GetFrame.ensure_index('ea')
db.GetFunctionName.ensure_index('ea')
db.Name.ensure_index('ea')
db.LocByName.ensure_index('name')
db.Demangle.ensure_index('s')
db.isData.ensure_index('flags')
db.GetFirstMember.ensure_index('frame')
db.GetLastMember.ensure_index('frame')
db.GetOpnd.ensure_index([('ea',ASCENDING),('n',ASCENDING)])
db.GetMemberName.ensure_index([('frame',ASCENDING),('member',ASCENDING)])
db.GetMemberSize.ensure_index([('frame',ASCENDING),('member',ASCENDING)])
db.GetStrucNextOff.ensure_index([('frame',ASCENDING),('member',ASCENDING)])
print 'Done'
qexit(0)
