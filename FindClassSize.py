from imports import *

from analysisUtils import getDataAnalyzer
from func_utils import getFunctionStartAddress

class TypeRuler:
		
	# uninteresting functions are compiler generated function which are not part of user code
	from analysisUtils import outsideMain
	uninterestingFunctions = outsideMain()
	deletes = None
	news = None
	destructors = None
	depth = 2
	splitCache = {}
	
	# check if object originated from an inner command or is an argument from an external function
	def chooseInnerOrOuter(self,o,onl):
		#print o
		#print onl
		if len(o) == 0:
			return ([],False)
		matched = len(filter(lambda x: match('^((E[ACD]X0)|(EBX)|(E[SD]I)|(E[SBI]P)|(E[BS]P\+0[0-9A-Fa-f]+h))$',x), o))
		if matched == 0:
			#print 'inner'
			return self.getSizesFromInner(onl)
		else:
			#print 'outer'
			return self.getSizesFromOuter(onl)
			
	# get size of object created in this function
	def getSizesFromInner(self,objectsAndLines):
		objects = set(map(lambda y: y['object'],objectsAndLines)) # all object expressions
		sizes = []
		if len(filter(lambda x: match('^(E[ACD]X[0-9]+)$',x),objects)) != 0:
			# if there is an objects which was not created here (is an initial value of a register), raise error
			#raise Exception('Object is not a result of calling new')
			for record in objectsAndLines:
				line = record['line']
				obj = record['object']
				startLine = getFunctionStartAddress(line)
				da = getDataAnalyzer(startLine)
				if da:
					states = da.getStatesPerLine()
					# find last call before assigning virtual table (this should be operator new)
					states = states[filter(lambda l:long(l)==line,states.keys())[0]]
					def goBack(states,obj):
						result = []
						for s in states:
							if len(filter(lambda p: obj in p.registers.values(),s.previous)) != len(s.previous):
								result.append(s)
							else:
								result.extend(goBack(s.previous,obj))
						return result
					states = goBack(states,obj)
					def cleanTargetName(fname):
						if fname.startswith('j_'):
							fname = fname[2:]
						if match('.*_[0-9]$',fname):
							fname = fname[:-2]
						return fname
					for s in filter(lambda s: (GetMnem(s.ea) == 'call') and str(Demangle(cleanTargetName(GetOpnd(s.ea,0)),0)).endswith('operator new(unsigned int)'),states):
						def getSizeInt(s):
							try:
								return int(s.stack[s.registers['esp']][:-1],16)
							except ValueError:
								return None
						sizes.extend(filter(lambda n:n, map(lambda s:getSizeInt(s),s.previous)))
		if len(filter(lambda x: match('^(E[BS]P\-0[0-9A-Fa-f]+h)$',x),objects)) != 0:
			for record in objectsAndLines:
				obj = record['object']
				if match('^E[SB]P\-0[0-9a-fA-F]+h$',obj):
					line = record['line']
					frame = GetFrame(long(line))
					if frame:
						members = {}
						firstMember = GetFirstMember(frame)
						lastMember = GetLastMember(frame)
						currMember = firstMember
						zeroOffset = None
						rOffset = None
						sOffset = None
						while currMember<=lastMember:
							mName = GetMemberName(frame,currMember)
							#print '\t'+str(mName)+' '+str(currMember)
							if mName == ' r':
								rOffset = currMember
							if mName == ' s':
								sOffset = currMember
							members[currMember] = GetMemberSize(frame,currMember)
							currMember = GetStrucNextOff(frame,currMember)
						if sOffset:
							zeroOffset = sOffset
						else:
							if rOffset:
								zeroOffset = rOffset-4
						if zeroOffset:
							if obj[1] == 'S':
									zeroOffset += 4
							offset = zeroOffset - int(obj[4:-1],16)
							if offset in members.keys():
								sizes.append(members[offset])
		return (sizes,len(sizes)!=0)
	
	# get size of object created in an external function
	def getSizesFromOuter(self,objectsAndLines):
		outerObjectsAndLines = []
		outerObjects = []
		sizes = []
		for onl in objectsAndLines:
			obj = onl['object']
			c = getFunctionStartAddress(onl['line'])
			da = getDataAnalyzer(c)
			if da:
				if obj in da.getFields().keys():
					#print obj
					#print hex(c)
					#print da.getFields()[obj]
					#print map(lambda f:f[1]+4,da.getFields()[obj])
					sizes.append(max(map(lambda f:f[1]+4,da.getFields()[obj])))
			# object is created in external function. find all lines where current function is called
			for r in XrefsTo(c):
				f = getFunctionStartAddress(r.frm)
				#print hex(f)[:-1]
				# find matching object expression in external function
				da = getDataAnalyzer(f)
				if da:
					states = da.getStatesPerLine()
					currStates = states[filter(lambda l:long(l)==r.frm,states.keys())[0]]
					prevStates = []
					for s in currStates:
						prevStates.extend(s.previous)
					o = obj.lower()
					if o.endswith('0'):
						o = o[:-1]
					for s in prevStates:
						if o in s.registers.keys():
							newO = s.registers[o]
							outerObjectsAndLines.append({'object':newO,'line':r.frm})
							outerObjects.append(newO)
		result = self.chooseInnerOrOuter(outerObjects, outerObjectsAndLines)
		if not result[1]:
			result[0].extend(sizes)
		return result
			
	def getAllDeletes(self):
		if not TypeRuler.deletes:
			delete = set()
			for f in Functions():
				fname = GetFunctionName(f)
				if fname.startswith('j_'):
					fname = fname[2:]
				if match('.*_[0-9]$',fname):
					fname = fname[:-2]
				if str(Demangle(fname,0)).endswith('operator delete(void *)'):
					delete.add(f)
			#print map(lambda x:hex(x)[:-1],delete)
			result = set()
			for d in delete:
				result.add(d)
				t = d
				while GetMnem(t) == 'jmp':
					t = GetOpnd(t,0)
					t = LocByName(t)
					if (t == 0xffffffffffffffff) or (t in result):
						break
					result.add(t)
				def goBack(t):
					for x in XrefsTo(t):
						if GetMnem(x.frm) == 'jmp':
							if get_func(x.frm):
								if get_func(x.frm).startEA == x.frm:
									if x not in result:
										result.add(x.frm)
										goBack(x.frm)
				goBack(d)
			TypeRuler.deletes = result
		return TypeRuler.deletes
		
	def getAllNews(self):
		if not TypeRuler.news:
			delete = set()
			for f in Functions():
				fname = GetFunctionName(f)
				if fname.startswith('j_'):
					fname = fname[2:]
				if match('.*_[0-9]$',fname):
					fname = fname[:-2]
				if str(Demangle(fname,0)).endswith('operator new(unsigned int)'):
					delete.add(f)
			#print map(lambda x:hex(x)[:-1],delete)
			result = set()
			for d in delete:
				result.add(d)
				t = d
				while GetMnem(t) == 'jmp':
					t = GetOpnd(t,0)
					t = LocByName(t)
					if (t == 0xffffffffffffffff) or (t in result):
						break
					result.add(t)
				def goBack(t):
					for x in XrefsTo(t):
						if GetMnem(x.frm) == 'jmp':
							if get_func(x.frm):
								if get_func(x.frm).startEA == x.frm:
									if x not in result:
										result.add(x.frm)
										goBack(x.frm)
				goBack(d)
			TypeRuler.news = result
		return TypeRuler.news
		
	def getAllPossibleDestructors(self):
		from func_utils import collectCalls
		def getDeletedItems(ea):
			#print '\t\t'+hex(ea)
			da = getDataAnalyzer(ea)
			#print '\t\t\tdone'
			if not da:
				return set()				
			states = da.getStatesPerLine()
			currStates = states[filter(lambda l:long(l)==ea,states.keys())[0]]
			prevStates = []
			for s in currStates:
				prevStates.extend(s.previous)
			return set(filter(lambda y:y!='00h',map(lambda s: s.stack[s.registers['esp']] if (s.registers['esp'] in s.stack.keys()) else '['+s.registers['esp']+']',prevStates)))
		def clearOpnd(o):
			if match('^[scdefg]s\:',o):
				o = o[3:]
			if match('0x[0-9a-fA-F]+',o):
				try:
					o = long(o,16)
				except ValueError:
					o = 0xffffffffffffffff
			else:
				o = LocByName(o)
			return o
		if not TypeRuler.destructors:
			deletes = self.getAllDeletes()
			news = self.getAllNews()
			#print map(lambda x:hex(x),deletes)
			destructorsTmp = set()
			for d in deletes:
				for x in XrefsTo(d):
					if x.frm not in deletes:
						if len(collectCalls(x.frm).intersection(news)) == 0:
							if len(filter(lambda x: match('^EAX[1-9]+$',x) or match('\[ESP-[0-9A-Fa-f]+h\]',x),getDeletedItems(x.frm))) == 0:
								fnc = get_func(x.frm)
								if fnc:
									destructorsTmp.add((fnc.startEA,0))
			TypeRuler.destructors = set()
			def goBack(ea):
				result = set([ea])
				for x in XrefsTo(ea):
					xea = x.frm
					if GetMnem(xea) == 'jmp':
						f = get_func(xea)
						if f:
							if (f.endEA == (xea + ItemSize(xea))) or (f.startEA == xea):
								result = result.union(goBack(f.startEA))
				return result
			for ea in destructorsTmp:
				TypeRuler.destructors = TypeRuler.destructors.union(goBack(ea[0]))
			"""
			while len(destructorsTmp) != 0:
				d = destructorsTmp.pop()
				TypeRuler.destructors.add(d[0])
				if d[1] < TypeRuler.depth:
					funcs = collectCalls(d[0])
					for f in funcs:
						#f = clearOpnd(f)
						if f != 0xffffffffffffffff:
							if GetMnem(f) == 'jmp':
								while GetMnem(f) == 'jmp':
									if (f not in TypeRuler.destructors) and (f not in deletes):
										TypeRuler.destructors.add(f)
									t = GetOpnd(f,0)
									f = clearOpnd(t)
							if (f not in TypeRuler.destructors) and (f not in deletes):
								destructorsTmp.add((f,d[1]+1))
			"""
			#print sorted(map(lambda x:hex(x), TypeRuler.destructors))
		return TypeRuler.destructors
		
	# separate constructor from destructors
	def SplitConstructorsAndDestructorsOfVT(self,vtea):
		#print 'in split'
		if vtea not in TypeRuler.splitCache.keys():
			destructs = set()
			constructs = set()
			allDestructors = self.getAllPossibleDestructors()
			for r in DataRefsTo(vtea):
				#print 't'+hex(r)
				if getFunctionStartAddress(r) in allDestructors:
					destructs.add(r)
				else:
					constructs.add(r)
			TypeRuler.splitCache[vtea] = (constructs,destructs)
		return TypeRuler.splitCache[vtea]
	def getConstructorsOfVT(self,vtea):
		#print 'in get'
		return self.SplitConstructorsAndDestructorsOfVT(vtea)[0]
	def getDestructorsOfVT(self,vtea):
		return self.SplitConstructorsAndDestructorsOfVT(vtea)[1]
			
	# check if constructor is called from another constructor
	def isInheritanceConstructor(self,ea,vts):
		c = getFunctionStartAddress(ea)
		# if this constructor is shared with other virtual tables, it's probably an optimized constructor and not inheritance
		if len(filter(lambda vt:c in self.getFilteredConstructorsOfVT(vt.ea), vts)) >= 2:
			return True;			
		for r in XrefsTo(c):
			f = getFunctionStartAddress(r.frm)
			for vtable in vts:
				if ea != vtable.ea:
					# if constructor is called from constructor of other class, it is inheritance.
					if f in self.getFilteredConstructorsOfVT(vtable.ea):
						return False
		return True
	
	def getFilteredConstructorsOfVT(self,vt):
		#print 'in filter'
		return filter(lambda f: getFunctionStartAddress(f) not in self.uninterestingFunctions,self.getConstructorsOfVT(vt.ea))
	
	sizeCache = None
	def findClassSizeSingle(self,vt):
		constructors = self.getFilteredConstructorsOfVT(vt)
		#print hex(vt.ea)+':'
		sizes = []
		foundAllocation = False
		for ea in constructors:
			#print '\t'+hex(ea)
			da = getDataAnalyzer(ea)
			if da:
				#print '\t\tda done'
				states = da.getStatesPerLine()
				state = states[filter(lambda x: long(x) == ea, states.keys())[0]]
				objects = []
				#print '\t\t'+str(len(state)
				for s in state:
					#objects.extend(filter(lambda m: m.startswith('[') and m.endswith('h]') and m[1:-2].isdigit() and (long(s.memory[m][:-1],16) == vt.ea), s.memory.keys()))
					def filterNotNumbersMem(x,v):
						try:
							return long(s.memory[x][:-1],16) == v
						except ValueError:
							return False
					def filterNotNumbersStack(x,v):
						try:
							return long(s.stack[x][:-1],16) == v
						except ValueError:
							return False
					objects.extend(filter(lambda m: filterNotNumbersMem(m,vt.ea), s.memory.keys()))
					objects.extend(filter(lambda m: filterNotNumbersStack(m,vt.ea), s.stack.keys()))
				objects = set(objects)
				#print '\t\t'+str(objects)
				if len(objects) > 0:
					objectAndLine = map(lambda o: {'object':o,'line':ea}, objects)
					result = self.chooseInnerOrOuter(objects, objectAndLine)
					if foundAllocation == result[1]:
						sizes.extend(result[0])
					else:
						if result[1]:
							foundAllocation = True
							sizes = result[0]
		# if the are several sizes available, leave size as undetermined
		"""
		sizes = set(sizes)
		#print hex(vt.ea)[:-1]
		#print sizes
		if len(sizes) == 1:
			size = sizes.pop()
			vt.alloc = int(size[:-1],16)
			print 'allocated size of class for vtable at '+hex(vt.ea)[:-1]+' is '+hex(vt.alloc)
		if len(sizes) > 1:
			vt.allocMin = min(map(lambda x:int(x[:-1],16),sizes))
			vt.allocMax = max(map(lambda x:int(x[:-1],16),sizes))
			print 'allocated size of class for vtable at '+hex(vt.ea)[:-1]+' is between '+hex(vt.allocMin)+' and '+hex(vt.allocMax)
		"""
		#print sizes
		if len(sizes) > 0:
			#print sizes
			if foundAllocation:
				vt.alloc = min(sizes)
			else:
				vt.alloc = max(sizes)
			vt.foundAllocation = foundAllocation
			return (vt.alloc,vt.foundAllocation)
			#TypeRuler.sizeCache[vt.ea] = (vt.alloc,vt.foundAllocation)
			#print 'allocated size of class for vtable at '+hex(vt.ea)+' is '+hex(vt.alloc)
		return None		
	def findClassSizes(self,vts):
		if TypeRuler.sizeCache:
			for vt in vts:
				if vt.ea in TypeRuler.sizeCache.keys():
					vt.alloc = TypeRuler.sizeCache[vt.ea][0]
					vt.foundAllocation = TypeRuler.sizeCache[vt.ea][1]
			return
		from analysisUtils import getAllAllocatedVirtualTables
		allocated = getAllAllocatedVirtualTables()
		vts = filter(lambda v:v.ea in allocated,vts)
		#TypeRuler.sizeCache = {}	
		#constructors = {}
		#print 'uninteresting: '+str(map(lambda x:hex(x)[:-1],uninterestingFunctions))
		#print ''
		#for vt in vts:
		#	#print hex(vt.ea)
		#	constructors[vt.ea] = self.getFilteredConstructorsOfVT(vt)
		#	#print hex(vt.ea)[:-1]+': '+str(map(lambda x: hex(x)[:-1], constructors[vt.ea]))
		#print ''
		#for k in constructors.keys():
		#	print hex(k)[:-1] + ':'
		#	print str(map(lambda x: hex(x)[:-1], constructors[k]))
		#print ''
		#return
		TypeRuler.sizeCache = dict(filter(lambda (x,f):f, zip(map(lambda v:v.ea,vts), map(self.findClassSizeSingle, vts))))
			

if __name__ == '__main__':
	from vtables import VirtualTableFinder
	start = time()
	TypeRuler().findClassSizes(VirtualTableFinder().find_virtual_tables().values())
	end = time()
	print 'runtime: ' + str(end-start)
	print 'Done!'