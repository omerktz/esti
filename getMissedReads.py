#from idc import *
#from idaapi import *
#from idautils import *
from idaCache import *

from vtables import VirtualTableFinder

fOut = open('/home/omerkatz/vtsPomela/benchmarks/missedReads','a')

all = 0
counter = 0
mnem = set()
for funcea in Functions():
	f = get_func(funcea)
	c = f.startEA
	while c < f.endEA:
		all += 1
		if GetMnem(c) in ['add','sub', 'xor', 'cmp','or','imul','mul','div','and', 'adc', 'addsd', 'bsf', 'cmova', 'cmovb', 'cmovg', 'cmovnb', 'cmovnz', 'cmovz', 'comisd', 'comiss', 'cvtsd2ss', 'cvtsi2sd', 'cvttsd2si', 'div', 'divsd', 'divss', 'fadd', 'fcomp', 'fidiv', 'fild', 'fimul', 'fistp', 'fistp', 'fld', 'fmul', 'fst', 'fstp', 'fsub', 'idiv', 'movd', 'movdqa', 'movdqu', 'movq', 'movsd', 'movss', 'movups', 'mulss', 'sbb', 'subsd', 'subss', 'ucomiss']:
			if GetOpnd(c,1).endswith(']'):
				mnem.add(GetMnem(c))
				counter += 1	
		c += ItemSize(c)		
fOut.write(get_root_filename()+'\t'+str(counter)+'\t'+str(all)+'\t'+str(100*counter/float(all))+'\t'+str(mnem)+'\n')
fOut.close()

print 'Done!'

qexit(0)

