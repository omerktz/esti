from imports import *

def getEditCost(t1,t2):
	if t1==t2:
		return 0
	if (t1.startswith('Read') or t1.startswith('Write')) and (t2.startswith('Read') or t2.startswith('Write')):
		if t1[t1.find('-'):] == t2[t2.find('-'):]:
			return 5
	return 10
	
def damerau_levenshtein_distance(seq1, seq2):
	# Conceptually, this is based on a len(seq1) + 1 * len(seq2) + 1 matrix.
	# However, only the current and two previous rows are needed at once,
	# so we only store those.
	oneago = None
	seq1firstIsOptional = int(not seq1[0].startswith('ReturnedFromFunction-'))
	seq2firstIsOptional = int(not seq2[0].startswith('ReturnedFromFunction-'))
	thisrow = map(lambda n:n*10,range(seq2firstIsOptional, len(seq2) + seq2firstIsOptional)) + [0]
	for x in xrange(len(seq1)):
		# Python lists wrap around for negative indices, so put the
		# leftmost column at the *end* of the list. This matches with
		# the zero-indexed strings and saves extra calculation.
		twoago, oneago, thisrow = oneago, thisrow, [0] * len(seq2) + [(x + seq1firstIsOptional)*10]
		for y in xrange(len(seq2)):
			delcost = oneago[y] + 10
			addcost = thisrow[y - 1] + 10
			subcost = oneago[y - 1] + getEditCost(seq1[x],seq2[y])
			thisrow[y] = min(delcost, addcost, subcost)
			# This block deals with transpositions
			if (x > 0 and y > 0 and seq1[x] == seq2[y - 1]
				and seq1[x-1] == seq2[y] and seq1[x] != seq2[y]):
				thisrow[y] = min(thisrow[y], twoago[y - 2] + 10)
	return thisrow[len(seq2) - 1]

"""
def levenshtein_distance(s1, s2):
    if s1 == s2:
        return 0
    rows = len(s1)+1
    cols = len(s2)+1
    if not s1:
        return cols-1
    if not s2:
        return rows-1
    prev = None
    cur = range(cols)
    for r in xrange(1, rows):
        prev, cur = cur, [r] + [0]*(cols-1)
        for c in xrange(1, cols):
            deletion = prev[c] + 1
            insertion = cur[c-1] + 1
            edit = prev[c-1] + (0 if s1[r-1] == s2[c-1] else 1)
            cur[c] = min(edit, deletion, insertion)
    return cur[-1]
"""
	
def concatParents(t,targets):
	types = set()
	nextTypes = set([t])
	result = {}
	while len(nextTypes) != 0:
		currType = nextTypes.pop()
		if currType not in types:
			types.add(currType)
			for p in db.hierarchy.find({"binary":binary,"type":currType}):
				nextTypes.add(p["parent"])
			for c in targets[currType].keys():
				if c not in result.keys():
					result[c] = 0
				result[c] += targets[currType][c]
	return result.keys()

def concatSiblings(c,candidates):
	result = set()
	function = c[:c.rfind('.')]
	for r in db.siblings.find({"binary":binary,"function":function,"object":c[c.rfind('.')+1:]}):
		obj = function+'.'+r["sibling"]
		if obj in candidates.keys():
			for s in candidates[obj].keys():
				result.add(s)
	return result

def calcCoverageMatch(c,t):
	def calcScore(p1,p2):
		#p1 = map(lambda p: p[:p.find('-')] if p.startswith('Used') else p, p1)
		#p2 = map(lambda p: p[:p.find('-')] if p.startswith('Used') else p, p2)
		return damerau_levenshtein_distance(p1,p2)
	coverage = {}
	#print '\t\t'+str(len(c))
	for i in c:
		partsI = i.split(';')[:-1]
		scores = {}
		for j in t:
			partsJ = j.split(';')[:-1]
			scores[j] = calcScore(partsI,partsJ)
		coverage[i] = min(scores.values())/float(pow(len(partsI),2))
	return sum(coverage.values())
	
if __name__ == '__main__':
	start = time()

	candidates = {}
	targets = {}

	types = {}

	if len(sys.argv) != 3:
		print 'Usage: python matches.py <binaryName> <1=types, 2=objects>'
		sys,exit()
	binary = sys.argv[1]
	tORo = int(sys.argv[2])
		
	db = getDB(binary)
	
	for r in db.typesignatures.find({"binary":binary}):
		if r["typename"] not in targets.keys():
			targets[r["typename"]] = {}
		targets[r["typename"]][r["code"]] = r["appearances"]
	for r in db.untypedsignatures.find({"binary":binary}):
		obj = r["function"]+'.'+r["object"]
		types[obj] = r["realtype"]
		if obj not in candidates.keys():
			candidates[obj] = {}
		candidates[obj][r["code"]] = r["appearances"]

	results = {}

	if tORo == 2:
		print 'Number of objects: '+str(len(candidates.keys()))
		print ''
		for o in candidates.keys():
			results[o] = {}
			#typeStr = ""
			#if withTypes:
			typeStr = ' ('+types[o]+')'
			print o+typeStr
			for t in targets.keys():
				results[o][t] = calcCoverageMatch(concatSiblings(o,candidates),concatParents(t,targets))
			for t in sorted(targets.keys(), key=results[o].get):
				print '\t'+t+' : '+str(results[o][t])
			if types[o] not in db.typed.distinct("typename"):
				print 'Weird'
			else:
				if types[o] in filter(lambda t: results[o][t]==min(results[o].values()),results[o].keys()):
					if max(results[o].values())==min(results[o].values()):
						print 'Undetermined'
					else:
						print 'Good'
				else:
					print 'Bad'
			print ''
	if tORo == 1:
		for o in targets.keys():
			results[o] = {}
			print o
			for t in targets.keys():
				#print '\t'+t
				results[o][t] = calcCoverageMatch(targets[o].keys(),concatParents(t,targets))
			for t in sorted(targets.keys(), key=results[o].get):
				print '\t'+t+' : '+str(results[o][t])
			print ''
		print '=============================='
		for o in targets.keys():
			results[o] = {}
			print o
			for t in targets.keys():
				results[o][t] = calcCoverageMatch(concatParents(o,targets),concatParents(t,targets))
			for t in sorted(targets.keys(), key=results[o].get):
				print '\t'+t+' : '+str(results[o][t])
			print ''

	end = time()
	print 'runtime: ' + str(end-start)
	print 'Done!'
