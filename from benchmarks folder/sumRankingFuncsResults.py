import numpy
from re import match
import sys
import os
from pymongo import MongoClient
from mimify import File
if os.environ.get('OMERK_PROFILING'):
	from multiprocessingMock import Pool
else:
	from multiprocessing import Pool

def filterObjects(o):
	if sys.argv[1] in ignores.keys():
		if o in ignores[sys.argv[1]]:
			return False
	o = o[o.rfind('.')+1:]
	if o.startswith('[EBP'):
		return False
	def is_object(o):
		if '[' not in o and ']' not in o:
			return True
		matched = match('^\[(.+)\]([\+\-]0[0-9a-fA-F]+h)?$',o)
		if matched:
			return is_object(matched.groups()[0])
		else:
			return False
	return is_object(o)

resolvedCalls = 0
def countFuncsForCallLoc(callLoc):
	global resolvedCalls
	function = set(db.objectsInCalls.find({'binary':sys.argv[1],'line':callLoc}).distinct('function')).pop()
	count = []
	total = []
	matchedObjects = set(filter(lambda o: unstripped2stripped[function]+'.'+o in objects.keys(),filter(lambda o: (match('^0[0-9A-Fa-f]+h$',o) and len(o) == 8) or (filterObjects(o)),db.objectsInCalls.find({'binary':sys.argv[1],'line':callLoc,'function':function}).distinct('object'))))
	if len(matchedObjects) > 0:
		allTypes = set(objects[unstripped2stripped[function]+'.'+matchedObjects.pop()])
		for matchedObject in db.objectsInCalls.find({'binary':sys.argv[1],'line':callLoc,'function':function}).distinct('object'):
			fullObj = unstripped2stripped[function]+'.'+matchedObject
			if fullObj in objects.keys():
				allTypes = allTypes.intersection(objects[fullObj])
		for matchedObject in db.objectsInCalls.find({'binary':sys.argv[1],'line':callLoc,'function':function}).distinct('object'):
			if match('^0[0-9A-Fa-f]+h$',matchedObject) and len(matchedObject) == 8:
				if matchedObject[1:-1] in db.vtables.distinct('vtable'):
					offset = set(db.objectsInCalls.find({'binary':sys.argv[1],'line':callLoc,'function':function,'object':matchedObject}).distinct('offset')).pop()
					count.append(1)
					total.append(numFuncsPerOffset[offset])					
			else:		
				if filterObjects(matchedObject):
					offset = set(db.objectsInCalls.find({'binary':sys.argv[1],'line':callLoc,'function':function,'object':matchedObject}).distinct('offset')).pop()
					fullObj = unstripped2stripped[function]+'.'+matchedObject
					if fullObj in objects.keys():
						matchedTypes = filter(lambda t:t in allTypes, objects[fullObj])
						#matchedTypes = filter(lambda n: 'RTTI' not in n,matchedTypes)
						matchedFuncs = set(map(lambda t: typesCache[t][offset],filter(lambda y: offset in typesCache[y].keys(), filter(lambda x: x in typesCache.keys(),matchedTypes))))
						if len(matchedFuncs) > 0:
							count.append(len(matchedFuncs))
							total.append(numFuncsPerOffset[offset])
		if len(count) > 0:
			if len(set(count)) != 1:
				print 'WARNING: function up-to count inconsistent among objects (callLoc='+str(callLoc)+')'
			resolvedCalls += 1
			#if min(count)==165:
			#	print callLoc
			#	print map(lambda i:total[i],filter(lambda i: count[i]==165,range(len(count))))
			return (min(count),100*min(count)/min(map(lambda i:total[i],filter(lambda i: count[i]==min(count),range(len(count))))))
		return None

objects = {}
def statFile(fileInput):
	global resolvedCalls
	fIn = open('./matchResults/'+fileInput,'r')
	fIn.readline()
	fIn.readline()
	line = fIn.readline()
	object = None
	type = None
	lastScore = ''
	position = -1
	total = 0
	lastLineEmpty = False
	countUpTo = True
	upTo = set()
	finalScore = False
	#fOut.write('name\ttype\tscore_position\ttotal_scores\tfound_0\ttypes_up_to\tvariance\tmean\tvmr\n')
	types = 0
	exact = 0
	unmatched = 0
	totalObjects = 0
	matchedObjects = 0
	while line: # != '':
		#print line
		if line.strip() == '':
			if lastLineEmpty:
				break
			if filterObjects(object):
				totalObjects += 1
				if type != 'Unknown':
					#fOut.write(str(object)+'\t'+str(type)+'\t'+str(position)+'\t'+str(total)+'\t'+str(otherMatched)+'\t'+str(upTo)+'\t'+str(numpy.var(scores))+'\t'+str(numpy.mean(scores))+'\t'+str(0 if numpy.mean(scores)==0 else (numpy.var(scores)/numpy.mean(scores)))+'\n')
					if position == -1:
						unmatched += 1
					else:
						if types == 1:
							exact += 1
						objects[object] = upTo
						matchedObjects += 1
			object = None
			type = None
			lastScore = ''
			position = -1
			total = 0
			lastLineEmpty = True
			upTo = set()
			countUpTo = True
			finalScore = False
			types = 0
			rank = 0
		else:
			lastLineEmpty = False
			if line.startswith('\t'):
				tmp = line[1:]
				tmp = tmp[:tmp.find('\t')].strip()
				name = tmp[:tmp.rfind(':')-1]
				score = tmp[tmp.rfind(':')+2:]
				if name != 'OmerK-EMPTY':
					types += 1
					if score != lastScore:
						if finalScore:
							countUpTo = False
						lastScore = score
						total += 1
					if name == type:
						position = total
						finalScore = True
					if countUpTo:
						upTo.add(name)
			else:
				object = line[:line.find(' ')]
				tmp = line[len(object)+1:]
				type = tmp[:tmp.find('\t')][1:-1]
		line = fIn.readline()

	pool = Pool(processes=int(sys.argv[4]))
	
	callLocations = db.objectsInCalls.find({'binary':sys.argv[1]}).distinct('line')
	numCalls = len(callLocations)
	tmp = filter(lambda x:x, map(countFuncsForCallLoc,callLocations))
	uptos = map(lambda x:x[0], tmp)
	uptosRatio = map(lambda x:x[1], tmp)
	pool.close()
	pool.join()
	#print uptos
 
	mrr = sum(map(lambda x:1.0/x,uptos))/len(uptos) if len(uptos) > 0 else 0
 
	# calculate data for figure
	uptosHistogram = {}
	for i in xrange(int(sizes[sys.argv[1]])+1):
		uptosHistogram[i] = 0
	for i in uptos:
		uptosHistogram[i] += 1
	#print uptosHistogram
	count = 0
	for i in sorted(uptosHistogram.keys()):
		count += uptosHistogram[i]
		uptosHistogram[i] = count
	for i in uptosHistogram.keys():
		if len(uptos) > 0:
				uptosHistogram[i] = 100*uptosHistogram[i]/len(uptos)
		else:
				uptosHistogram[i] = 100
	#print len(filter(lambda x:uptosHistogram[x] == 100,uptosHistogram.keys()))

	area = 0
	for x in uptosHistogram.keys():
		area+= uptosHistogram[x]
	area = float(area)/len(uptosHistogram.keys())

	#create figure
	x = sorted(uptosHistogram.keys())
	y = map(lambda i:uptosHistogram[i],x)
	import matplotlib
	matplotlib.use('Agg')
	import matplotlib.pyplot as plt
	plt.figure(figsize=(30,15))
	plt.tick_params(axis='both', which='major', labelsize=25)
	plt.tick_params(axis='both', which='minor', labelsize=25)
	plt.yticks(numpy.arange(0,110,10))
	xticks = numpy.arange(0,max(uptosHistogram.keys())+1,1)
	plt.xticks(xticks,map(lambda x: str(x) if x % 5 == 0 else '', xticks))
	plt.fill_between(x, 0, y, facecolor='blue', alpha=0.3)
	plt.plot(x, y, color='blue', linewidth=3.0)
	plt.grid(True)
	plt.ylabel('% of Call Sites covered', fontsize=50)
	plt.xlabel('Number of Analyzed Fucntions', fontsize=50)
	#plt.title(sys.argv[1]+' ('+str(resolvedCalls)+'/'+str(numCalls)+')', fontsize=30)
	plt.savefig(sys.argv[3],format='png',transparent=True)


	#if len(lengths) > 0:
	if len(objects.keys()) > 0:
		name = fileInput[:-8]
		min(uptos)
		min(uptosRatio)
		fOut.write(name+'\t'+str(sizes[name])+'\t'+str(numCalls)+'\t'+str(resolvedCalls)+'\t'+str(len(objects.keys())+unmatched)+'\t'+str(unmatched)+'\t'+str(area)+'\t'+str(min(uptos))+'\t'+str(max(uptos))+'\t'+str(numpy.mean(uptos))+'\t'+str(numpy.median(uptos))+'\t'+str(numpy.var(uptos))+'\t'+str(min(uptosRatio))+'\t'+str(max(uptosRatio))+'\t'+str(numpy.mean(uptosRatio))+'\t'+str(numpy.median(uptosRatio))+'\t'+str(numpy.var(uptosRatio))+'\t'+str(mrr)+'\t'+str(uptos)+'\t'+str(uptosRatio)+'\n')
	
	fIn.close()

sizes = {}
fSizes = open(sys.argv[1]+'.sizes','r')
line = fSizes.readline()
while line != '':
	s = line.strip().split('\t')
	sizes[s[0]] = s[1]
	line = fSizes.readline()
fSizes.close()

ignores = {}
fIgnores = open(sys.argv[1]+'.ignores','r')
line = fIgnores.readline()
while line != '':
	s = line.strip().split('\t')
	if s[0] not in ignores.keys():
		ignores[s[0]] = set()
	ignores[s[0]].add(s[1])
	line = fIgnores.readline()
fIgnores.close()

unstripped2stripped = {}
fUnstrippedNames = open('./matchResultsUnstripped/'+sys.argv[1]+'.addresses','r')
line = fUnstrippedNames.readline()
while line != '':
	s = line.split('\t')
	unstripped2stripped[s[1].strip()] = s[0].strip()
	line = fUnstrippedNames.readline()
fUnstrippedNames.close()
	
db = MongoClient()['OmerK_Types_'+sys.argv[1].replace(' ','_').replace('.','_')]
	
typesCache = {}
numFuncsPerOffset = {}
offsets = set()
for typename in db.vtables.find({'binary':sys.argv[1]}).distinct('vtableName'):
	typeFuncs = {}
	for f in db.vtables.find({'binary':sys.argv[1],'vtableName':typename}):
		typeFuncs[f['index']] = f['function']
		offsets.add(f['index'])
	typesCache[typename] = typeFuncs
for i in offsets:
  numFuncsPerOffset[i] = len(set(map(lambda x: typesCache[x][i], filter(lambda t:i in typesCache[t].keys(),typesCache.keys()))))

fOut = open(sys.argv[2],'w')

fOut.write('file\tvtables\tcalls\tresolved\tchecked-objects\tunmatched\tarea\tup-to\t\t\t\t\tratio\t\t\t\t\t\t\t\n')	
fOut.write('\t\t\t\t\t\t\tmin\tmax\tmean\tmedian\tvariance\tmin\tmax\tmean\tmedian\tvariance\tmrr\tup-to raw\tratio raw\n')	

#for f in os.listdir('.'):
#	if f.endswith('.matches'):
#		print f[:-8]
statFile(sys.argv[1]+'.matches')
	
fOut.close()
