from imports import *

# collect all call target from call in function
def collectCallsFromConstructors(ea,funcs):
	startF = LocByName(GetFunctionName(ea))
	if startF != 0xffffffffffffffff:
		from FunctionProvider import Function
		f = Function(startF)
		calls = set()
		for b in f.getBlocks():
			for c in f.getBlockCommands(b):
				if c.startswith('call '):
					targetStr = c[5:]
					if match('^[scdefg]s\:.*',targetStr):
						targetStr = targetStr[3:]
					target = LocByName(targetStr)
					if target != 0xffffffffffffffff:
						calls.add(hex(target)[:-1])
		funcs[hex(startF)[:-1]] = calls

# collect all call targets from final function. if this jump, check target function.
def collectCalls(ea,funcs):
	startF = ea
	while GetMnem(startF) == 'jmp':
		startF = LocByName(GetOpnd(startF,0))
	if startF != 0xffffffffffffffff:		
		from FunctionProvider import Function
		f = Function(startF)
		calls = set()
		for b in f.getBlocks():
			for c in f.getBlockCommands(b):
				if c.startswith('call '):
					target = LocByName(c[5:])
					if target != 0xffffffffffffffff:
						calls.add(hex(target)[:-1])
		funcs[hex(ea)[:-1]] = calls
	
# fs is set of targets. if a target is a jump, add actual target to fs.
def extendWithJmps(fs):
	additionals = set()
	for f in fs:
		if GetMnem(int(f,16)) == 'jmp':
			opnd = GetOpnd(int(f,16),0)
			if match('^[scdefg]s\:.*',opnd):
				opnd = opnd[3:]
			additionals.add(hex(LocByName(opnd))[:-1])
	return fs.union(additionals)
	
# get address of operator delete (should be replaced with demangle)
def getKnownDeleteAddresses():
	return set(filter(lambda y: y!='0xffffffffffffffff', map(lambda x: hex(LocByName(x))[:-1],['__imp_??3@YAXPAX@Z','__imp___ZdlPv'])))

# when a function can't be called (such as pure virtual functions), the returned arg size is 0xffffffffffffffff
def findPureVirtualFuncs(fs):
	from func_utils import get_function_args_size
	keys = fs.keys()
	pures = {}
	for k in keys:
		pure = set()
		for f in fs[k]:
			if get_function_args_size(int(f,16)) == 0xffffffffffffffff:
				pure.add(f)
		pures[k] = pure
	for k in pures.keys():
		if len(pures[k]) > 0:
			print k + ' has ' + str(len(pures[k])) + ' pure virtual functions'
	
# check if 2 virutal tables share a common function
def checkRelated(fs):
	keys = fs.keys()
	for i in range(len(keys)):
		for j in range(i+1,len(keys)):
			if len(fs[keys[i]].intersection(fs[keys[j]])) != 0:
				print keys[i] + ' and ' + keys[j] + ' are related'

# check if a virtual table calls a function belonging to another virtual table
def checkDependant(fs,deps):
	keys = fs.keys()
	for i in range(len(keys)):
		for j in range(i+1,len(keys)):
			if len(fs[keys[i]].intersection(deps[keys[j]])) != 0:
				print keys[j] + ' depends on ' + keys[i]

# check if constructor calls constructor of another virtual table
def findAncestorsByConstructors(deps,cNd):
	ancestors = {}
	for d in deps.keys():
		matches = set()
		for c in cNd.keys():
			if len(cNd[c].intersection(deps[d])) != 0:
				matches.add(c)
		if len(matches) == 1:
			ancestors[d] = matches.pop()
	print ancestors
	
def buildInhertianceGraph(vts):
	functions = {}
	knownDeleteNames = getKnownDeleteAddresses()
	constructorsAndDestrctors = {}
	dependenciesInconstructors = {}
	dependenciesInFuncs = {}
	constructors = {}
	destructors = {}
	for vt in vts:
		vtAddr = hex(vt.ea)[:-1]
		funcs = {}
		# get constructors and destructors of virtual table
		for r in DataRefsTo(vt.ea):
			collectCallsFromConstructors(r,funcs)
		constructorsAndDestrctors[vtAddr] = set(funcs.keys())
		targets = set()
		constr = set()
		destr = set()
		if len(funcs) > 0:
			for f in funcs.keys():
				targets = targets.union(set(funcs[f]))
				if len(knownDeleteNames.intersection(extendWithJmps(set(funcs[f])))) > 0:
					destr.add(f)
				else:
					constr.add(f)
		constructors[vtAddr] = constr
		destructors[vtAddr] = destr
		dependenciesInconstructors[vtAddr] = targets
		# collect function and dependencies of virtual table
		funcs = {}
		for f in vt.functions:
			collectCalls(f.ea,funcs)
		deps = set()
		if len(funcs) > 0:
			for f in funcs.keys():
				deps = deps.union(set(funcs[f]))
		dependenciesInFuncs[vtAddr] = extendWithJmps(deps)
		functions[vtAddr] = set(map(lambda x: hex(int(x.ea)),vt.functions))
	findPureVirtualFuncs(functions)		
	checkRelated(functions)
	checkDependant(functions,dependenciesInFuncs)
	findAncestorsByConstructors(dependenciesInconstructors,constructorsAndDestrctors)
	
if __name__ == '__main__':
	from vtables import VirtualTableFinder
	buildInhertianceGraph(VirtualTableFinder().find_virtual_tables().values())
